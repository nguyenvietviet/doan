-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2020 at 10:43 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `develop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `avatar`, `active`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Việt', 'nvviet59@gmail.com', NULL, NULL, 1, '$2y$10$.57.MpWrKJnmbYmkiy9Z6.79HucG/htCkqKTgXXJsdjGkpl0Alsdi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `a_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_content` longtext COLLATE utf8mb4_unicode_ci,
  `a_active` tinyint(4) NOT NULL DEFAULT '1',
  `a_author_id` int(11) NOT NULL DEFAULT '0',
  `a_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_view` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `a_hot` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `a_name`, `a_slug`, `a_description`, `a_content`, `a_active`, `a_author_id`, `a_description_seo`, `a_title_seo`, `a_avatar`, `a_view`, `created_at`, `updated_at`, `a_hot`) VALUES
(1, 'Cách sử dụng đt 1', 'cach-su-dung-dt-1', 'Cách sử dụng đt 2019', '<p>C&aacute;ch sử dụng đt</p>', 1, 0, 'Cách sử dụng đt', 'Cách sử dụng đt', '2019-11-20__oppo-a5-2020-white-400x460.png', 0, '2019-11-16 09:56:38', '2019-12-31 04:28:16', 1),
(2, 'iPhone 7: Thiết kế nhàm chán liệu?', 'iphone-7-thiet-ke-nham-chan-lieu', 'iPhone 7: Thiết kế nhàm chán?', '<p>Bẵng đi 2 năm kể từ khi iPhone 6 rồi cả 6S ra mắt, mọi người tr&ocirc;ng chờ một m&agrave;n kế nghiệp thần sầu tới từ gương mặt mới toanh mang t&ecirc;n iPhone 7. Tuy nhi&ecirc;n, tại thời điểm lộ diện, thế hệ n&agrave;y vẫn chưa thực sự khiến fan h&acirc;m mộ ấn tượng mạnh mẽ với những lời rỉ tai nhau rằng &quot;thiết kế y hệt đời trước&quot;, &quot;camera k&eacute;p lồi v&agrave; th&ocirc;&quot;. Lời chế giễu đ&oacute; kh&ocirc;ng phải l&agrave; kh&ocirc;ng c&oacute; cơ sở, khi hầu như mọi thay đổi tr&ecirc;n thiết kế iPhone 7 chỉ l&agrave; c&aacute;ch sắp xếp dải ăng-ten kh&aacute;c đi, bỏ cổng tai nghe truyền thống v&agrave; th&ecirc;m camera k&eacute;p tr&ecirc;n phi&ecirc;n bản Plus. Sự lồi của camera vẫn được l&ocirc;i ra b&agrave;n t&aacute;n nhiều khi c&aacute;c đối thủ kh&aacute;c hầu hết đ&atilde; loại bỏ diện mạo đ&oacute;, trong khi Apple vẫn &quot;ngoan cố&quot; cứng đầu.</p>', 1, 0, 'iPhone 7: Thiết kế nhàm chán', 'iPhone 7: Thiết kế nhàm chán', '2019-11-20__iphone.jpg', 0, '2019-11-16 10:10:05', '2019-12-28 07:17:10', 0),
(3, 'laptop giảm khủng 20%, cơ hội hiếm có đừng bỏ lỡ', 'laptop-giam-khung-20-co-hoi-hiem-co-dung-bo-lo', 'Khuyến mãi duy nhất ngày 20/11/2019.', '<p>Chương tr&igrave;nh Hot sale 20/11 năm nay, kh&aacute;ch h&agrave;ng c&oacute; thể mua laptop Online tại Thế Giới Di Động sẽ được giảm gi&aacute; 20%, duy nhất trong ng&agrave;y h&ocirc;m nay (20/11). Mua laptop cho Ng&agrave;y nh&agrave; gi&aacute;o từ c&aacute;c thương hiệu nổi tiếng với ch&iacute;nh s&aacute;ch bảo h&agrave;nh uy t&iacute;n c&ograve;n được tặng k&egrave;m th&ecirc;m loạt ưu đ&atilde;i gi&aacute; trị.</p>', 1, 0, 'Hôm nay (20/11) laptop giảm khủng 20%, cơ hội hiếm có, săn ngay', 'Hôm nay (20/11) laptop giảm khủng 20%, cơ hội hiếm có, săn ngay', '2019-11-20__tin.jpg', 0, '2019-11-19 22:12:47', '2019-12-28 07:17:26', 0),
(4, 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi ?', 'iphone-x-tai-tho-bi-anti-day-tranh-cai', 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', '<p>Mặc d&ugrave; gl&agrave; chiếc smartphone đột ph&aacute; nhất sau 10 năm ph&aacute;t triển của Apple đồng thời gần như hiện thực ho&aacute; được thiết kế m&agrave;n h&igrave;nh tr&agrave;n cạnh, iPhone X vẫn mang tr&ecirc;n m&igrave;nh một &quot;c&aacute;i gai&quot; kh&oacute; bỏ: Phần &quot;tai thỏ&quot; ở đỉnh cạnh tr&ecirc;n. Đ&acirc;y l&agrave; biểu hiện r&otilde; nhất cho thấy Apple vẫn chưa sẵn s&agrave;ng nghĩ ra một phương &aacute;n kh&aacute;c để giấu đi camera trước c&ugrave;ng c&aacute;c cảm biến, bắt buộc phải giữ lại ở một khoảng ch&egrave;n v&agrave;o m&agrave;n h&igrave;nh n&agrave;y. Kẻ ti&ecirc;n phong lu&ocirc;n gặp nhiều trắc trở, d&ugrave; c&oacute; những ấn tượng tốt nhưng nhiều fan kh&oacute; t&iacute;nh vẫn chưa thực sự bị thuyết phục 100% bởi phần &quot;tai thỏ&quot; n&agrave;y. Việc cắt khuyết lạ lẫm cũng phần n&agrave;o để lại nhiều hệ quả phức tạp như h&igrave;nh ảnh bị che lấp khi xem video, chơi game...</p>', 1, 0, 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', '2019-11-20__iphonex.jpg', 0, '2019-11-19 22:17:28', '2019-12-28 07:17:38', 0),
(5, 'Các điểm tổ chức sự kiện chào năm mới 2020', 'cac-diem-to-chuc-su-kien-chao-nam-moi-2020', 'Chương trình chào năm mới 2020 gồm hoạt động chính là nhạc hội, một số nơi tổ chức bắn pháo hoa.', '<h1>C&aacute;c điểm tổ chức sự kiện ch&agrave;o năm mới 2020</h1>\r\n\r\n<p>Chương tr&igrave;nh ch&agrave;o năm mới 2020 gồm hoạt động ch&iacute;nh l&agrave; nhạc hội, một số nơi tổ chức bắn ph&aacute;o hoa.</p>\r\n\r\n<p>Theo kế hoạch, nhiều địa phương tổ chức hoạt động đếm ngược thời khắc chuyển giao năm cũ v&agrave; năm mới (countdown).&nbsp;</p>\r\n\r\n<p><strong>H&agrave; Nội&nbsp;</strong>tổ chức lễ countdown tại Quảng trường C&aacute;ch mạng Th&aacute;ng T&aacute;m v&agrave; vườn hoa tượng đ&agrave;i L&yacute; Th&aacute;i Tổ. Ngo&agrave;i ra, Quảng trường Đ&ocirc;ng Kinh Nghĩa Thục sẽ c&oacute; chương tr&igrave;nh &acirc;m nhạc điện tử s&ocirc;i động, c&ugrave;ng nhiều hoạt động văn nghệ, tr&ograve; chơi quanh phố đi bộ Hồ Gươm.</p>\r\n\r\n<p>Chương tr&igrave;nh &acirc;m nhạc đếm ngược v&agrave; bắn ph&aacute;o hoa&nbsp;<strong>Đ&agrave; Nẵng&nbsp;</strong>sẽ diễn ra từ 21h ng&agrave;y 31/12 đến 0h ng&agrave;y 1/1/2020 tại Quảng trường 29 th&aacute;ng 3.&nbsp;</p>\r\n\r\n<p>Tại&nbsp;<strong>TP HCM</strong>, chương tr&igrave;nh &acirc;m nhạc countdown 31/12 được tổ chức tại phố đi bộ Nguyễn Huệ (đoạn từ Mạc Thị Bưởi đến Ng&ocirc; Đức Kế) từ&nbsp;20h.&nbsp;Ph&aacute;o hoa sẽ được bắn ở ba điểm l&agrave; đường hầm s&ocirc;ng S&agrave;i G&ograve;n (hầm Thủ Thi&ecirc;m) quận 2, t&ograve;a nh&agrave; Landmark 81 v&agrave; C&ocirc;ng vi&ecirc;n Central Park, quận B&igrave;nh Thạnh; C&ocirc;ng vi&ecirc;n Văn h&oacute;a Đầm Sen, quận 11 từ&nbsp;0h đến 0h15 ng&agrave;y 1/1/2020. Th&agrave;nh phố c&ograve;n tổ chức chương tr&igrave;nh chiếu s&aacute;ng nghệ thuật v&agrave; đồ họa 3D tr&ecirc;n mặt tiền t&ograve;a nh&agrave; UBND, đường L&ecirc; Th&aacute;nh T&ocirc;n v&agrave;o tối 31/12 v&agrave; 1/1/2020.&nbsp;</p>', 1, 0, 'Chương trình chào năm mới 2020 gồm hoạt động chính là nhạc hội, một số nơi tổ chức bắn pháo hoa.', 'Các điểm tổ chức sự kiện chào năm mới 2020', '2019-12-28__daiphunnuochunguyn-1577333869-6006-1577335673.jpg', 0, '2019-12-28 07:52:09', '2019-12-31 04:38:58', 1),
(6, 'Những điểm khách đổ đến, người dân tránh xa', 'nhung-diem-khach-do-den-nguoi-dan-tranh-xa', 'Những điểm khách đổ đến, người dân tránh xa', '<p>Nhiều người khuyến c&aacute;o du kh&aacute;ch kh&ocirc;ng n&ecirc;n mất tiền v&agrave; thời gian tới Champs-Elys&eacute;es, Time Square, Pattaya d&ugrave; ch&uacute;ng được xem l&agrave; biểu tượng du lịch.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/champs-elysees-m_680x0.jpg\" style=\"height:383px; width:680px\" /></p>\r\n\r\n<p><strong>Champs-Elys&eacute;es, Paris, Ph&aacute;p</strong></p>\r\n\r\n<p>Celine, nh&acirc;n vi&ecirc;n thiết kế thời trang, chia sẻ: &ldquo; Kh&ocirc;ng ai sống ở Paris t&igrave;nh nguyện tới đ&acirc;y, trừ khi họ tham gia c&aacute;c cuộc biểu t&igrave;nh. C&oacute; qu&aacute; nhiều du kh&aacute;ch, những nh&agrave; h&agrave;ng, cửa h&agrave;ng đắt đỏ. Đ&acirc;y chắc chắn kh&ocirc;ng phải con đường đẹp nhất ở th&agrave;nh phố&rdquo;. Ban ng&agrave;y, những đo&agrave;n du kh&aacute;ch v&agrave; người th&iacute;ch &quot;sống ảo&quot; khiến con phố trở n&ecirc;n qu&aacute; tải. &ldquo;Du kh&aacute;ch n&ecirc;n đi dạo con k&ecirc;nh, ở bờ s&ocirc;ng Seine, khu Marais, hoặc xung quanh Montorgueil&rdquo;, Celine n&oacute;i. Ảnh:<em>&nbsp;France 24</em>.</p>', 1, 0, 'Những điểm khách đổ đến, người dân tránh xa', 'Những điểm khách đổ đến, người dân tránh xa', '2019-12-28__8-diem-khach-do-den-nguoi-dan-tranh-xa-1577360771.jpg', 0, '2019-12-28 07:53:52', '2019-12-28 07:53:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_icon` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_active` tinyint(4) NOT NULL DEFAULT '1',
  `c_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_total_product` int(11) NOT NULL DEFAULT '0',
  `c_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_keyword_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `c_home` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `c_name`, `c_slug`, `c_icon`, `c_avatar`, `c_active`, `c_title_seo`, `c_total_product`, `c_description_seo`, `c_keyword_seo`, `created_at`, `updated_at`, `c_home`) VALUES
(3, 'Sam Sung', 'sam-sung', 'fa-apple', NULL, 1, 'Điện thoại samsung', 0, 'Điện thoại samsung', NULL, '2019-11-14 06:04:10', '2019-12-30 10:16:41', 1),
(4, 'Xiaomi', 'xiaomi', 'fa-mobile', NULL, 1, 'Điện thoại Xiaomi', 0, 'Điện thoại Xiaomi', NULL, '2019-11-14 06:04:31', '2019-12-30 10:16:49', 1),
(5, 'Apple', 'apple', 'fa-mobile', NULL, 1, 'Điện thoại apple', 0, 'Điện thoại apple', NULL, '2019-11-20 06:03:38', '2019-12-30 16:14:16', 1),
(6, 'Huawei', 'huawei', 'fa-mobile', NULL, 1, 'Điện thoại Huawei', 0, 'Điện thoại Huawe', NULL, '2019-11-20 06:04:14', '2019-11-20 06:04:14', 0),
(7, 'Nokia', 'nokia', 'fa-mobile', NULL, 1, 'Điện thoại Nokia', 0, 'Điện thoại Nokia', NULL, '2019-11-20 06:04:59', '2019-11-20 06:04:59', 0),
(8, 'OPPO', 'oppo', 'fa-mobile', NULL, 1, 'Điện thoại OPPO', 0, 'Điện thoại OPPO', NULL, '2019-11-20 06:05:46', '2019-11-20 06:05:46', 0),
(10, 'VinSmart', 'vinsmart', 'fa-fa-mobile', NULL, 1, 'điện thoại VinSmart', 0, 'điện thoại VinSmart', NULL, '2020-01-10 07:49:47', '2020-01-10 07:49:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_content` text COLLATE utf8mb4_unicode_ci,
  `c_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `c_name`, `c_title`, `c_email`, `c_content`, `c_status`, `created_at`, `updated_at`) VALUES
(1, 'nguyễn việt', 'mua', 'nvviet59@gmail.com', 'mua hàng', 0, NULL, NULL),
(2, 'nguyễn việt địa', 'đơn mua', 'abc@gmail.com', 'mua hàng', 0, '2019-12-01 09:12:39', '2019-12-01 09:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(16, '2014_10_12_000000_create_users_table', 1),
(17, '2014_10_12_100000_create_password_resets_table', 1),
(18, '2019_11_12_043257_create_categories_table', 1),
(19, '2019_11_13_185702_create_products_table', 1),
(20, '2019_11_13_193132_alter_column_pro_content_and_pro_title_seo_in_table_products', 1),
(22, '2019_11_16_160931_create_article_table', 2),
(23, '2019_12_01_155321_create_contact_table', 3),
(24, '2019_12_09_114002_create_transactions_table', 4),
(25, '2019_12_09_114026_create_orders_table', 4),
(26, '2019_12_09_115021_alter_column_pro_pay_in_table_products', 4),
(27, '2019_12_27_113410_create_ratings_table', 5),
(28, '2019_12_27_114442_alter_column_rating_in_table_products', 6),
(29, '2019_12_28_132521_alter_column_total_pay_in_table_users', 7),
(30, '2019_12_30_100453_create_page_statics_table', 8),
(31, '2019_12_30_165758_alter_column_c_home_in_table_categories', 9),
(32, '2019_12_31_093435_create_admins_table', 10),
(33, '2019_12_31_112052_alter_column_a_hot_in_table_articles', 11),
(34, '2020_01_02_113001_create_parameters_table', 12),
(35, '2020_01_02_113648_alter_column_pro_parameter_id_in_products_table', 12),
(36, '2020_01_02_164107_create_slides_table', 13),
(37, '2020_01_08_105936_alter_column_about_end_address_in_users_table', 14),
(38, '2020_01_08_150640_alter_column_code_and_time_code_in_users_table', 15),
(39, '2020_01_10_131424_alter_column_code_active_in_users_table', 16),
(40, '2020_01_16_011423_alter_column_tr_type_in_table_transaction', 17);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `or_transaction_id` int(11) NOT NULL DEFAULT '0',
  `or_product_id` int(11) NOT NULL,
  `or_qty` tinyint(4) NOT NULL DEFAULT '0',
  `or_price` int(11) NOT NULL DEFAULT '0',
  `or_sale` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `or_transaction_id`, `or_product_id`, `or_qty`, `or_price`, `or_sale`, `created_at`, `updated_at`) VALUES
(1, 4, 6, 1, 4, 4, NULL, NULL),
(2, 4, 10, 1, 0, 0, NULL, NULL),
(3, 5, 7, 1, 2, 2, NULL, NULL),
(4, 6, 7, 1, 32323, 2, NULL, NULL),
(5, 6, 10, 1, 21990000, 0, NULL, NULL),
(6, 10, 7, 1, 32323, 2, NULL, NULL),
(7, 11, 7, 1, 32323, 2, NULL, NULL),
(8, 12, 11, 1, 7400000, 0, NULL, NULL),
(9, 13, 9, 1, 5600000, 2, NULL, NULL),
(10, 14, 11, 1, 7400000, 0, NULL, NULL),
(11, 15, 10, 1, 21990000, 50, NULL, NULL),
(12, 16, 10, 1, 21990000, 50, NULL, NULL),
(13, 17, 10, 1, 21990000, 50, NULL, NULL),
(14, 18, 10, 1, 21990000, 50, NULL, NULL),
(15, 19, 10, 1, 21990000, 50, NULL, NULL),
(16, 20, 14, 1, 8000000, 0, NULL, NULL),
(17, 21, 10, 1, 21990000, 50, NULL, NULL),
(18, 23, 10, 1, 21990000, 50, NULL, NULL),
(19, 24, 11, 1, 7400000, 0, NULL, NULL),
(20, 25, 10, 1, 21990000, 50, NULL, NULL),
(21, 26, 10, 1, 21990000, 50, NULL, NULL),
(22, 27, 10, 1, 21990000, 50, NULL, NULL),
(23, 28, 11, 1, 7400000, 0, NULL, NULL),
(24, 29, 11, 1, 7400000, 0, NULL, NULL),
(25, 30, 11, 1, 7400000, 0, NULL, NULL),
(26, 31, 11, 1, 7400000, 0, NULL, NULL),
(27, 32, 15, 1, 14000000, 0, NULL, NULL),
(28, 33, 10, 1, 21990000, 50, NULL, NULL),
(29, 34, 10, 1, 21990000, 50, NULL, NULL),
(30, 35, 10, 1, 21990000, 50, NULL, NULL),
(31, 36, 10, 1, 21990000, 50, NULL, NULL),
(32, 37, 10, 1, 21990000, 50, NULL, NULL),
(33, 38, 10, 1, 21990000, 50, NULL, NULL),
(34, 39, 10, 1, 21990000, 50, NULL, NULL),
(35, 40, 10, 1, 21990000, 50, NULL, NULL),
(36, 41, 10, 1, 21990000, 50, NULL, NULL),
(37, 41, 14, 1, 8000000, 0, NULL, NULL),
(38, 42, 12, 1, 29000000, 0, NULL, NULL),
(39, 43, 10, 1, 21990000, 50, NULL, NULL),
(40, 44, 10, 1, 21990000, 50, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_statics`
--

CREATE TABLE `page_statics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ps_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ps_type` tinyint(4) NOT NULL DEFAULT '1',
  `ps_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_statics`
--

INSERT INTO `page_statics` (`id`, `ps_name`, `ps_type`, `ps_content`, `created_at`, `updated_at`) VALUES
(1, 'về chúng tôi', 1, '<p>nội dung về ch&uacute;ng t&ocirc;i</p>', '2019-12-30 04:01:10', '2019-12-30 04:01:10'),
(3, 'Thông tin giao hàng', 2, '<p><strong>I. Vận chuyển trong nội ngoại th&agrave;nh S&agrave;i G&ograve;n:</strong></p>\r\n\r\n<p><em>a. Kh&aacute;ch h&agrave;ng&nbsp;muốn đặt giao h&agrave;ng xin để lại:</em></p>\r\n\r\n<p>- M&atilde; số sản phẩm c&aacute;c bạn muốn đặt (nếu c&oacute;)</p>\r\n\r\n<p>- Số điện thoại của bạn.</p>\r\n\r\n<p>- Thời gian + địa điểm lấy h&agrave;ng.</p>\r\n\r\n<p>- để G&agrave; Tươi Long B&igrave;nh giao h&agrave;ng nhanh hơn cho qu&yacute; kh&aacute;ch.</p>\r\n\r\n<p><em>b. Thời gian giao h&agrave;ng v&agrave; cước ph&iacute;:</em></p>\r\n\r\n<p>- Nhận ship tất cả c&aacute;c quận trong TP.HCM kể cả c&aacute;c quận xa như B&igrave;nh T&acirc;n, B&igrave;nh Ch&aacute;nh, G&ograve; Vấp, Thủ Đức, H&oacute;c M&ocirc;n&hellip;</p>\r\n\r\n<p>- Giao trong khoản thời gian từ 8h tới 17h (đơn h&agrave;ng sau 18h th&igrave; qua h&ocirc;m sau mới giao)</p>\r\n\r\n<p><strong>- Gi&aacute; giao h&agrave;ng:</strong></p>\r\n\r\n<p>Mức ph&iacute; giao h&agrave;ng: 30k (gồm c&aacute;c&nbsp;quận 12, H&oacute;c M&ocirc;n, B&igrave;nh T&acirc;n,&nbsp;quận 2, 9, Thủ Đức ,quận B&igrave;nh Ch&aacute;nh, Nh&agrave; B&egrave;, Củ Chi)</p>\r\n\r\n<p>- Ph&iacute; n&agrave;y trả cho b&ecirc;n cty chuyển ph&aacute;t h&agrave;ng hoặc nh&acirc;n vi&ecirc;n giao h&agrave;ng</p>\r\n\r\n<p><strong>II. Vận chuyển c&aacute;c tỉnh th&agrave;nh kh&aacute;c:</strong></p>\r\n\r\n<p>- Li&ecirc;n hệ G&agrave; Tươi Long B&igrave;nh&nbsp;để vận chuyển h&agrave;ng c&aacute;ch tốt nhất.</p>\r\n\r\n<p>- hotline : 091.6610.228</p>', '2020-01-02 08:31:07', '2020-01-02 08:31:07'),
(4, 'Thông tin bảo mật', 3, '<p>Ch&uacute;ng t&ocirc;i ph&aacute;t triển một loạt c&aacute;c dịch vụ gi&uacute;p h&agrave;ng triệu người h&agrave;ng ng&agrave;y kh&aacute;m ph&aacute; v&agrave; tương t&aacute;c với thế giới theo những c&aacute;ch mới. C&aacute;c dịch vụ của ch&uacute;ng t&ocirc;i gồm c&oacute;:</p>\r\n\r\n<ul>\r\n	<li>C&aacute;c ứng dụng, trang web v&agrave; thiết bị của Google, chẳng hạn như T&igrave;m kiếm, YouTube v&agrave; Google Home</li>\r\n	<li>C&aacute;c nền tảng như tr&igrave;nh duyệt Chrome v&agrave; hệ điều h&agrave;nh Android</li>\r\n	<li>C&aacute;c sản phẩm t&iacute;ch hợp v&agrave;o c&aacute;c ứng dụng v&agrave; trang web của b&ecirc;n thứ ba, chẳng hạn như c&aacute;c quảng c&aacute;o v&agrave; Google Maps được nh&uacute;ng</li>\r\n</ul>\r\n\r\n<p>Bạn c&oacute; thể sử dụng c&aacute;c dịch vụ của ch&uacute;ng t&ocirc;i theo nhiều c&aacute;ch kh&aacute;c nhau để quản l&yacute; quyền ri&ecirc;ng tư của m&igrave;nh. V&iacute; dụ: bạn c&oacute; thể đăng k&yacute; một T&agrave;i khoản Google nếu muốn tạo v&agrave; quản l&yacute; những nội dung như email v&agrave; ảnh, hoặc xem th&ecirc;m c&aacute;c kết quả t&igrave;m kiếm c&oacute; li&ecirc;n quan. Ngo&agrave;i ra, bạn c&oacute; thể sử dụng nhiều dịch vụ của Google khi đ&atilde; đăng xuất hoặc kh&ocirc;ng cần tạo t&agrave;i khoản, chẳng hạn như t&igrave;m kiếm tr&ecirc;n Google hoặc xem c&aacute;c video tr&ecirc;n YouTube. Bạn cũng c&oacute; thể chọn duyệt web ở chế độ ri&ecirc;ng tư bằng c&aacute;ch sử dụng Chrome ở Chế độ ẩn danh. Hơn nữa, tr&ecirc;n những dịch vụ của ch&uacute;ng t&ocirc;i, bạn c&oacute; thể điều chỉnh c&aacute;c mục c&agrave;i đặt bảo mật để kiểm so&aacute;t th&ocirc;ng tin ch&uacute;ng t&ocirc;i thu thập v&agrave; c&aacute;ch th&ocirc;ng tin của bạn được sử dụng.</p>\r\n\r\n<p>Để gi&uacute;p giải th&iacute;ch mọi điều r&otilde; r&agrave;ng nhất c&oacute; thể, ch&uacute;ng t&ocirc;i đ&atilde; th&ecirc;m c&aacute;c v&iacute; dụ, video giải th&iacute;ch v&agrave; c&aacute;c định nghĩa cho những&nbsp;<a href=\"https://policies.google.com/privacy/key-terms?hl=vi#key-terms\">thuật ngữ ch&iacute;nh</a>. Nếu c&oacute; bất kỳ c&acirc;u hỏi n&agrave;o về Ch&iacute;nh s&aacute;ch bảo mật n&agrave;y, bạn c&oacute; thể&nbsp;<a href=\"https://support.google.com/policies?p=privpol_privts&amp;hl=vi\" target=\"_blank\">li&ecirc;n hệ với ch&uacute;ng</a>&nbsp;t&ocirc;i</p>', '2020-01-02 08:31:45', '2020-01-02 08:31:45'),
(5, 'Điều khoản sửa dụng', 4, '<h2>Sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i</h2>\r\n\r\n<p>Bạn phải tu&acirc;n thủ mọi ch&iacute;nh s&aacute;ch đ&atilde; cung cấp cho bạn trong phạm vi Dịch vụ.</p>\r\n\r\n<p>Kh&ocirc;ng được sử dụng tr&aacute;i ph&eacute;p Dịch vụ của ch&uacute;ng t&ocirc;i. V&iacute; dụ: kh&ocirc;ng được g&acirc;y trở ngại cho Dịch vụ của ch&uacute;ng t&ocirc;i hoặc t&igrave;m c&aacute;ch truy cập ch&uacute;ng bằng phương ph&aacute;p n&agrave;o đ&oacute; kh&ocirc;ng th&ocirc;ng qua giao diện v&agrave; hướng dẫn m&agrave; ch&uacute;ng t&ocirc;i cung cấp. Bạn chỉ c&oacute; thể sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i theo như được luật ph&aacute;p cho ph&eacute;p, bao gồm cả c&aacute;c luật v&agrave; quy định hiện h&agrave;nh về quản l&yacute; xuất khẩu v&agrave; t&aacute;i xuất khẩu. Ch&uacute;ng t&ocirc;i c&oacute; thể tạm ngừng hoặc ngừng cung cấp Dịch vụ của ch&uacute;ng t&ocirc;i cho bạn nếu bạn kh&ocirc;ng tu&acirc;n thủ c&aacute;c điều khoản hoặc ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hoặc nếu ch&uacute;ng t&ocirc;i đang điều tra h&agrave;nh vi bị nghi ngờ l&agrave; sai phạm.</p>\r\n\r\n<p>Việc bạn sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i kh&ocirc;ng c&oacute; nghĩa l&agrave; bạn được sở hữu bất cứ c&aacute;c quyền sở hữu tr&iacute; tuệ n&agrave;o đối với Dịch vụ của ch&uacute;ng t&ocirc;i hoặc n&ocirc;̣i dung m&agrave; bạn truy c&acirc;̣p. Bạn kh&ocirc;ng được sử dụng n&ocirc;̣i dung từ Dịch vụ của ch&uacute;ng t&ocirc;i trừ khi bạn được chủ sở hữu nội dung đ&oacute; cho ph&eacute;p hoặc được luật ph&aacute;p cho ph&eacute;p. C&aacute;c điều khoản n&agrave;y kh&ocirc;ng cấp cho bạn quyền sử dụng bất kỳ thương hiệu hoặc l&ocirc;g&ocirc; n&agrave;o được sử dụng trong Dịch vụ của ch&uacute;ng t&ocirc;i. Kh&ocirc;ng được x&oacute;a, che khuất hoặc thay đổi bất kỳ th&ocirc;ng b&aacute;o ph&aacute;p l&yacute; n&agrave;o được hiển thị trong hoặc k&egrave;m theo Dịch vụ của ch&uacute;ng t&ocirc;i.</p>\r\n\r\n<p>Dịch vụ của ch&uacute;ng t&ocirc;i hiển thị một số n&ocirc;̣i dung kh&ocirc;ng phải của Google. Chỉ một m&igrave;nh đối tượng cung cấp sẽ chịu tr&aacute;ch nhiệm về nội dung n&agrave;y. Ch&uacute;ng t&ocirc;i c&oacute; thể xem x&eacute;t n&ocirc;̣i dung để x&aacute;c định xem nội dung đ&oacute; c&oacute; bất hợp ph&aacute;p hoặc vi phạm ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hay kh&ocirc;ng, v&agrave; ch&uacute;ng t&ocirc;i c&oacute; thể x&oacute;a hoặc từ chối hiển thị n&ocirc;̣i dung m&agrave; ch&uacute;ng t&ocirc;i c&oacute; l&yacute; do ch&iacute;nh đ&aacute;ng để tin rằng nội dung đ&oacute; vi phạm ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hoặc luật ph&aacute;p. Tuy nhi&ecirc;n, điều đ&oacute; kh&ocirc;ng nhất thiết c&oacute; nghĩa l&agrave; ch&uacute;ng t&ocirc;i sẽ xem x&eacute;t nội dung, do vậy bạn đừng cho rằng ch&uacute;ng t&ocirc;i sẽ l&agrave;m như vậy.</p>\r\n\r\n<p>Li&ecirc;n quan đến việc bạn sử dụng Dịch vụ, ch&uacute;ng t&ocirc;i c&oacute; thể gửi cho bạn c&aacute;c th&ocirc;ng b&aacute;o dịch vụ, th&ocirc;ng b&aacute;o quản trị v&agrave; th&ocirc;ng tin kh&aacute;c. Bạn c&oacute; thể chọn kh&ocirc;ng nhận một số th&ocirc;ng b&aacute;o n&agrave;y.</p>\r\n\r\n<p>Một số c&aacute;c Dịch vụ của ch&uacute;ng t&ocirc;i c&oacute; sẵn tr&ecirc;n c&aacute;c thiết bị di động. Kh&ocirc;ng sử dụng c&aacute;c Dịch vụ đ&oacute; theo c&aacute;ch khiến bạn bị mất tập trung v&agrave; ngăn cản bạn tu&acirc;n thủ c&aacute;c luật về an to&agrave;n hoặc giao th&ocirc;ng.</p>', '2020-01-02 08:32:11', '2020-01-02 08:32:11');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `para_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `para_name`, `created_at`, `updated_at`) VALUES
(1, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td style=\"text-align:center\">&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>', '2020-01-02 05:03:47', '2020-02-20 09:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_category_id` int(11) NOT NULL DEFAULT '0',
  `pro_price` int(11) NOT NULL DEFAULT '0',
  `pro_author_id` int(11) NOT NULL DEFAULT '0',
  `pro_sale` tinyint(4) NOT NULL DEFAULT '0',
  `pro_active` tinyint(4) NOT NULL DEFAULT '1',
  `pro_hot` tinyint(4) NOT NULL DEFAULT '0',
  `pro_view` int(11) NOT NULL DEFAULT '0',
  `pro_description` text COLLATE utf8mb4_unicode_ci,
  `pro_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_keyword_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pro_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_content` longtext COLLATE utf8mb4_unicode_ci,
  `pro_pay` tinyint(4) NOT NULL DEFAULT '1',
  `pro_number` tinyint(4) NOT NULL DEFAULT '1',
  `pro_total_rating` int(11) NOT NULL DEFAULT '0' COMMENT 'Tổng số đánh giá',
  `pro_total_number` int(11) NOT NULL DEFAULT '0' COMMENT 'Tổng số điểm đánh giá',
  `pro_parameter` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pro_name`, `pro_slug`, `pro_category_id`, `pro_price`, `pro_author_id`, `pro_sale`, `pro_active`, `pro_hot`, `pro_view`, `pro_description`, `pro_avatar`, `pro_description_seo`, `pro_keyword_seo`, `created_at`, `updated_at`, `pro_title_seo`, `pro_content`, `pro_pay`, `pro_number`, `pro_total_rating`, `pro_total_number`, `pro_parameter`) VALUES
(9, 'Điện thoại Samsung Galaxy Note 5', 'dien-thoai-samsung-galaxy-note-5', 3, 5600000, 0, 2, 1, 1, 0, 'Điện thoại Samsung Galaxy Note 5', '2019-11-20__nokia-61-plus-3-400x460.png', 'Điện thoại Samsung Galaxy Note 5', NULL, '2019-11-20 09:11:15', '2020-01-17 10:01:23', 'Điện thoại Samsung Galaxy Note 5', '<p>Nội dung h&igrave;nh ảnh test&nbsp;Điện thoại Samsung Galaxy Note 5</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/nokia-61-plus-3-400x460.png\" style=\"height:460px; width:400px\" /></p>', 2, 1, 1, 4, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;1080x1920</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>24mp</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>14mp</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>4000mah</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>4gb</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>snapDragon 660</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>1080p 60fps</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(10, 'Iphone 11', 'iphone-11', 5, 21990000, 0, 50, 1, 1, 0, 'Khuyến mại\r\nCó hỗ trợ trả góp\r\nMáy mới 100% do Apple Việt Nam phân phối chính hãng\r\nKhuyến mãi mua kèm tai nghe Airpods2 case sạc thường chính hãng VN/A với giá ưu đãi 3.690.000\r\nBảo hành\r\n- Bảo hành chính hãng Apple Việt Nam 12 tháng ..', '2019-11-20__iphone-11-red-2-400x460.png', 'Iphone 11', NULL, '2019-11-20 09:39:51', '2020-02-13 07:04:04', 'Iphone 11', '<p>Phone 11 - si&ecirc;u phẩm được mong chờ nhất năm 2019 của Apple chuẩn bị ra mắt. Với những cải tiến vượt trội, phi&ecirc;n bản iPhone mới nhất hứa hẹn sẽ l&agrave;m b&ugrave;ng nổ thị trường c&ocirc;ng nghệ.</p>\r\n\r\n<h2>Thiết kế cực kỳ ấn tượng với m&agrave;n h&igrave;nh c&oacute; độ ph&acirc;n giải cao</h2>\r\n\r\n<p><img src=\"https://hoanghamobile.com/Uploads/Originals/2019/10/28/201910281022255722_mau-sac-iphone-11.jpg;width=650\" /></p>\r\n\r\n<p>iPhone 11 vẫn sở hữu thiết kế tr&agrave;n viền với &ldquo;tai thỏ&rdquo; giống iPhone X. Viền bezel ph&iacute;a tr&ecirc;n v&agrave; dưới cũng được l&agrave;m gọn lại nhằm tối đa m&agrave;n h&igrave;nh sử dụng. C&ugrave;ng với đ&oacute;, 4 g&oacute;c của m&aacute;y cũng được bo tr&ograve;n nhẹ tạo cảm gi&aacute;c chắc chắn khi cầm tr&ecirc;n tay. Mặt lưng iPhone 11 l&agrave;m từ chất liệu k&iacute;nh n&ecirc;n rất sang trọng, tinh tế. Kh&aacute;c với c&aacute;c d&ograve;ng iPhone trước, sản phẩm n&agrave;y sẽ c&oacute; 6 m&agrave;u bản bạc, v&agrave;ng, xanh l&aacute;, trắng, đen đỏ.</p>\r\n\r\n<p>C&ocirc;ng nghệ m&agrave;n h&igrave;nh LCD Retina mang đến chất lượng tốt nhất. Độ ph&acirc;n giải m&agrave;n h&igrave;nh 1125 x 2436 pixels hiển thị m&agrave;u sắc ch&iacute;nh x&aacute;c v&agrave; cho h&igrave;nh ảnh sắc n&eacute;t. M&agrave;n h&igrave;nh rộng 6.1 inch gi&uacute;p người sử dụng thoải m&aacute;i trải nghiệm xem phim, lướt web&hellip; Đặc biệt Apple đ&atilde; trang bị tần số qu&eacute;t từ 90 đến 120 Hz với 463 điểm m&agrave;u.</p>\r\n\r\n<h2>Camera độc đ&aacute;o n&acirc;ng tầm chụp ảnh chuy&ecirc;n nghiệp</h2>\r\n\r\n<p><img src=\"https://icdn3.digitaltrends.com/image/digitaltrends/iphone11-review-1200x630-c-ar1.91.jpg\" /></p>\r\n\r\n<p>iPhone 11 vẫn sở hữu 2 camera nhưng thay v&igrave; đặt dọc như iPhone X th&igrave; ch&uacute;ng lại được sắp xếp theo h&igrave;nh vu&ocirc;ng. Đ&egrave;n flash cũng được đặt trong h&igrave;nh vu&ocirc;ng n&agrave;y. Trong đ&oacute; c&oacute; một camera ch&iacute;nh với độ ph&acirc;n giải 12MP gi&uacute;p bạn c&oacute; được những bức ảnh cực đẹp. Camera thứ hai c&oacute; g&oacute;c si&ecirc;u rộng 12 MP, ti&ecirc;u cự 13mm cho g&oacute;c chụp 120 độ.</p>\r\n\r\n<p>C&aacute;c thuật to&aacute;n xử l&yacute; c&ugrave;ng được Apple n&acirc;ng cấp gồm chụp ch&acirc;n dung Potrait Mode, chụp ch&acirc;n dung đen trắng Highkey. Đồng thời tự động chuyển sang t&iacute;nh năng chụp đ&ecirc;m Night Mode khi nhận thấy thiếu &aacute;nh s&aacute;ng.</p>\r\n\r\n<p>Camera selfie được n&acirc;ng cấp với độ ph&acirc;n giải l&ecirc;n đến 12MP. Đồng thời đ&egrave;n flash cũng được n&acirc;ng độ s&aacute;ng v&agrave; v&ugrave;ng chiếu s&aacute;ng rộng hơn. Nhờ đ&oacute;, bạn sẽ thoải m&aacute;i chụp ảnh kể cả ở những nơi &aacute;nh s&aacute;ng yếu. B&ecirc;n cạnh đ&oacute;, chế độ slow-motiton hứa hẹn sẽ trở th&agrave;nh một t&iacute;nh năng độc đ&aacute;o. Mỗi một sản phẩm mới của Apple đều được n&acirc;ng cấp camera nhằm tạo ra những trải nghiệm mới th&uacute; vị hơn cho người sử dụng.</p>\r\n\r\n<h2>Pin dung lượng 3110 mAh n&acirc;ng thời gian nhiều hơn 1 giờ so với iPhone XR</h2>\r\n\r\n<p>iPhone 11 ch&iacute;nh h&atilde;ng được trang bị pin c&oacute; dung lượng 3110 mAh. Với dung lượng như vậy th&igrave; chỉ một lần sạc l&agrave; c&oacute; thể sử dụng thoải m&aacute;i. Th&ecirc;m một điểm cộng nữa cho sản phẩm n&agrave;y đ&oacute; ch&iacute;nh l&agrave; c&ocirc;ng nghệ sạc kh&ocirc;ng d&acirc;y 15W. So với sạc kh&ocirc;ng d&acirc;y 7.5W hiện tại, sạc kh&ocirc;ng d&acirc;y 15W tăng tốc độ sạc l&ecirc;n 15% v&agrave; hiệu quả năng lượng cao hơn 30%. Mặc d&ugrave; l&agrave; c&ocirc;ng nghệ sạc nhanh nhưng vẫn đảm bảo iPhone kh&ocirc;ng qu&aacute; n&oacute;ng v&agrave; giảm 3 độ trong l&uacute;c sạc.</p>\r\n\r\n<h2>Hiệu năng tối ưu với bộ xử l&yacute; Apple A13</h2>\r\n\r\n<p><img src=\"https://hoanghamobile.com/Uploads/Originals/2019/10/28/201910281023484679_iPhone-11-17-1568148638_680x0.jpg;width=650\" /></p>\r\n\r\n<p>Bộ xử l&yacute; A13 được sản xuất độc quyền cho iPhone 11 nhằm tạo hiệu năng mạnh mẽ, tiết kiệm pin hiệu quả. Hơn nữa, hệ điều h&agrave;nh iOS v13.0 sẽ tăng tốc độ mở ứng dụng 40%, tốc độ hiển thị b&agrave;n ph&iacute;m 50%, tốc độ camera 70%. C&ocirc;ng nghệ Wi-fi 6 mới nhất cũng được đưa v&agrave;o iPhone 11 nhằm tăng tốc độ kết nối internet tới 40%.</p>\r\n\r\n<p>Để thay thế cho 5G, Apple đ&atilde; ph&aacute;t triển c&ocirc;ng nghệ Modified Polyimide (MPI) thay v&igrave; Liquid Crystal Polymer (LCP). N&acirc;ng cấp băng th&ocirc;ng si&ecirc;u rộng gi&uacute;p kết nối mạng nhanh hơn.</p>\r\n\r\n<p>Face ID cũng được cải thiện khi c&oacute; thể ph&acirc;n biệt tiền cảnh v&agrave; &aacute;nh xạ khu&ocirc;n mặt để mở kh&oacute;a nhanh ch&oacute;ng hơn. Apple đ&atilde; tăng cường &aacute;nh s&aacute;ng của đ&egrave;n chiếu nhằm tr&aacute;nh &aacute;nh s&aacute;ng của m&ocirc;i trường t&aacute;c động l&agrave;m ảnh hưởng đến hoạt động của Face&nbsp; ID. Nhờ đ&oacute; n&acirc;ng cao t&iacute;nh bảo mật cho sản phẩm.</p>', 12, 7, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(11, 'Điện thoại Xiaomi Mi 9 SE', 'dien-thoai-xiaomi-mi-9-se', 4, 7400000, 0, 0, 1, 1, 0, 'KHUYẾN MÃI\r\nGiảm thêm 5% (430.000₫) cho khách mua online có sinh nhật trong tháng 11 Xem chi tiết *\r\nGiảm ngay 1 triệu (đã trừ vào giá)\r\n* Không áp dụng khi mua trả góp 0%', '2019-11-21__xiaomi-mi-9-se-blue-18thangbh-400x460.png', 'Điện thoại Xiaomi Mi 9 SE', NULL, '2019-11-21 10:13:27', '2020-02-13 07:13:09', 'Điện thoại Xiaomi Mi 9 SE', '<h2><strong><a href=\"https://cellphones.com.vn/xiaomi-mi-9-se.html\" target=\"_blank\" title=\"Xiaomi Mi 9 SE\">Điện thoại Xiaomi Mi 9 SE&nbsp;</a>độc đ&aacute;o với thiết kế viền m&agrave;n h&igrave;nh cong 2.5D c&ugrave;ng dung lượng pin khủng</strong></h2>\r\n\r\n<p><em><strong>(*) Điện thoại Xiaomi ch&iacute;nh h&atilde;ng, c&oacute; sẵn tiếng Việt, đầy đủ ứng dụng của Google. Bạn c&oacute; thể sử dụng ngay b&igrave;nh thường. Kh&aacute;c với c&aacute;c m&aacute;y h&agrave;ng x&aacute;ch tay: kh&ocirc;ng c&oacute; sẵn rom tiếng Việt, chặn c&aacute;c ứng dụng của Google</strong></em></p>\r\n\r\n<p><em>Trong thời đại ng&agrave;y nay, để sở hữu cho m&igrave;nh một chiếc&nbsp;<a href=\"https://cellphones.com.vn/mobile.html\" target=\"_blank\" title=\"Điện thoại smartphone chính hãng\"><strong>smartphone</strong></a>&nbsp;kh&ocirc;ng c&ograve;n l&agrave; điều kh&oacute; khăn nữa. Những l&agrave;m thế n&agrave;o để c&oacute; được một chiếc smartphone với gi&aacute; cả hợp l&iacute; m&agrave; vẫn đảm bảo c&aacute;c tiện &iacute;ch vượt trội mới l&agrave; điều m&agrave; người d&ugrave;ng cần đến. Để đ&aacute;p ứng điều đ&oacute;&nbsp;<a href=\"https://cellphones.com.vn/mobile/xiaomi.html\" target=\"_blank\" title=\"Điện thoại Xiaomi chính hãng\"><strong>Xiaomi</strong></a>&nbsp;đ&atilde; cho ra mắt&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;với những t&iacute;nh năng hứa hẹn sẽ l&agrave;m h&agrave;i l&ograve;ng người d&ugrave;ng.</em></p>\r\n\r\n<p><em>Tham khảo th&ecirc;m&nbsp;<strong><a href=\"https://cellphones.com.vn/xiaomi-mi-9t.html\" target=\"_blank\">gi&aacute; Mi 9T</a></strong>&nbsp;- Smartphone tầm trung mới nhất của Xiaomi</em></p>\r\n\r\n<h3><strong>Xiaomi Mi 9 SE c&oacute; m&agrave;n h&igrave;nh giọt nước bắt kịp xu hướng c&ugrave;ng hiệu ứng gradient đẹp mắt</strong></h3>\r\n\r\n<p>Về ngoại h&igrave;nh, m&aacute;y kh&ocirc;ng c&oacute; nhiều kh&aacute;c biệt so với người anh em&nbsp;<a href=\"https://cellphones.com.vn/xiaomi-mi-9.html\" target=\"_blank\" title=\"Điện thoại Xiaomi Mi 9\">Xiaomi Mi 9</a>. K&iacute;ch thước của m&agrave;n h&igrave;nh tr&ecirc;n m&aacute;y chỉ c&ograve;n 5.97 inch c&ugrave;ng viền m&agrave;n h&igrave;nh được thiết kế cong 2.5D rất hiện đại. Song song đ&oacute;, bạn sẽ vẫn c&oacute; cơ hội trải nghiệm&nbsp;thiết kế mặt lưng với hiệu ứng đổi m&agrave;u gradient đẹp mắt tr&ecirc;n&nbsp;<strong>Mi 9 SE</strong>. Hiệu ứng gradient l&agrave; sự kết hợp giữa hai hoặc nhiều m&agrave;u sắc kh&aacute;c nhau, được biến chuyển một c&aacute;ch mềm mại m&agrave; kh&ocirc;ng c&oacute; gi&aacute;n đoạn, tr&ocirc;ng nổi bật hơn bất k&igrave; m&agrave;u đơn sắc n&agrave;o kh&aacute;c<strong><em>.</em></strong><strong><em>&nbsp;</em></strong></p>\r\n\r\n<p><strong><em><img alt=\"Màn hình với thiết kế hình giọt nước bắt kịp xu hướng \" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/xiaomi/xiaomi-mi-9-se-1_2.jpg\" /></em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Xiaomi Mi 9 SE</strong>&nbsp;vẫn giữ nguy&ecirc;n kiểu thiết kế m&agrave;n h&igrave;nh giọt nước rất được ưa chuộng tr&ecirc;n thị trường, qua đ&oacute; tỉ lệ m&agrave;n h&igrave;nh ở mặt trước được tăng l&ecirc;n tới&nbsp;90.47%. C&aacute;c g&oacute;c cạnh của m&aacute;y được bo cong cho bạn cảm gi&aacute;c cầm nắm thoải m&aacute;i cũng như tạo cho vẻ ngo&agrave;i của chiếc điện thoại&nbsp;mềm mại hơn.</p>\r\n\r\n<h3><strong>Cụm 3 camera hiện đại tr&ecirc;n Mi 9 SE với cảm biến ch&iacute;nh l&ecirc;n đến 48 MP</strong></h3>\r\n\r\n<p>Tương tự như Xiaomi Mi 9 chiếc&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;vẫn được trang bị cụm 3 camera v&ocirc; c&ugrave;ng hiện đại v&agrave; đẹp mắt. Khiến việc chụp ảnh của bạn trở n&ecirc;n dễ d&agrave;ng hơn bao giờ hết, cũng như đ&aacute;p ứng được c&aacute;c y&ecirc;u cầu chụp ảnh kh&aacute;c nhau của người d&ugrave;ng.&nbsp;</p>\r\n\r\n<p><img alt=\"Cụm 3 camera hiện đại với cảm biến chính lên đến 48 MP\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/xiaomi/xiaomi-mi-9-se-2_2.jpg\" /></p>\r\n\r\n<p>Cụm camera sau gồm cảm biến ch&iacute;nh l&ecirc;n đến 48 MP v&agrave; 2 cảm biến phụ 8 MP v&agrave; 13 MP. Mặt trước vẫn l&agrave; camera 20 MP được t&iacute;ch hợp chế độ l&agrave;m đẹp cho bạn thoải m&aacute;i cho bạn&nbsp;<a href=\"https://www.lifewire.com/what-is-a-selfie-3485946\" rel=\"nofollow\" target=\"_blank\" title=\"Selfie là gì\">selfie</a>&nbsp;với bạn b&egrave;. Do đ&oacute; người d&ugrave;ng ho&agrave;n to&agrave;n c&oacute; thể mong chờ một chiếc&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;với khả năng chụp ảnh sắc n&eacute;t v&agrave; v&ocirc; c&ugrave;ng ho&agrave;n hảo.</p>\r\n\r\n<h3><strong>Xiaomi 9 SE - Smartphone đầu ti&ecirc;n được trang bị&nbsp;<a href=\"https://cellphones.com.vn/sforum/qualcomm-chinh-thuc-ra-mat-snapdragon-712-chipset-tam-trung-cho-hieu-nang-cao-cap\" target=\"_blank\" title=\"Qualcomm chính thức ra mắt Snapdragon 712\">Snapdragon 712</a></strong></h3>\r\n\r\n<p>Nếu như năm ngo&aacute;i chiếc<a href=\"https://cellphones.com.vn/xiaomi-mi-8-se-6-gb-ram-chinh-hang.html\" target=\"_blank\" title=\"Điện thoại Xiaomi Mi 8 SE\">&nbsp;Xiaomi Mi 8 SE&nbsp;</a>l&agrave; chiếc smartphone đầu ti&ecirc;n được trang bị con chip&nbsp;<a href=\"https://www.qualcomm.com/products/snapdragon-710-mobile-platform\" rel=\"nofollow\" target=\"_blank\" title=\"Kiểm thử hiệu năng Snapdragon 710\">Snapdragon 710</a>&nbsp;th&igrave; chiếc&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;cũng l&agrave; chiếc m&aacute;y đầu ti&ecirc;n được trang bị con chip&nbsp;Snapdragon 712&nbsp;8 nh&acirc;n 64-bit. Với con chip mới n&agrave;y hiệu năng của m&aacute;y tốt hơn 10% so với thế hệ cũ, nhờ đ&oacute; m&agrave; Xiaomi Mi 9 SE th&aacute;ch thức mọi loại game nặng nhất.&nbsp;</p>\r\n\r\n<p><img alt=\"Chiếc smartphone đầu tiên được trang bị Snapdragon 712\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/xiaomi/xiaomi-mi-9-se-3_2.jpg\" /></p>\r\n\r\n<p>Ngo&agrave;i ra,&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;c&ograve;n được trang bị RAM l&ecirc;n tới 6 GB c&ugrave;ng bộ nhớ trong 64 GB khiến việc lưu trữ dữ liệu của bạn trở n&ecirc;n thoải m&aacute;i hơn bao giờ hết.</p>\r\n\r\n<h3><strong>Bảo mật v&acirc;n tay dưới m&agrave;n h&igrave;nh c&ugrave;ng dung lượng pin l&ecirc;n đến 3.070 mAh</strong></h3>\r\n\r\n<p>Bạn sẽ v&ocirc; c&ugrave;ng bất ngờ khi&nbsp;<em><strong>Mi 9 SE</strong></em>&nbsp;được Xiaomi đặc biệt trang bị c&ocirc;ng nghệ&nbsp;bảo mật v&acirc;n tay dưới m&agrave;n h&igrave;nh tiện lợi. Ch&iacute;nh điều n&agrave;y đ&atilde; l&agrave;m tăng th&ecirc;m gi&aacute; trị v&agrave; đẳng cấp của chiếc điện thoại&nbsp;v&agrave; cũng l&agrave; một trong những điểm l&agrave;m h&agrave;i l&ograve;ng người d&ugrave;ng.</p>\r\n\r\n<p><img alt=\"Được trang bị công nghệ bảo mật vân tay dưới màn hình cùng dung lượng pin lên đến 3.070 mAh\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/xiaomi/xiaomi-mi-9-se-4.jpg\" /></p>\r\n\r\n<p>Dung lượng pin của&nbsp;<strong>Xiaomi 9 SE</strong>&nbsp;l&ecirc;n tới&nbsp;3.070 mAh cho bạn thoải m&aacute;i xem phim, nghe nhạc hay li&ecirc;n lạc m&agrave; kh&ocirc;ng lo lắng bị gi&aacute;n đoạn do hết pin giữa chừng. Đặc biệt m&aacute;y c&ograve;n hỗ trợ sạc nhanh c&ocirc;ng suất 18W cho bạn r&uacute;t ngắn thời gian sạc pin đ&aacute;ng kể.</p>\r\n\r\n<h3><strong>Mua&nbsp;</strong><strong>Xiaomi Mi 9 SE</strong>&nbsp;<strong>ch&iacute;nh h&atilde;ng, chất lượng với gi&aacute; rẻ tại&nbsp;<a href=\"https://cellphones.com.vn/\" target=\"_blank\" title=\"Hệ thống bán lẻ điện thoại chính hãng\">CellphoneS</a></strong></h3>\r\n\r\n<p>Nếu bạn đang muốn t&igrave;m mua một chiếc&nbsp;<strong>Xiaomi Mi 9 SE</strong>&nbsp;<strong>ch&iacute;nh h&atilde;ng</strong>&nbsp;với gi&aacute; tốt nhất th&igrave; h&atilde;y đến ngay CellphoneS &ndash; hệ thống chuy&ecirc;n b&aacute;n lẻ điện thoại, m&aacute;y t&iacute;nh bảng v&agrave; phụ kiện ch&iacute;nh h&atilde;ng tr&ecirc;n to&agrave;n quốc. Tại đ&acirc;y, bạn sẽ được tư vấn chi tiết về sản phẩm c&ugrave;ng th&aacute;i độ phục vụ tận t&igrave;nh của c&aacute;c nh&acirc;n vi&ecirc;n c&oacute; nhiều năm kinh nghiệm trong lĩnh vực c&ocirc;ng nghệ với mục ti&ecirc;u nhằm c&oacute; thể lựa chọn những sản phẩm ph&ugrave; hợp nhất mong muốn của kh&aacute;ch h&agrave;ng.&nbsp;Khi mua&nbsp;<strong>Mi 9 SE</strong>&nbsp;tại đ&acirc;y, bạn sẽ được bảo h&agrave;nh 12 th&aacute;ng tại trung t&acirc;m bảo h&agrave;nh ch&iacute;nh h&atilde;ng v&agrave; đổi mới trong v&ograve;ng 30 ng&agrave;y đầu ti&ecirc;n sau khi mua h&agrave;ng nếu c&oacute; lỗi nh&agrave; sản xuất. Ngo&agrave;i ra, ch&uacute;ng t&ocirc;i c&ograve;n c&oacute; những ưu đ&atilde;i hấp dẫn kh&aacute;c cho Smember khi mua h&agrave;ng tại CellphoneS.&nbsp;&nbsp;</p>', 7, 6, 1, 3, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(12, 'Sam sung galaxy s10+(250Gb)', 'sam-sung-galaxy-s10250gb', 3, 29000000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy S10+ (512GB)', '2019-12-28__sieu-pham-galaxy-s-moi-2-512gb-black-600x600.jpg', NULL, NULL, '2019-12-28 08:29:21', '2020-02-13 07:17:41', 'Sam sung galaxy s10+(250Gb)', '<h3>Thiết kế lột x&aacute;c, tạo xu thế mới</h3>\r\n\r\n<p>C&aacute;i nh&igrave;n sẽ l&agrave; điểm ấn tượng đầu ti&ecirc;n tr&ecirc;n&nbsp;Samsung Galaxy S10 khi Samsung mang đến cho ch&uacute;ng ta một thiết bị cao cấp v&agrave; sang trọng như m&oacute;n đồ trang sức.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-2-1.jpg\" onclick=\"return false;\"><img alt=\"Thiết kế điện thoại Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-2-1.jpg\" title=\"Thiết kế điện thoại Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Điểm đặc biệt l&agrave; viền tr&ecirc;n v&agrave; dưới tr&ecirc;n chiếc smartphone n&agrave;y được l&agrave;m mỏng hơn, ấn tượng hơn rất nhiều so với Galaxy S9 hay Note 9.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-7.jpg\" onclick=\"return false;\"><img alt=\"Mặt trước điện thoại Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-7.jpg\" title=\"Mặt trước điện thoại Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Để l&agrave;m được điều đ&oacute;, Samsung đ&atilde; đưa cụm camera v&agrave;o b&ecirc;n trong m&agrave;n h&igrave;nh, một điều m&agrave; chưa một h&atilde;ng n&agrave;o l&agrave;m trước đ&oacute;.</p>\r\n\r\n<p>Thiết kế mới mang lại cho người d&ugrave;ng trải nghiệm mới mẻ v&agrave;&nbsp;c&oacute; lẽ Samsung sẽ l&agrave; h&atilde;ng đầu ti&ecirc;n khơi m&agrave;o cho phong c&aacute;ch thiết kế camera trong m&agrave;n h&igrave;nh sau n&agrave;y.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-10.jpg\" onclick=\"return false;\"><img alt=\"Màn hình điện thoại Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-10.jpg\" title=\"Màn hình điện thoại Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Ngo&agrave;i ra m&agrave;n h&igrave;nh của S10 được cải tiến nhờ tấm nền&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/man-hinh-dynamic-amoled-la-gi-no-co-cong-dung-gi-noi-bat-1153075\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">Dynamic AMOLED</a>&nbsp;c&ugrave;ng&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/hdr10-la-gi-vi-sao-no-lam-cho-man-hinh-galaxy-s10-dep-hon--1151189\" target=\"_blank\" title=\"Tìm hiểu về công nghệ HDR10+\" type=\"Tìm hiểu về công nghệ HDR10+\">c&ocirc;ng nghệ HDR10+</a>&nbsp;gi&uacute;p h&igrave;nh ảnh s&aacute;ng, rực rỡ hơn, tối ưu mọi chi tiết h&igrave;nh ảnh được hiển thị.</p>\r\n\r\n<p>M&agrave;n h&igrave;nh S10 c&ograve;n đặc biệt ở thiết kế, chứa camera selfie nằm b&ecirc;n trong m&agrave;n h&igrave;nh độc nhất v&ocirc; nhị. Kiểu thiết kế n&agrave;y được Samsung ưu &aacute;i gọi l&agrave; &quot;<a href=\"https://www.thegioididong.com/hoi-dap/tong-hop-cac-loai-man-hinh-vo-cuc-moi-tren-dien-th-1150900#infinity-o\" target=\"_blank\" title=\"Tìm hiểu công nghệ màn hình Infinity-O trên điện thoại Samsung\" type=\"Tìm hiểu công nghệ màn hình Infinity-O trên điện thoại Samsung\">Infinity-O</a>&quot;.</p>\r\n\r\n<h3><a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-quet-van-tay-song-sieu-am-la-gi-912419\" target=\"_blank\" title=\"Tìm hiểu công nghệ vân tay siêu âm\" type=\"Tìm hiểu công nghệ vân tay siêu âm\">Cảm biến v&acirc;n tay si&ecirc;u &acirc;m</a>&nbsp;b&ecirc;n dưới m&agrave;n h&igrave;nh</h3>\r\n\r\n<p>Galaxy S10 l&agrave; những điện thoại đầu ti&ecirc;n sử dụng c&ocirc;ng nghệ v&acirc;n tay si&ecirc;u &acirc;m dưới m&agrave;n h&igrave;nh của Qualcomm, n&oacute; sử dụng s&oacute;ng &acirc;m để đọc bản in ng&oacute;n tay bạn.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-6.jpg\" onclick=\"return false;\"><img alt=\"Vân tay siêu âm Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-6.jpg\" title=\"Vân tay siêu âm Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>C&ocirc;ng nghệ n&agrave;y c&oacute; thể hoạt động bằng cả trong điều kiện tay của bạn d&iacute;nh nước, kem dưỡng da v&agrave; dầu mỡ, v&agrave;o ban đ&ecirc;m hoặc dưới &aacute;nh s&aacute;ng ban ng&agrave;y.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-9.jpg\" onclick=\"return false;\"><img alt=\"Vân tay siêu âm Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-9.jpg\" title=\"Vân tay siêu âm Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Theo Samsung th&igrave;&nbsp;n&oacute; nhanh hơn v&agrave; an to&agrave;n hơn nhiều so với cảm biến v&acirc;n tay quang học m&agrave; bạn đ&atilde; thấy ở c&aacute;c điện thoại kh&aacute;c trước đ&acirc;y.</p>\r\n\r\n<p>Xem th&ecirc;m:&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/tren-tay-danh-gia-chi-tiet-sieu-pham-samsung-galaxy-s10-va-galaxy-s10--1151126\" target=\"_blank\" title=\"Trên tay, đánh giá chi tiết siêu phẩm Samsung Galaxy S10 và Galaxy S10+\" type=\"Trên tay, đánh giá chi tiết siêu phẩm Samsung Galaxy S10 và Galaxy S10+\">Tr&ecirc;n tay, đ&aacute;nh gi&aacute; chi tiết si&ecirc;u phẩm Samsung Galaxy S10 v&agrave; Galaxy S10+</a></p>\r\n\r\n<h3>Giờ d&ograve;ng Galaxy S đ&atilde; c&oacute; tới 3 camera</h3>\r\n\r\n<p>Tr&ecirc;n chiếc&nbsp;<a href=\"https://www.thegioididong.com/dtdd-samsung\" target=\"_blank\" title=\"Tham khảo các dòng điện thoại Samsung chính hãng\">điện thoại Samsung</a>&nbsp;n&agrave;y th&igrave; h&atilde;ng đ&atilde; đầu tư kh&aacute; mạnh mẽ về camera khi kh&ocirc;ng chỉ c&oacute;&nbsp;nguy&ecirc;n cụm camera k&eacute;p của&nbsp;<a href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-note-9\" target=\"_blank\" title=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng\">Note 9</a>&nbsp;m&agrave; c&ograve;n c&oacute; một cảm biến g&oacute;c rộng gi&uacute;p cung cấp trường nh&igrave;n l&ecirc;n đến 123 độ. Bộ camera của m&aacute;y gồm cảm biến ch&iacute;nh 12 MP v&agrave; 2 cảm biến phụ 12 MP + 16 MP.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-4.jpg\" onclick=\"return false;\"><img alt=\"Cụm camera sau Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-4.jpg\" title=\"Cụm camera sau Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Samsung c&ograve;n cung cấp th&ecirc;m t&iacute;nh năng Super Steady Video để hỗ trợ chống rung trong c&aacute;c cảnh quay thể thao, hay t&iacute;nh năng quay video HDR10+ với h&igrave;nh ảnh sinh động hơn, độ s&acirc;u ảnh tốt hơn.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-11-1.jpg\" onclick=\"return false;\"><img alt=\"Giao diện camera trên Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-11-1.jpg\" title=\"Giao diện camera trên Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Galaxy S10 cũng c&oacute; Chế độ Instagram, v&agrave; bạn c&oacute; thể chia sẻ ảnh tr&ecirc;n Instagram với một lần chạm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s104.jpg\" />Ảnh chụp g&oacute;c rộng tr&ecirc;n Samsung S10</p>\r\n\r\n<p><img src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s103.jpg\" />Ảnh chụp g&oacute;c thường tr&ecirc;n Samsung S10</p>\r\n\r\n<p><img src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s102.jpg\" />Ảnh chụp bằng camera tele tr&ecirc;n Samsung S10</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ở mặt trước, Galaxy S10+ c&oacute; camera 10 MP Dual Pixel với khả năng tự động lấy n&eacute;t v&agrave; quay video với độ ph&acirc;n giải l&ecirc;n tới 4K.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s105.jpg\" onclick=\"return false;\"><img alt=\"Ảnh selfie trên điện thoai Samsung Galaxy S10\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s105.jpg\" title=\"Ảnh selfie trên điện thoai Samsung Galaxy S10\" /></a></p>\r\n\r\n<h3>Hiệu năng mạnh mẽ h&agrave;ng đầu hiện nay</h3>\r\n\r\n<p>Samsung Galaxy S10&nbsp;được trang bị vi xử l&yacute; Exynos 9820 cho hiệu năng nhanh hơn đến 30% v&agrave; cho tốc độ xử l&yacute; đồ họa nhanh hơn đến 37% so với thế hệ trước.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-12.jpg\" onclick=\"return false;\"><img alt=\"Chơi game trên Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-12.jpg\" title=\"Chơi game trên Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>V&agrave; điều đặc biệt trong đ&oacute; l&agrave; t&iacute;nh năng quản l&yacute; hiệu suất th&ocirc;ng minh tr&ecirc;n 2 thiết bị n&agrave;y, tức l&agrave; m&aacute;y sẽ ph&acirc;n t&iacute;ch v&agrave; thống k&ecirc; c&aacute;c ứng dụng sử dụng nhiều, từ đ&oacute; tập trung t&agrave;i nguy&ecirc;n cho ch&uacute;ng.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-white-8.jpg\" onclick=\"return false;\"><img alt=\"Giao diện Android trên Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-white-8.jpg\" title=\"Giao diện Android trên Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Điều n&agrave;y gi&uacute;p tối ưu t&agrave;i nguy&ecirc;n CPU, RAM, v&agrave; thiết bị của bạn sẽ hoạt động nhanh hơn, trơn tru hơn ở mọi l&uacute;c, mọi thời điểm. Galaxy S10 xứng tầm đẳng cấp, mọi thể loại game 3D, game nặng như Asphalt 8, PUBG,... cũng kh&ocirc;ng l&agrave; trở ngại cho m&aacute;y.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s101.jpg\" onclick=\"return false;\"><img alt=\"Điểm Antutu Benchmark trên Samsung Galaxy S10\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s101.jpg\" title=\"Điểm Antutu Benchmark trên Samsung Galaxy S10\" /></a></p>\r\n\r\n<h3>Dung lượng pin lớn, khả năng sạc ngược độc đ&aacute;o</h3>\r\n\r\n<p>Samsung Galaxy S10 sở hữu vi&ecirc;n pin c&oacute; dung lượng&nbsp;3.400 mAh hứa hẹn c&oacute; thể đ&aacute;p ứng cho bạn nhu cầu sử dụng trong khoảng 1 ng&agrave;y.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/sieu-pham-galaxy-s-white9.jpg\" onclick=\"return false;\"><img alt=\"Thời lượng pin của điện thoại Samsung Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/sieu-pham-galaxy-s-white9.jpg\" title=\"Thời lượng pin của điện thoại Samsung Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>M&aacute;y cũng hỗ trợ c&ocirc;ng nghệ sạc nhanh kh&ocirc;ng d&acirc;y thế hệ 2 sẽ gi&uacute;p tiết kiệm tối đa thời gian sạc.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/images/42/161554/samsung-galaxy-s10-powershare.jpg\" onclick=\"return false;\"><img alt=\"Tính năng PowerShare trên Galaxy S10 chính hãng\" src=\"https://cdn.tgdd.vn/Products/Images/42/161554/samsung-galaxy-s10-powershare.jpg\" title=\"Tính năng PowerShare trên Galaxy S10 chính hãng\" /></a></p>\r\n\r\n<p>Điều đặc biệt lần đầu ti&ecirc;n Samsung trang bị ch&iacute;nh l&agrave; khả năng&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/tinh-nang-powershare-tren-galaxy-s10-la-gi-lam-sao-de-su-dung-no-1151089\" target=\"_blank\" title=\"Tìm hiểu tính năng sạc ngược PowerShare\" type=\"Tìm hiểu tính năng sạc ngược PowerShare\">sạc ngược kh&ocirc;ng d&acirc;y (PowerShare)</a>.</p>\r\n\r\n<p>Bạn c&oacute; thể sử dụng t&iacute;nh năng n&agrave;y để sạc cho bất kỳ thiết bị hỗ trợ sạc kh&ocirc;ng d&acirc;y kh&aacute;c, từ tai nghe kh&ocirc;ng d&acirc;y, điện thoại kh&ocirc;ng d&acirc;y đến pin dự ph&ograve;ng kh&ocirc;ng d&acirc;y...</p>', 1, 10, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(13, 'Sam sung galaxy note 9', 'sam-sung-galaxy-note-9', 3, 180000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy Note 9', '2019-12-28__samsung-galaxy-a50-black-600x600.jpg', NULL, NULL, '2019-12-28 08:32:30', '2020-02-13 10:22:10', 'Sam sung galaxy note 9', '<h2><a href=\"https://didongviet.vn/galaxy-note-9\" target=\"_blank\">Galaxy Note 9</a>&nbsp;l&agrave; mẫu smartphone được nhiều người d&ugrave;ng đ&aacute;nh gi&aacute; cao nhất về khả năng trải nghiệm thực tế. Mẫu điện thoại Note 9 th&iacute;ch hợp cho người d&ugrave;ng c&oacute; cường độ l&agrave;m việc li&ecirc;n tục, xen lẫn những ph&uacute;t gi&acirc;y giải tr&iacute; đỉnh cao.</h2>\r\n\r\n<h3><strong>Đ&aacute;nh gi&aacute; nhanh Galaxy Note 9</strong></h3>\r\n\r\n<p>Phi&ecirc;n bản&nbsp;<strong>Galaxy Note 9</strong>&nbsp;đ&atilde; được Samsung n&acirc;ng cấp hơn so với Note 8, m&agrave;n h&igrave;nh đ&atilde; tăng l&ecirc;n 6.4 inch, độ ph&acirc;n giải QuadHD+. Cụm camera k&eacute;p 12MP+12MP được đặt nằm ngang, khẩu độ f/1.5- f/2.4 c&ugrave;ng nhiều chế độ chụp h&igrave;nh mới cho h&igrave;nh ảnh được chỉnh s&aacute;ng tốt nhất, r&otilde; n&eacute;t hơn.</p>\r\n\r\n<p><strong>Cấu h&igrave;nh Note 9</strong>&nbsp;cũng được cải thiện khi được trang bị chipset Snapdragon 845 v&agrave; chip Exynos 9810 t&ugrave;y từng thị trường đi k&egrave;m với RAM dung lượng cao cho m&aacute;y hoạt động tốt m&agrave; kh&ocirc;ng bị hiện tượng giật lag. Dung lượng&nbsp;<strong>pin Galaxy Note 9</strong>&nbsp;cũng được tăng l&ecirc;n đến 4000 mAh c&ugrave;ng hệ thống l&agrave;m m&aacute;t điện thoại nhanh bằng nước cacbon gi&uacute;p thoải m&aacute;i sử dụng điện thoại trong một ng&agrave;y d&agrave;i m&agrave; kh&ocirc;ng lo về pin hay n&oacute;ng điện thoại. Ngo&agrave;i ra, c&acirc;y b&uacute;t S-PEN tr&ecirc;n Note 9 đ&atilde; được t&iacute;ch hợp Bluetooth gi&uacute;p mở rộng th&ecirc;m nhiều t&iacute;nh năng mới như chiếu slide, chụp ảnh từ xa, soạn thảo văn bản,...</p>\r\n\r\n<p><img alt=\"Thiết kế Galaxy Note 9 hiện đại, sang trọng, tạo sự đẳng cấp cho người dùng\" src=\"https://didongviet.vn/pub/media/wysiwyg/samsung-danh-muc/samsung-galaxy-note-9/thiet-ke-samsung-galaxy-note-9-didongviet.jpg\" title=\"Thiết kế Samsung Galaxy Note 9\" /></p>\r\n\r\n<p><em>Thiết kế Galaxy Note 9 hiện đại, sang trọng, tạo sự đẳng cấp cho người d&ugrave;ng</em></p>\r\n\r\n<p><strong>Galaxy Note 9 c&oacute; bao nhi&ecirc;u phi&ecirc;n bản dung lượng:</strong></p>\r\n\r\n<ul>\r\n	<li><a href=\"https://didongviet.vn/samsung-galaxy-note-9-6gb-128gb-cty\" target=\"_blank\" title=\"Galaxy Note 9 128GB\">Galaxy Note 9 128GB</a></li>\r\n	<li><a href=\"https://didongviet.vn/samsung-galaxy-note-9-8gb-512gb\" target=\"_blank\" title=\"Galaxy Note 9 512GB\">Galaxy Note 9 512GB</a></li>\r\n</ul>\r\n\r\n<p><strong>Galaxy Note 9 c&oacute; bao nhi&ecirc;u phi&ecirc;n bản m&agrave;u sắc:</strong></p>\r\n\r\n<ul>\r\n	<li>Xanh dương</li>\r\n	<li>T&iacute;m</li>\r\n	<li>Đen&nbsp;</li>\r\n	<li>M&agrave;u đồng</li>\r\n</ul>\r\n\r\n<p><img alt=\"Galaxy Note 9 chạy mượt mà và nhanh chóng nhờ cấu hình mạnh mẽ\" src=\"https://didongviet.vn/pub/media/wysiwyg/samsung-danh-muc/samsung-galaxy-note-9/cau-hinh-samsung-galaxy-note-9-didongviet.jpg\" title=\"Cấu hình Samsung Galaxy Note 9\" /></p>\r\n\r\n<p><em>Galaxy Note 9 chạy mượt m&agrave; v&agrave; nhanh ch&oacute;ng nhờ cấu h&igrave;nh mạnh mẽ</em></p>\r\n\r\n<p><strong>C&oacute; n&ecirc;n mua Galaxy Note 9 hay chờ Galaxy Note 10 mới?</strong></p>\r\n\r\n<p>Hiện tại, Samsung sắp ra mắt flagship Galaxy Note 10, vậy n&ecirc;n&nbsp;<strong>mua Samsung Note 9</strong>&nbsp;hay chờ Galaxy Note 10 ra đời v&agrave; sở hữu khiến nhiều người d&ugrave;ng boăn khoăn. Cầm nắm những đặc điểm nổi bật của Note 9 để quyết định n&ecirc;n mua Note 9 hay kh&ocirc;ng nh&eacute;.</p>\r\n\r\n<p><img alt=\"Camera Galaxy Note 9 chụp hình chất lượng, cho hình ảnh luôn rõ nét nhất\" src=\"https://didongviet.vn/pub/media/wysiwyg/samsung-danh-muc/samsung-galaxy-note-9/camera-samsung-galaxy-note-9-didongviet.jpg\" title=\"Camera Samsung Galaxy Note 9\" /></p>\r\n\r\n<p><em>Camera Galaxy Note 9 chụp h&igrave;nh chất lượng, cho h&igrave;nh ảnh lu&ocirc;n r&otilde; n&eacute;t nhất</em></p>\r\n\r\n<ul>\r\n	<li><strong>Thiết kế Galaxy Note 9</strong>&nbsp;đẹp mắt, sang trọng. Điện thoại vẫn đảm bảo những t&iacute;nh năng hiện đại thậm ch&iacute; c&ograve;n vượt trội hơn hẳn c&aacute;c d&ograve;ng smartphone hiện tại.</li>\r\n	<li>Samsung Galaxy Note 9 c&oacute; bộ nhớ trong khủng, l&ecirc;n đến 512GB v&agrave; người d&ugrave;ng c&oacute; thể mở rộng th&ecirc;m bằng thẻ nhớ ngo&agrave;i, n&acirc;ng tổng mức lưu trữ l&ecirc;n đến 1TB.</li>\r\n	<li>Thuật to&aacute;n trong camera Galaxy Note 9 c&oacute; nhiều cải tiến mang lại nhiều bức ảnh đẹp v&agrave; đậm n&eacute;t, vẫn đ&aacute;p ứng tốt nhu cầu của người d&ugrave;ng.</li>\r\n	<li>Con chip Snapdragon 845 hay&nbsp;Exynos 9810 Octa mạnh mẽ, chạy mượt m&agrave; v&agrave; nhanh ch&oacute;ng c&aacute;c t&aacute;c vụ hiện tại.</li>\r\n	<li>B&uacute;t S-Pen cũng được t&iacute;ch hợp nhiều c&ocirc;ng nghệ mới, trở th&agrave;nh một chiếc m&aacute;y t&iacute;nh thu nhỏ tiện lợi cho người d&ugrave;ng.</li>\r\n	<li>Pin dung lượng 4000 mAh, dư sức d&ugrave;ng trong một ng&agrave;y d&agrave;i.</li>\r\n	<li><strong>Gi&aacute; b&aacute;n Galaxy Note 9</strong>&nbsp;kh&aacute; rẻ ở thời điểm hiện tại, ph&ugrave; hợp với t&uacute;i tiền nhiều người, chắc chắn sẽ thấp hơn nhiều Galaxy Note 10 sắp ra mắt.</li>\r\n</ul>\r\n\r\n<p><strong>V&igrave; sao n&ecirc;n mua Samsung Galaxy Note 9 mới cũ gi&aacute; rẻ - X&aacute;ch tay Mỹ, H&agrave;n Quốc tại Di Động Việt</strong></p>\r\n\r\n<p>Di Động Việt lại l&agrave; địa chỉ đ&aacute;ng tin cậy khi&nbsp;<strong>mua Galaxy Note 9</strong>&nbsp;cũng như nhiều d&ograve;ng sản phẩm kh&aacute;c đang c&oacute; mặt tr&ecirc;n thị trường hiện nay, khi hệ thống sở hữu c&aacute;c ch&iacute;nh s&aacute;ch ưu đ&atilde;i đặc biệt sau:</p>\r\n\r\n<p><img alt=\"Mua Galaxy Note 9 tại Di Động Việt với mức giá hấp dẫn nhất\" src=\"https://didongviet.vn/pub/media/wysiwyg/samsung-danh-muc/samsung-galaxy-note-9/but-spen-samsung-galaxy-note-9-didongviet.jpg\" title=\"Bút Spen Samsung Galaxy Note 9\" /></p>\r\n\r\n<p><em>Mua Galaxy Note 9 tại Di Động Việt với mức gi&aacute; hấp dẫn nhất</em></p>', 1, 20, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(14, 'Sam sung galaxy note 11', 'sam-sung-galaxy-note-11', 3, 8000000, 0, 0, 1, 1, 0, 'Sam sung galaxy note 11', '2019-12-28__note11.jpg', NULL, NULL, '2019-12-28 08:34:29', '2020-02-13 10:20:29', 'Sam sung galaxy note 11', '<p><strong>Với gi&aacute; b&aacute;n kh&ocirc;ng ch&ecirc;nh lệch nhiều, Galaxy S20 c&ugrave;ng iPhone 11, Galaxy Note 10 sẽ l&agrave; 3 sản phẩm được nhiều người ph&acirc;n v&acirc;n chọn mua nhất.</strong></p>\r\n\r\n<p><img alt=\"\" src=\"https://vnreview.vn/image/20/38/50/2038504.jpg?t=1581449875200\" /></p>\r\n\r\n<p>Đ&uacute;ng như dự đo&aacute;n, Samsung đ&atilde; ch&iacute;nh thức giới thiệu d&ograve;ng flagship mới Galaxy S20. Trong 3 phi&ecirc;n bản S20, S20+ v&agrave; S20 Ultra th&igrave; S20 l&agrave; phi&ecirc;n bản gi&aacute; thấp nhất, ph&ugrave; hợp cho những ai cần một chiếc smartphone đủ mạnh, cao cấp nhưng kh&ocirc;ng qu&aacute; nhiều t&iacute;nh năng thừa th&atilde;i. M&aacute;y trang bị 3 camera sau, m&agrave;n h&igrave;nh Dynamic AMOLED 6.2 inch, chip xử l&yacute; Exynos 990/Snapdragon 865, RAM xGB, bộ nhớ trong xGB với dung lượng pin l&ecirc;n đến 4.000 mAh.</p>\r\n\r\n<p>Kh&ocirc;ng chỉ Samsung m&agrave; Apple cũng hướng đến những người d&ugrave;ng n&agrave;y khi ra mắt iPhone 11 v&agrave;o th&aacute;ng 9 năm ngo&aacute;i với 2 camera sau, m&agrave;n h&igrave;nh viền mỏng, Face ID v&agrave; chip xử l&yacute; A13 Bionic mạnh nhất của h&atilde;ng.</p>\r\n\r\n<p><img alt=\"\" src=\"https://vnreview.vn/image/20/38/50/2038507.jpg?t=1581449875200\" /><img alt=\"\" src=\"https://vnreview.vn/image/20/38/51/2038510.jpg?t=1581449875200\" /></p>\r\n\r\n<p><em>iPhone 11 v&agrave; Galaxy Note 10</em></p>\r\n\r\n<p>Ngo&agrave;i ra, Galaxy Note 10 sau nửa năm ra mắt vẫn l&agrave; sản phẩm được quan t&acirc;m với thiết kế nhỏ gọn, cấu h&igrave;nh ổn (Exynos 9825/Snapdragon 855, m&agrave;n h&igrave;nh Dynamic AMOLED 6.3 inch, RAM 8GB, bộ nhớ trong 256GB, 3 camera sau v&agrave; đặc biệt l&agrave; b&uacute;t cảm ứng S Pen).</p>', 2, 1, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>');
INSERT INTO `products` (`id`, `pro_name`, `pro_slug`, `pro_category_id`, `pro_price`, `pro_author_id`, `pro_sale`, `pro_active`, `pro_hot`, `pro_view`, `pro_description`, `pro_avatar`, `pro_description_seo`, `pro_keyword_seo`, `created_at`, `updated_at`, `pro_title_seo`, `pro_content`, `pro_pay`, `pro_number`, `pro_total_rating`, `pro_total_number`, `pro_parameter`) VALUES
(15, 'Sam sung galaxy tab6', 'sam-sung-galaxy-tab6', 3, 14000000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy Tab S6', '2019-12-28__sieu-pham-galaxy-s-moi-2-512gb-black-600x600.jpg', NULL, NULL, '2019-12-28 08:36:39', '2020-02-13 07:11:30', 'Sam sung galaxy tab6', '<h2><strong><a href=\"https://cellphones.com.vn/samsung-galaxy-tab-s6.html\" target=\"_blank\" title=\"Samsung Galaxy Tab S6\">M&aacute;y t&iacute;nh bảng Samsung Tab S6</a>&nbsp;&ndash;&nbsp;<a href=\"https://cellphones.com.vn/tablet.html\" target=\"_self\" title=\"Tablet chính hãng\">tablet</a>&nbsp;cao cấp của Samsung hỗ trợ b&agrave;n ph&iacute;m v&agrave; b&uacute;t cảm ứng</strong></h2>\r\n\r\n<p>Tương tự chiếc iPad Pro của Apple,&nbsp;<strong>Samsung Galaxy Tab S6</strong>&nbsp;&ndash; mẫu&nbsp;<a href=\"https://cellphones.com.vn/tablet/samsung.html\" target=\"_self\" title=\"máy tính bảng samsung\">m&aacute;y t&iacute;nh bảng Samsung</a>&nbsp;mới n&agrave;y&nbsp;cũng được hỗ trợ nhiều t&iacute;nh năng th&ocirc;ng minh khi sở hữu thiết kế mỏng nhẹ, hỗ trợ b&agrave;n ph&iacute;m v&agrave; b&uacute;t cảm ứng.</p>\r\n\r\n<h3><strong>Galaxy Tab S6 với m&agrave;n h&igrave;nh&nbsp;<a href=\"https://www.samsung.com/global/galaxy/what-is/amoled/\" rel=\"nofollow\" target=\"_blank\" title=\"Màn hình Super AMOLED\">sAMOLED</a></strong>&nbsp;<strong>10.5 inch, mỏng 5.7 mm dễ d&agrave;ng mang theo mọi l&uacute;c - mọi nơi</strong></h3>\r\n\r\n<p>Samsung Galaxy Tab S6 sở hữu tấm nền sAMOLED với độ ph&acirc;n giải WQXGA (2560x1600) tr&ecirc;n tỉ lệ m&agrave;n h&igrave;nh 16:10 l&yacute; tưởng. C&ugrave;ng với đ&oacute; l&agrave; mật độ điểm ảnh ở mức 287ppi, nhờ đ&oacute; Tab S6 cho khả năng hiển thị tốt. Người d&ugrave;ng c&oacute; thể sử dụng trong nhiều c&ocirc;ng việc từ giải tr&iacute; (xem phim, nghe nhạc, chơi game,&hellip;) đến l&agrave;m việc (đồ họa, ph&acirc;n t&iacute;ch,&hellip;).</p>\r\n\r\n<p><img alt=\"Tab S6 với màn hình sAMOLED 10.5 inch, mỏng 5.7 mm dễ dàng mang theo mọi lúc - mọi nơi\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-1.jpg\" /></p>\r\n\r\n<p>B&ecirc;n cạnh khả năng hiển thị tốt, SamSung cũng kh&ocirc;ng qu&ecirc;n tối ưu về thiết kế cho sản phẩm của m&igrave;nh. Tab S6 được trang bị m&agrave;n h&igrave;nh 10.5 inch, d&agrave;y 5.7 mm c&ugrave;ng thiết kế gần như kh&ocirc;ng viền. Nhờ đ&oacute; m&agrave; chiếc tablet Samsung n&agrave;y gọn &ndash; nhẹ, với trọng lượng chỉ 420g, nhẹ hơn c&aacute;c sản phẩm c&oacute; c&ugrave;ng k&iacute;ch thước. &nbsp;B&ecirc;n cạnh đ&oacute;, m&aacute;y sẽ được b&aacute;n ra với ba phi&ecirc;n bản m&agrave;u sắc kh&aacute;c nhau đ&oacute; l&agrave; Cloud Blue, Rose Blush, Mountain Grey.</p>\r\n\r\n<h3><strong>Camera trước 8MP c&ugrave;ng hệ thống camera k&eacute;p ph&iacute;a sau cho chất lượng ảnh sắc n&eacute;t</strong></h3>\r\n\r\n<p><strong>Samsung Tab S6</strong>&nbsp;sở hữu camera trước c&oacute; độ ph&acirc;n giải 8 MP, khẩu độ f/2.0 với g&oacute;c chụp 80˚ cho khả năng chụp selfie tốt. Cụm camera k&eacute;p 13MP v&agrave; 5MP ph&iacute;a sau với ống k&iacute;nh g&oacute;c rộng v&agrave; si&ecirc;u rộng, cho g&oacute;c chụp l&ecirc;n tới 123˚, Đ&aacute;p ứng tốt nhu cầu l&agrave;m việc v&agrave; giải tr&iacute;.</p>\r\n\r\n<p><img alt=\"Camera trước 8MP cùng hệ thống camera kép phía sau cho chất lượng ảnh sắc nét\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-2.jpg\" /></p>\r\n\r\n<p>B&ecirc;n cạnh ống k&iacute;nh g&oacute;c rộng,&nbsp;<strong>Galaxy Tab S6</strong>&nbsp;c&ograve;n hỗ trợ AI th&ocirc;ng minh gi&uacute;p tối ưu h&oacute;a t&ocirc;ng m&agrave;u cho ra những bức ảnh chụp như d&acirc;n chuy&ecirc;n nghiệp. C&ugrave;ng với Adobe Premiere Rush, việc chỉnh sửa ảnh, video chất lượng chưa bao giờ đơn giản đến vậy.</p>\r\n\r\n<h3><strong>Hiệu năng mạnh cho khả năng xử l&yacute; c&aacute;c t&aacute;c vụ mượt m&agrave; với&nbsp;<a href=\"https://cellphones.com.vn/sforum/snapdragon-8150-la-con-chip-cao-cap-tiep-theo-cua-qualcomm-manh-me-hon-ho-tro-5g\" target=\"_blank\" title=\"Snapdragon 8150 là con chip cao cấp tiếp theo của Qualcomm\">Snapdragon 8150</a>, 6GB RAM</strong></h3>\r\n\r\n<p>Nhờ con chip Qualcomm Snapdragon 8150, 8 nh&acirc;n m&agrave;&nbsp;<strong>Tab S6</strong>&nbsp;cho khả năng c&aacute;c t&aacute;c vụ ấn tượng, kh&ocirc;ng c&oacute; hiện tượng giật, lag c&ugrave;ng khả năng đa nhiệm kh&aacute; tốt.</p>\r\n\r\n<p><img alt=\"Hiệu năng mạnh cho khả năng xử lý các tác vụ mượt mà với Snapdragon 8150, 6GB RAM\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-3.jpg\" /></p>\r\n\r\n<p>C&ugrave;ng với đ&oacute;,&nbsp;<em><strong>Samsung Tab S6</strong></em>&nbsp;được c&agrave;i sẵn hệ điều h&agrave;nh&nbsp;<a href=\"https://cellphones.com.vn/sforum/android-9-chinh-thuc-ra-mat-voi-ten-goi-android-pie\" target=\"_blank\" title=\"Android 9 chính thức ra mắt với tên gọi Android Pie\">Android 9 (Pie)</a>&nbsp;c&ugrave;ng 6GB RAM, 128GB bộ nhớ trong gi&uacute;p c&aacute;c ứng dụng hoạt động mượt m&agrave; hơn, khả năng lưu trữ tốt đ&aacute;p ứng cho mọi c&ocirc;ng việc. Ngo&agrave;i ra, tại một số thị trường, m&aacute;y sẽ được b&aacute;n ra với bản 8GB RAM v&agrave; 256 GB bộ nhớ trong.</p>\r\n\r\n<p><img alt=\"ông nghệ âm thanh vòm Dolby Atmos\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-4.jpg\" /></p>\r\n\r\n<p>B&ecirc;n cạnh một hiệu năng tốt cho nhu cầu c&ocirc;ng việc th&igrave; Tab S6 c&ograve;n mang lại khả năng giải tr&iacute; cao với c&ocirc;ng nghệ &acirc;m thanh v&ograve;m Dolby Atmos.</p>\r\n\r\n<h3><strong>B&uacute;t S Pen đầy tiện lợi, ghi ch&uacute; mọi l&uacute;c &ndash; mọi nơi</strong></h3>\r\n\r\n<p>Điểm đặc biệt của&nbsp;<strong>Samsung Tab S6</strong>&nbsp;đ&oacute; l&agrave; sự xuất hiện của b&uacute;t S Pen. Cũng giống tr&ecirc;n chiếc&nbsp;<a href=\"https://cellphones.com.vn/samsung-galaxy-note-9-chinh-hang.html\" rel=\"nofollow\" target=\"_blank\" title=\"Samsung Galaxy Note 9\">Galaxy Note 9</a>, b&uacute;t s Pen cũng mang lại nhiều tiện &iacute;ch như t&iacute;nh năng chụp h&igrave;nh từ xa, quay phim,&hellip; Cụ thể, S Pen gi&uacute;p người d&ugrave;ng c&oacute; thể nhanh ch&oacute;ng ghi lại những &yacute; tưởng bất chợt, gi&uacute;p sự s&aacute;ng tạo lu&ocirc;n ph&aacute;t triển.</p>\r\n\r\n<p><img alt=\"Bút S Pen đầy tiện lợi, ghi chú mọi lúc – mọi nơi\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-5.jpg\" /></p>\r\n\r\n<p>Hay khả năng điều khiển từ xa với c&aacute;ch bấm n&uacute;t trong phạm vi 10m. B&ecirc;n cạnh những t&iacute;nh năng trước đ&oacute; th&igrave;&nbsp; S Pen c&ograve;n được th&ecirc;m 3 h&agrave;nh động mới đ&oacute; l&agrave; cho ph&eacute;p điều khiển trực quan bằng cử chỉ tay, thực hiện thuyết tr&igrave;nh hay kiểm so&aacute;t video m&agrave; kh&ocirc;ng cần tiếp x&uacute;c với tablet. Với độ ch&iacute;nh x&aacute;c v&agrave; khả năng kiểm so&aacute;t ấn tượng, với 4.096 mức độ nhạy &aacute;p lực kh&aacute;c nhau gi&uacute;p người d&ugrave;ng c&oacute; thể sử dụng cho nhiều việc, từ vẽ ph&aacute;c họa, viết,&hellip; trong nhiều tư thế.</p>\r\n\r\n<p><img alt=\"bút được gắn trực tiếp vào vết lõm ở mạt lưng với lực hút từ tính\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-6.jpg\" /></p>\r\n\r\n<p>Tiện lợi hơn đ&oacute; l&agrave; b&uacute;t được gắn trực tiếp v&agrave;o vết l&otilde;m ở mạt lưng với lực h&uacute;t từ t&iacute;nh. Thiết kế tiện dụng gi&uacute;p người d&ugrave;ng c&oacute; thể lấy &ndash; cất nhanh ch&oacute;ng. Ngo&agrave;i ra, theo SamSung, khi S Pen được sạc đầy c&oacute; thể k&eacute;o d&agrave;i đến 10 giờ ở chế độ chờ. Trong qu&aacute; tr&igrave;nh sử dụng, t&ugrave;y v&agrave;o từng t&aacute;c vụ m&agrave; giời gian thực tế sẽ kh&aacute;c nhau. Tuy nhi&ecirc;n, với t&iacute;nh năng sạc tự động bắt đầu ngay khi bạn ngắn, đảm bảo S Pen lu&ocirc;n tr&agrave;n đầy năng lượng mỗi khi bạn cần.</p>\r\n\r\n<h3><strong>Samsung Tab S6 sở hữu khả năng chuyển đổi linh hoạt giữa laptop v&agrave; tablet</strong></h3>\r\n\r\n<p>Kh&ocirc;ng chỉ l&agrave; một chiếc tablet đơn thuần, Samsung Galaxy Tab S6 c&ograve;n c&oacute; khả năng chuyển đổi nhanh ch&oacute;ng th&agrave;nh một chiếc laptop khi kết hợp với b&agrave;n ph&iacute;m BookCover di động để biến m&aacute;y tỉnh bảng trở th&agrave;nh một chiếc laptop gọn nhẹ, đầy đủ chức năng. B&ecirc;n cạnh đ&oacute;, Galaxy Tab S6 c&ograve;n c&oacute; khả năng kết nối với m&agrave;n h&igrave;nh kh&aacute;c th&ocirc;ng qua c&aacute;p HDMI. Một thiết kế th&ocirc;ng minh th&iacute;ch hợp cho người d&ugrave;ng c&oacute; nhu cầu l&agrave;m việc tr&ecirc;n kh&ocirc;ng gian m&agrave;n h&igrave;nh lớn.</p>\r\n\r\n<p><img alt=\"Samsung Tab S6 sở hữu khả năng chuyển đổi linh hoạt giữa laptop và tablet\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-7.jpg\" /></p>\r\n\r\n<p>Samsung Tab S6 c&ograve;n sở hữu khả năng xem t&aacute;ch nhiều cửa sổ. T&iacute;nh năng th&ocirc;ng minh gi&uacute;p chia nhỏ ứng dụng xem, gi&uacute;p người d&ugrave;ng c&oacute; thể sử dụng đồng thời c&ugrave;ng l&uacute;c tới 4 ứng dụng như Thư viện, Email v&agrave; Samsung Notes cũng như chia sẻ m&agrave;n h&igrave;nh theo &yacute; bạn muốn. Tuy nhi&ecirc;n, kh&ocirc;ng phải ứng dụng n&agrave;o Tab S6 cũng hỗ trợ thu nhỏ. Hiện tại, chỉ c&oacute; một số ứng dụng như Thư viện, M&aacute;y t&iacute;nh, Tin nhắn, Ghi ch&uacute; Samsung v&agrave; Video.&nbsp;Một số ứng dụng b&ecirc;n thứ 3 kh&aacute;c cũng c&oacute; thể hỗ trợ chế độ xem t&aacute;ch.</p>\r\n\r\n<h3><strong>Mở kh&oacute;a v&acirc;n tay v&agrave; dung lượng pin 7.040 mAh cho thời gian sử dụng đến 15 giờ</strong></h3>\r\n\r\n<p>Galaxy Tab S6 t&iacute;ch hợp m&aacute;y qu&eacute;t v&acirc;n tay quang học tr&ecirc;n m&agrave;n h&igrave;nh gi&uacute;p mở kh&oacute;a nhanh ch&oacute;ng c&ugrave;ng khả năng bảo mật tối đa.&nbsp;Kh&ocirc;ng cần mật khẩu, chỉ 1 lần chạm ng&oacute;n tay v&agrave;o m&agrave;n h&igrave;nh để mở kh&oacute;a ngay lập tức.</p>\r\n\r\n<p><img alt=\"Mở khóa vân tay và dung lượng pin 7.040 mAh \" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-8.jpg\" /></p>\r\n\r\n<p>Samsung Tab S6 được SamSung trang bị vi&ecirc;n pin c&oacute; dung lượng tới 7.040 mAh. Với dung lượng n&agrave;y, Tab S6 cho thời gian sử dụng kh&aacute; ấn tượng với 15 giờ xem video. B&ecirc;n cạnh đ&oacute;, m&atilde;y c&ograve;n được trang bị sạc nhanh th&ocirc;ng qua cổng USB-C hoặc đế sạc GordO. Nhờ đ&oacute;, người d&ugrave;ng c&oacute; thể thoải m&aacute;i sử dụng m&agrave; kh&ocirc;ng cần lo lắng về t&igrave;nh trạng pin.</p>\r\n\r\n<h3><strong>Hỗ trợ kết nối mạng di động LTE v&agrave; kết nối kh&ocirc;ng d&acirc;y th&ocirc;ng minh</strong></h3>\r\n\r\n<p>Galaxy Tab S6 hỗ trợ Nano sim cho ph&eacute;p kết nối với LTE để tải xuống với tốc độ tối đa 2.0Gbps v&agrave; tải l&ecirc;n tối đa 150Mbps.&nbsp;Với kết n&oacute;i wifi, tốc độ tải l&ecirc;n v&agrave; xuống l&ecirc;n tới 867Mbps.</p>\r\n\r\n<p><img alt=\"Hỗ trợ kết nối mạng di động LTE và kết nối không dây thông minh\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/samsung/SamSung-Galaxy-Tab-S6-9.jpg\" /></p>\r\n\r\n<p>Ngo&agrave;i ra, Galaxy Tab S6 c&ograve;n c&oacute; khả năng đồng bộ với điện thoại th&ocirc;ng minh Galaxy, gi&uacute;p bạn kh&ocirc;ng bị bở lỡ c&aacute;c th&ocirc;ng b&aacute;o, tin nhắn hay c&aacute;c cuộc gọi quan trọng. V&agrave; để thực hiện được t&iacute;nh năng n&agrave;y, c&aacute;c thiết bị phải được đăng k&iacute; cũng một t&agrave;i khoản SamSung. Ngo&agrave;i ra, trợ l&yacute; ảo Bixby sẽ l&agrave; một phần kh&ocirc;ng thể thiếu tr&ecirc;n Galaxy Tab S6.</p>\r\n\r\n<h3><strong>&nbsp;Mua m&aacute;y t&iacute;nh bảng SamSung Galaxy Tab S6 ch&iacute;nh h&atilde;ng, gi&aacute; rẻ tại&nbsp;<a href=\"https://cellphones.com.vn/\" target=\"_blank\" title=\"Hệ thống cửa hàng bán lẻ thiết bị công nghệ - điện tử chính hãng\">CellphoneS</a></strong></h3>\r\n\r\n<p>Galaxy Tab S6 l&agrave; một chiếc tablet th&ocirc;ng minh, hỗ trợ b&uacute;t S Pen hữu &iacute;ch cho c&ocirc;ng việc.&nbsp;<em><strong>SamSung Galaxy Tab S6</strong>&nbsp;</em>hứa hẹn l&agrave; một thiết bị c&ocirc;ng nghệ đ&aacute;ng sở hữu. Nếu bạn c&oacute; nhu cầu hoặc quan t&acirc;m về sản phẩm n&agrave;y, h&atilde;y đăng k&yacute; nhận th&ocirc;ng tin sản phẩm ngay để l&agrave; người đầu ti&ecirc;n tr&ecirc;n tay sản phẩm mới đến từ &ocirc;ng lớn SamSung.</p>\r\n\r\n<p>&nbsp;</p>', 1, 20, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(16, 'Apple iPad Pro 11 2018 Wi-fi 64G', 'apple-ipad-pro-11-2018-wi-fi-64g', 5, 3000000, 0, 2, 1, 1, 0, 'Nhằm mục đích biến iPad Pro thành chiếc máy tính bảng có khả năng thay thế đảm nhiệm công việc cho laptop, năm nay Apple đã ra mắt phiên bản nâng cấp mạnh mẽ về thiết kế lẫn hiệu năng “khủng” cho Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng.', '2020-02-13__pro2018-3-3.jpg', NULL, NULL, '2019-12-28 16:40:45', '2020-02-20 09:30:34', 'Máy tính bảng 21in23', '<h2>Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng - Cấu h&igrave;nh v&agrave; t&iacute;nh năng</h2>\r\n\r\n<p>Nhằm mục đ&iacute;ch biến&nbsp;<a href=\"https://cellphones.com.vn/tablet/ipad-pro.html\" target=\"_blank\" title=\"iPad Pro\">iPad Pro</a>&nbsp;th&agrave;nh chiếc m&aacute;y t&iacute;nh bảng c&oacute; khả năng thay thế đảm nhiệm c&ocirc;ng việc cho laptop, năm nay Apple đ&atilde; ra mắt phi&ecirc;n bản n&acirc;ng cấp mạnh mẽ về thiết kế lẫn hiệu năng &ldquo;khủng&rdquo; cho&nbsp;<strong>Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</strong>.</p>\r\n\r\n<h3>Thiết kế viền si&ecirc;u mỏng tr&ecirc;n Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</h3>\r\n\r\n<p>Về thiết kế,&nbsp;<strong>iPad Pro 11 2018 Wi-fi 64GB</strong>&nbsp;sở hữu phần c&aacute;c cạnh viền xung quanh mỏng đều nhau ở bốn cạnh. M&aacute;y đ&atilde; bỏ đi ph&iacute;m home truyền thống thay v&agrave;o đ&oacute; l&agrave; c&aacute;c cử chỉ vuốt khi sử dụng như tr&ecirc;n iPhone X. Tổng k&iacute;ch thước của sản phẩm l&agrave; 247.6 x 178.5 x 5.9mm v&agrave; c&oacute; trọng lượng 468g. Với k&iacute;ch thước n&agrave;y c&ugrave;ng với c&aacute;c cạnh xung quanh được bo tr&ograve;n mềm mại sẽ tạo cho người d&ugrave;ng cảm gi&aacute;c cầm nắm v&ocirc; c&ugrave;ng chắc chắn v&agrave; thoải m&aacute;i.</p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-1_1.jpg\" /></p>\r\n\r\n<h3>Trang bị c&ocirc;ng nghệ Face ID cao cấp tr&ecirc;n Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</h3>\r\n\r\n<p>Sản phẩm đ&atilde; được Apple thay thế phương thức bảo mật cảm biến v&acirc;n tay th&agrave;nh Face ID. Với cụm camera TrueDepth được t&iacute;ch hợp tr&ecirc;n&nbsp;<strong>iPad Pro 11 2018</strong>&nbsp;sẽ cho ph&eacute;p m&aacute;y nhận dạng tối đa 2 gương mặt người d&ugrave;ng. Đồng thời bất cứ hướng xoay cầm m&aacute;y theo chiều n&agrave;o của người d&ugrave;ng, Face ID cũng c&oacute; thể thực hiện mở kh&oacute;a được.</p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-2_1.jpg\" /></p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-4_1.jpg\" /></p>\r\n\r\n<h3>Trang bị cổng USB-Type C tr&ecirc;n Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</h3>\r\n\r\n<p>Đ&acirc;y l&agrave; lần đầu ti&ecirc;n Apple thay thế cổng kết nối Lightning tr&ecirc;n d&ograve;ng thiết bị iPad Pro của m&igrave;nh. Với việc t&iacute;ch hợp USB-Type C sẽ cho ph&eacute;p người d&ugrave;ng đồng bộ h&oacute;a tốt hơn với c&aacute;c thiết bị ngoại vi kh&aacute;c tr&ecirc;n thị trường. Đồng thời cổng kết nối n&agrave;y sẽ gi&uacute;p tốc độ truyền tải dữ liệu nhanh l&ecirc;n đến 10 gigabit. Người d&ugrave;ng c&oacute; thể xuất h&igrave;nh ảnh truyền từ m&agrave;n h&igrave;nh&nbsp;<strong>iPad Pro 11 64GB</strong>&nbsp;ra m&agrave;n h&igrave;nh ngo&agrave;i với độ ph&acirc;n giải khủng l&ecirc;n đến 5K.</p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-3_1.jpg\" /></p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-5_1.jpg\" /></p>\r\n\r\n<h3>Hiệu năng đỉnh cao tr&ecirc;n Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</h3>\r\n\r\n<p><strong><a href=\"https://cellphones.com.vn/apple-ipad-pro-11-2018-wifi-64-gb-chinh-hang.html\" target=\"_blank\" title=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\">Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng</a></strong>&nbsp;được mệnh danh l&agrave; chiếc m&aacute;y t&iacute;nh bảng đầu ti&ecirc;n tr&ecirc;n thế giới c&oacute; hiệu năng mạnh hơn cả c&aacute;c d&ograve;ng laptop c&ugrave;ng mức gi&aacute; tr&ecirc;n thị trường. Cụ thể trang bị con chip Apple A12X Bionic với 8 l&otilde;i CPU nhanh hơn đến 35% so với thiết bị tiềm nhiệm v&agrave; 7 l&otilde;i GPU cho tốc độ xử l&yacute; đồ họa nhanh hơn 2 lần so với con chip A11X. Con chip n&agrave;y được sản xuất theo tiến tr&igrave;nh 7nm tối t&acirc;n nhất tr&ecirc;n thị trường. Sở hữu b&ecirc;n trong 10 tỷ b&oacute;ng b&aacute;n dẫn mang đến hiệu năng cực xuất sắc trong mọi t&aacute;c vụ từ đơn giản đến phức tạp nhất.</p>\r\n\r\n<p><img alt=\"Apple iPad Pro 11 2018 Wi-fi 64GB Chính hãng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/tablet/apple/apple-ipad-pro-11-2018-wi-fi-64gb-chinh-hang-7_1.jpg\" /></p>\r\n\r\n<p>Mua&nbsp;<strong>iPad Pro 11 2018 Wi-fi 64GB gi&aacute; rẻ</strong>&nbsp;c&ugrave;ng chế độ bảo h&agrave;nh tốt ở đ&acirc;u? H&atilde;y đến với CellphoneS, hệ thống b&aacute;n lẻ Apple iPad Pro tr&ecirc;n to&agrave;n quốc hiện sẽ cung cấp cho kh&aacute;ch h&agrave;ng sản phẩm Apple iPad Pro 11 2018 Wi-fi 64GB Ch&iacute;nh h&atilde;ng trong thời gian sớm nhất khi được nh&agrave; sản xuất ra mắt c&ugrave;ng với việc b&aacute;n mức gi&aacute; v&ocirc; c&ugrave;ng hấp dẫn, chế độ hậu m&atilde;i v&ocirc; c&ugrave;ng tốt. Đối với c&aacute;c kh&aacute;ch h&agrave;ng ở xa c&oacute; nhu cầu mua sản phẩm c&oacute; thể tham khảo qua dịch vụ mua h&agrave;ng v&agrave; thanh to&aacute;n tận nơi miễn ph&iacute; của CellphoneS. Đặc biệt, đối với những sản phẩm mới ra mắt, kh&aacute;ch h&agrave;ng c&oacute; thể đặt cọc online để ưu ti&ecirc;n nhận m&aacute;y v&agrave; sở hữu nhiều phần qu&agrave; hấp dẫn.</p>', 1, 10, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p style=\"text-align:left\"><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td>1620x920</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p>20mpx</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p>8mpx</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p>5000mah</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p>64Gb</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p>4gb</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(17, 'iPhone 11 Pro Max', 'iphone-11-pro-max', 5, 29000000, 0, 0, 1, 1, 0, 'Trả góp 0%:\r\nTrả góp lãi suất 0% với Home Credit. Trả trước 50%, kỳ hạn 8 tháng (Áp dụng trên GIÁ NIÊM YẾT, không áp dụng cùng các khuyến mại khác)\r\nChương trình khuyến mại:\r\nGiảm 200.000đ khi mua kèm Tai nghe Airpods\r\nThu cũ đổi mới iPhone chính hãng VNA - Bù tiền ít nhất', '2020-02-13__thumb-11-promax-2-1.jpg', NULL, NULL, '2020-02-13 10:25:21', '2020-02-20 09:31:49', 'iphone11', '<h2><strong><a href=\"https://cellphones.com.vn/iphone-11-pro-max.html\" target=\"_blank\" title=\"Điện thoại iPhone 11 Pro Max chính hãng VN/A\">iPhone 11 Pro Max</a>&nbsp;ch&iacute;nh h&atilde;ng Vn/a&nbsp;&ndash; Trải nghiệm chụp ảnh đỉnh cao&nbsp;</strong><strong>với 3 camera sau</strong></h2>\r\n\r\n<p><em><strong>iPhone 11 Pro Max</strong>&nbsp;l&agrave; mẫu smartphone cao cấp nhất của Apple được ra mắt năm 2019. Với thiết kế cao cấp, hệ thống camera 3 camera c&ugrave;ng cấu h&igrave;nh si&ecirc;u mạnh mẽ th&igrave; iPhone 11 Pro Max ch&iacute;nh l&agrave; một chiếc&nbsp;<a href=\"https://cellphones.com.vn/mobile.html\" target=\"_blank\" title=\"Điện thoại smartphone chính hãng\">smartphone&nbsp;</a>đ&aacute;p ứng mọi trải nghiệm của người d&ugrave;ng.</em></p>\r\n\r\n<h3><strong>iPhone 11 Pro Max c&oacute; k&iacute;ch thước to hơn với chất liệu th&eacute;p kh&ocirc;ng gỉ bền bỉ</strong></h3>\r\n\r\n<p><strong>iPhone 11 Pro Max</strong>&nbsp;c&oacute; thiết kế tương tự như iPhone 11 Pro nhưng k&iacute;ch thước th&igrave; to hơn với k&iacute;ch thước m&agrave;n h&igrave;nh 6.5 inch v&agrave; to&agrave;n bộ m&aacute;y c&oacute; k&iacute;ch thước 158 x 77.8 x 8.1 mm. Thiết kế kh&ocirc;ng c&oacute; nhiều thay đổi trừ hệ thống camera sau được n&acirc;ng cấp th&agrave;nh 3 camera. M&agrave;n h&igrave;nh tai thỏ vẫn được giữ nguy&ecirc;n v&agrave; chất liệu th&eacute;p kh&ocirc;ng gỉ gi&uacute;p iPhone 11 Pro Max bền bỉ hơn.</p>\r\n\r\n<p><img alt=\"Thiết kế to hơn cho trải nghiệm tốt hơn với chất liệu thép không gỉ bền bỉ\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-1.jpg\" /></p>\r\n\r\n<p>Đối với Series iPhone 11,&nbsp;<a href=\"https://cellphones.com.vn/mobile/apple.html\" target=\"_blank\" title=\"Điện thoại Apple chính hãng\">Apple&nbsp;</a>đ&atilde; bỏ đi mặt k&iacute;nh ở mặt lưng v&agrave; thay bằng chất liệu mờ mang đến cảm gi&aacute;c sờ th&iacute;ch hơn cũng như hạn chế trầy xước, b&aacute;m v&acirc;n tay. C&aacute;c m&agrave;u sắc của iPhone 11 Pro Max cũng lạ hơn so với c&aacute;c người anh em tiền nhiệm trước đ&acirc;y với c&aacute;c m&agrave;u x&aacute;m, v&agrave;ng, bạc v&agrave; xanh b&oacute;ng đ&ecirc;m.</p>\r\n\r\n<p><img alt=\"4 phiên bản màu sắc\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-2.jpg\" /></p>\r\n\r\n<h3><strong>M&agrave;n h&igrave;nh 6.5 inch, c&ocirc;ng nghệ m&agrave;n h&igrave;nh Super Retina XDR cho khả năng hiển thị tuyệt vời</strong></h3>\r\n\r\n<p><strong>iPhone 11 Pro Max</strong>&nbsp;c&oacute; m&agrave;n h&igrave;nh lớn với k&iacute;ch thước m&agrave;n h&igrave;nh 6.5 inch, độ ph&acirc;n giải 1242 x 2688 pixels cho h&igrave;nh ảnh hiển thị sắc n&eacute;t. M&agrave;n h&igrave;nh đạt chuẩn Super Retina XDR mang đến khả năng hiển thị tuyệt vời, h&igrave;nh ảnh ch&acirc;n thật, m&agrave;u sắc sinh động, độ tương phản m&agrave;u cao, h&igrave;nh ảnh chi tiết cao. Với m&agrave;n h&igrave;nh của iPhone 11 Pro Max, bạn c&oacute; thể tận hưởng những bộ phim hấp dẫn, c&aacute;c game đỉnh cao với chất lượng h&igrave;nh ảnh v&ocirc; c&ugrave;ng tuyệt vời.</p>\r\n\r\n<p><img alt=\"Màn hình 6.5 inch, công nghệ màn hình Super Retina XDR cho khả năng hiển thị tuyệt vời\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-3.jpg\" /></p>\r\n\r\n<p>Tấm nền&nbsp;<a href=\"https://cellphones.com.vn/sforum/cong-nghe-man-hinh-oled-la-gi-smartphone-nao-dang-su-dung-man-hinh-oled?utm_source=sforum&amp;utm_medium=thu-thuat\" target=\"_self\" title=\"Màn hình Oled là gì\">OLED&nbsp;</a>sẽ gi&uacute;p cho iPhone 11 Pro Max c&oacute; m&agrave;u sắc hiển thị đẹp hơn v&agrave; s&aacute;t với thực tế, c&oacute; thể n&oacute;i l&agrave; thực hơn bao giờ hết. Ở iPhone 11 Pro Max, c&oacute; thể thấy Apple đ&atilde; chăm ch&uacute;t rất nhiều cho h&igrave;nh ảnh hiển thị đến với người d&ugrave;ng tốt nhất, ho&agrave;n hảo nhất.</p>\r\n\r\n<h3><strong>Hệ thống 3 camera</strong><strong>&nbsp;12MP</strong><strong>&nbsp;sau được n&acirc;ng cấp ch&iacute;nh l&agrave; điểm nổi bật ở iPhone 11 Pro Max</strong></h3>\r\n\r\n<p>Apple đ&atilde; n&acirc;ng cấp hệ thống camera cho iPhone 11 Pro Max th&agrave;nh cụm 3 camera v&agrave; được đặt gọn trong một h&igrave;nh vu&ocirc;ng. Với 1 camera g&oacute;c si&ecirc;u rộng m&agrave; ở c&aacute;c thế hệ iPhone trước đ&acirc;y chưa từng c&oacute;, gi&uacute;p cho khả năng chụp ảnh của iPhone 11 Pro Max đỉnh hơn v&agrave; chuy&ecirc;n nghiệp hơn.</p>\r\n\r\n<p><img alt=\"Hệ thống 3 camera 12MP sau được nâng cấp chính là điểm nổi bật ở iPhone 11 Pro Max\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-4.jpg\" /></p>\r\n\r\n<p>Hệ thống 3 camera c&oacute; c&ugrave;ng th&ocirc;ng số cảm biến l&agrave; 12MP v&igrave; thế h&igrave;nh ảnh được chụp từ iPhone 11 Pro Max v&ocirc; c&ugrave;ng sắc n&eacute;t v&agrave; ấn tượng. Khả năng chụp ảnh ở điều kiện thiếu s&aacute;ng tốt hơn, phạm vi zoom được mở rộng, cảm biến chiều s&acirc;u 3D mang đến h&igrave;nh ảnh như được chụp từ c&aacute;c m&aacute;y ảnh chuy&ecirc;n nghiệp, v&ocirc; c&ugrave;ng chất lượng, sắc n&eacute;t. B&ecirc;n cạnh đ&oacute; camera trước 12MP TrueDepth với c&ocirc;ng nghệ IR v&agrave; RBG cho khả năng nhận diện &aacute;nh s&aacute;ng tốt hơn, mang đến ảnh selfie đẹp v&agrave; rực rỡ hơn.</p>\r\n\r\n<h3><strong>Cấu h&iacute;nh mạnh mẽ với chip&nbsp;A13 Bionic, RAM đến 6GB xử l&yacute; nhanh ch&oacute;ng mọi t&aacute;c vụ v&agrave; chạy hệ điều h&agrave;nh iOS 13</strong></h3>\r\n\r\n<p>Con chip A13 Bionic l&agrave; một bộ vi xử l&yacute; tuyệt vời, mang sức mạnh vượt trội hơn hẳn chip A12 cũ. Được trang bị con chip A13,&nbsp;<em><strong>iPhone 11 Pro Max</strong></em>&nbsp;sở hữu cấu h&igrave;nh mạnh mẽ, ấn tượng v&agrave; k&egrave;m với RAM dung lượng đến 6GB, mọi thao t&aacute;c t&aacute;c vụ iPhone 11 Pro Max đều c&oacute; thể xử l&yacute; nhanh ch&oacute;ng, cho bạn trải nghiệm mượt m&agrave;, ổn định tất cả c&aacute;c t&aacute;c vụ, đa nhiệm.</p>\r\n\r\n<p><img alt=\"Cấu hính mạnh mẽ với chip A13 Bionic, RAM đến 6GB xử lý nhanh chóng mọi tác vụ và chạy hệ điều hành iOS 13\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-5.jpg\" /></p>\r\n\r\n<p>iPhone 11 Pro Max chạy hệ điều h&agrave;nh iOS 13 v&agrave; đ&acirc;y cũng l&agrave; hệ điều h&agrave;nh đang thu h&uacute;t nhiều sự ch&uacute; &yacute; từ ph&iacute;a truyền th&ocirc;ng cũng như cộng đồng y&ecirc;u c&ocirc;ng nghệ. iOS 13 ch&iacute;nh l&agrave; nền tảng cơ bản để iPhone 11 Pro Max c&oacute; thể ph&aacute;t huy những t&iacute;nh năng, sức mạnh nổi bật của m&igrave;nh. iPhone 11 Pro Max c&oacute; bộ nhớ trong 64GB v&igrave; vậy bạn c&oacute; thể lưu trữ nhiều dữ liệu, tập tin h&igrave;nh ảnh. Ngo&agrave;i ra iPhone 11 Pro Max c&ograve;n c&oacute; phi&ecirc;n bản&nbsp;<a href=\"https://cellphones.com.vn/iphone-11-pro-max-256gb.html\" target=\"_blank\" title=\"iPhone 11 Pro Max 256GB\">iPhone 11 Pro Max 256GB</a>&nbsp;v&agrave;&nbsp;<a href=\"https://cellphones.com.vn/iphone-11-pro-max-512gb.html\" target=\"_blank\" title=\"iPhone 11 Pro Max 512GB\">iPhone 11 Pro Max 512GB</a>&nbsp;d&agrave;nh cho ai c&oacute; nhu cầu lưu trữ cao, tận dụng tối đa bộ nhớ điện thoại.</p>\r\n\r\n<h3><strong>FaceID tr&ecirc;n iPhone 11 Pro Max được n&acirc;ng cấp cho&nbsp;bảo mật an to&agrave;n, mở kh&oacute;a nhanh hơn</strong></h3>\r\n\r\n<p>T&iacute;nh năng nhận diện khu&ocirc;n mặt FaceID vẫn được ứng dụng cho&nbsp;<strong>iPhone 11 Pro Max</strong>&nbsp;để bạn c&oacute; thể sử dụng điện thoại an to&agrave;n hơn. Khả năng nhận diện khu&ocirc;n mặt chuẩn x&aacute;c với camera hồng ngoại v&igrave; vậy bạn c&oacute; thể y&ecirc;n t&acirc;m về độ bảo mật tr&ecirc;n iPhone 11 Pro Max. Với FaceID, bạn c&oacute; thể nhanh ch&oacute;ng mở kh&oacute;a đăng nhập v&agrave;o điện thoại m&agrave; kh&ocirc;ng cần phải nhập mật khẩu cho mỗi lần đăng nhập.</p>\r\n\r\n<p><img alt=\"FaceID bảo mật an toàn, mở khóa nhanh chóng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-6.jpg\" /></p>\r\n\r\n<h3><strong>Dung lượng pin 3500 mAh, c&ocirc;ng nghệ sạc ngược kh&ocirc;ng d&acirc;y tối ưu khả năng sử dụng</strong></h3>\r\n\r\n<p>Pin của&nbsp;<em>iPhone 11 Pro Max</em>&nbsp;c&oacute; dung lượng 3500 mAh, lớn hơn pin của iPhone Xs Max chỉ c&oacute; 3174 mAh v&igrave; thế iPhone 11 Pro Max mang đến thời gian sử dụng l&acirc;u hơn. B&ecirc;n cạnh đ&oacute;, sự hỗ trợ từ chip A13 Bionic c&ograve;n gi&uacute;p thời lượng pin tăng l&ecirc;n ch&iacute;nh v&igrave; thế bạn sẽ c&oacute; thể trải nghiệm điện thoại l&acirc;u hơn, nghe nhạc, xem phim, chơi game, l&agrave;m việc...trong thời gian d&agrave;i hơn.</p>\r\n\r\n<p><img alt=\"Dung lượng pin 3500 mAh, công nghệ sạc ngược không dây tối ưu khả năng sử dụng\" src=\"https://cdn.cellphones.com.vn/media/wysiwyg/mobile/apple/iphone-11-pro-max-7.jpg\" /></p>\r\n\r\n<p>iPhone 11 Pro Max c&ograve;n được trang bị c&ocirc;ng nghệ sạc nhanh 18W, c&ocirc;ng nghệ sạc ngược kh&ocirc;ng d&acirc;y gi&uacute;p bạn chia sẻ pin cho c&aacute;c thiết bị Apple kh&aacute;c. Với c&ocirc;ng nghệ sạc nhanh, bạn c&oacute; thể nạp lại ngay 50% pin cho iPhone của m&igrave;nh chỉ trong 30 ph&uacute;t. Giờ đ&acirc;y bạn c&oacute; thể sử dụng điện thoại mọi l&uacute;c m&agrave; kh&ocirc;ng c&ograve;n lo t&igrave;nh trạng hết pin cũng như phải chờ đợi sạc l&acirc;u.</p>\r\n\r\n<h3>&nbsp;</h3>', 1, 7, 0, 0, '<table align=\"left\" border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></big></p>\r\n			</td>\r\n			<td style=\"text-align:center\">\r\n			<p><big><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big><big><strong><span style=\"color:#000000\">Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></big></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Độ ph&acirc;n giải</big></p>\r\n			</td>\r\n			<td style=\"text-align:center\">123123</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Camera sau</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">123123</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Camera trước</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Dung lượng Pin</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Dung lượng</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Ram</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Th&ocirc;ng số CPU</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Ch&iacute;p&nbsp;</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p style=\"text-align:center\"><big>&nbsp; &nbsp;Quay phim</big></p>\r\n			</td>\r\n			<td>\r\n			<p style=\"text-align:center\">&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ra_product_id` int(11) NOT NULL DEFAULT '0',
  `ra_number` tinyint(4) NOT NULL DEFAULT '0',
  `ra_content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `ra_product_id`, `ra_number`, `ra_content`, `ra_user_id`, `created_at`, `updated_at`) VALUES
(3, 9, 4, 'sản phẩm tốt', 4, '2019-12-27 08:43:23', '2019-12-27 08:43:23'),
(4, 11, 3, 'tốt', 4, '2019-12-30 09:21:15', '2019-12-30 09:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noidung` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `ten`, `noidung`, `hinh`, `created_at`, `updated_at`) VALUES
(2, 'Thuận Thành Tech', '<p>eee 123123</p>', 'DM_big-oppo-800-300-800x300-1.png', '2020-01-02 09:54:24', '2020-02-14 01:51:50'),
(3, 'Slide2', '<p>slide2</p>', '92_800-300-800x300-17.png', '2020-01-02 10:23:00', '2020-02-14 01:52:22'),
(4, 'Slide3', '<p>Slide3</p>', 'big-iphone-800-300-800x300-4.png', '2020-02-14 01:52:51', '2020-02-14 01:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tr_user_id` int(11) NOT NULL DEFAULT '0',
  `tr_total` int(11) NOT NULL DEFAULT '0',
  `tr_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tr_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `tr_user_id`, `tr_total`, `tr_note`, `tr_address`, `tr_phone`, `tr_status`, `created_at`, `updated_at`, `tr_type`) VALUES
(8, 4, 31677, 'ship đi', 'test lại', '09231232', 2, '2019-12-18 10:34:31', '2020-01-20 04:36:46', '1'),
(12, 4, 7400000, 'tôi muốn mua trong 2 ngày', 'Hà nội - Thanh xuân', '09231232', 0, '2019-12-30 08:55:20', '2020-01-31 09:20:43', '1'),
(13, 4, 5488000, 'rrrrrrrrrrrrrrrrrrrrrrrrrrr', 'hn', '09231232', 3, '2019-12-30 08:57:53', '2020-01-21 04:29:23', '1'),
(23, 5, 10995000, 'ko', 'bn', '0923123232', 1, '2020-01-08 09:10:31', '2020-01-08 10:17:27', '1'),
(24, 5, 7400000, 'ko có', 'BN', '0923123232', 1, '2020-01-08 09:31:38', '2020-01-08 10:21:32', '1'),
(26, 5, 10995000, 'thanh toán', 'NGghi AN', '0923123232', 0, '2020-01-16 02:16:30', '2020-01-16 02:16:30', '1'),
(32, 5, 14000000, 'Mua điện thoại', 'Thôn nghi AN - Trạm lộ', '0923123232', 1, '2020-01-17 02:15:22', '2020-01-17 02:16:12', '2'),
(34, 5, 10995000, 'ggggggg', 'gggggggg', '0923123232', 1, '2020-01-17 03:37:52', '2020-01-17 03:42:21', '2'),
(35, 5, 10995000, 'đặt hàng', 'bn', '0923123232', 1, '2020-01-17 06:11:26', '2020-01-18 13:26:58', '1'),
(38, 4, 10995000, 'ASSSSS', 'AAAAAAA', '09231232', 0, '2020-01-18 13:40:41', '2020-01-18 13:40:41', '1'),
(39, 4, 10995000, 'FFFF', '5', '09231232', 1, '2020-01-18 13:45:56', '2020-01-18 14:18:24', '1'),
(40, 5, 10995000, 'ggg', 'ggg', '0923123232', 1, '2020-01-18 13:47:10', '2020-01-18 13:47:35', '2'),
(42, 5, 29000000, 'aaaaaaaaaaa', 'ccccccccccc', '0923123232', 1, '2020-01-31 07:38:31', '2020-01-31 07:38:54', '1'),
(43, 5, 10995000, 'qưe', 'qưe', '0923123232', 1, '2020-02-02 14:30:23', '2020-02-02 14:30:37', '2'),
(44, 5, 10995000, 'vì tao dùng trên môi triuowng test của bọn vnpay lên nó hỗ trợ ngân hàng NCB', 'Hưng yên', '0923123232', 1, '2020-02-05 09:36:10', '2020-02-05 09:36:52', '2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_pay` int(11) NOT NULL DEFAULT '0',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_code` timestamp NULL DEFAULT NULL,
  `code_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_active` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `avatar`, `active`, `password`, `remember_token`, `created_at`, `updated_at`, `total_pay`, `address`, `about`, `code`, `time_code`, `code_active`, `time_active`) VALUES
(4, 'huy', 'emlacaloc@gmail.com', '09231232', NULL, 1, '$2y$10$9rp3So6wgoi04rSKzgm4Mel8vcvWiThVCiatSSapkkc21N5gvr/4K', NULL, '2019-11-21 23:39:00', '2020-01-08 06:58:26', 16, 'Nghi an trmaj lộ', 'giới thiệu bnar thân', NULL, NULL, NULL, NULL),
(5, 'Nguyễn Viết Việt', 'nvviet59@gmail.com', '0923123232', NULL, 2, '$2y$10$bJMqNkkP3rtvYa1tPzpNTuzeAO3uHddnC75eDRI7KOZHoKxmLnC/2', NULL, '2019-11-22 08:54:02', '2020-01-10 01:50:57', 8, NULL, NULL, '$2y$10$TVJeJQx/MiVu8mSiU8q1heiHbzLYvoESg/L9KnkSTtOCtPkl8X42W', '2020-01-10 01:50:57', NULL, NULL),
(6, 'ngViet', 'huy@gmail.com', '021212', NULL, 1, '$2y$10$291IL8H0MbcSQ5spWDJCEOuOTlD3FOkDTYjd8.woNrrI.AG140sQO', NULL, '2019-12-09 03:27:00', '2019-12-09 03:27:00', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Nguyễn Hiệu', 'nnn@gmail.com', '091293', NULL, 2, '$2y$10$zQwP3qXwq5RXa.kK8v1H9upQ0o8KV8YuM/kqViMsSaPr106JyrZX.', NULL, '2020-01-10 06:41:58', '2020-01-10 06:48:59', 0, NULL, NULL, NULL, NULL, '$2y$10$E3ikTaoJwGIC0o/Fo/XyQe0GVLeqrMn8Ymu0kEK2OjRmKky6d4toW', '2020-01-10 06:41:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD KEY `admins_active_index` (`active`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_a_name_unique` (`a_name`),
  ADD KEY `articles_a_slug_index` (`a_slug`),
  ADD KEY `articles_a_active_index` (`a_active`),
  ADD KEY `articles_a_author_id_index` (`a_author_id`),
  ADD KEY `articles_a_hot_index` (`a_hot`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_c_name_unique` (`c_name`),
  ADD KEY `categories_c_slug_index` (`c_slug`),
  ADD KEY `categories_c_active_index` (`c_active`),
  ADD KEY `categories_c_home_index` (`c_home`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_or_transactions_id_index` (`or_transaction_id`),
  ADD KEY `orders_or_product_id_index` (`or_product_id`);

--
-- Indexes for table `page_statics`
--
ALTER TABLE `page_statics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_statics_ps_type_index` (`ps_type`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_pro_slug_index` (`pro_slug`),
  ADD KEY `products_pro_category_id_index` (`pro_category_id`),
  ADD KEY `products_pro_author_id_index` (`pro_author_id`),
  ADD KEY `products_pro_active_index` (`pro_active`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_ra_product_id_index` (`ra_product_id`),
  ADD KEY `ratings_ra_user_id_index` (`ra_user_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_tr_user_id_index` (`tr_user_id`),
  ADD KEY `transactions_tr_status_index` (`tr_status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_active_index` (`active`),
  ADD KEY `users_code_index` (`code`),
  ADD KEY `users_code_active_index` (`code_active`),
  ADD KEY `users_time_active_index` (`time_active`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `page_statics`
--
ALTER TABLE `page_statics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
