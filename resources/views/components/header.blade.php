<style>
    /*body { padding: 2em; }*/


/* Shared */
.loginBtn {
  box-sizing: border-box;
  position: relative;
  /* width: 13em;  - apply for fixed size */
  margin: 0.2em;
  padding: 0 15px 0 46px;
  border: none;
  text-align: left;
  line-height: 34px;
  white-space: nowrap;
  border-radius: 0.2em;
  font-size: 16px;
  color: #FFF;
}
.loginBtn:before {
  content: "";
  box-sizing: border-box;
  position: absolute;
  top: 0;
  left: 0;
  width: 34px;
  height: 100%;
}
.loginBtn:focus {
  outline: none;
}
.loginBtn:active {
  box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
}


/* Facebook */
.loginBtn--facebook {
  background-color: #4C69BA;
  background-image: linear-gradient(#4C69BA, #3B55A0);
  /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
  text-shadow: 0 -1px 0 #354C8C;
}
.loginBtn--facebook:before {
  border-right: #364e92 1px solid;
  background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
}
.loginBtn--facebook:hover,
.loginBtn--facebook:focus {
  background-color: #5B7BD5;
  background-image: linear-gradient(#5B7BD5, #4864B1);
}


/* Google */
.loginBtn--google {
  /*font-family: "Roboto", Roboto, arial, sans-serif;*/
  background: #DD4B39;
}
.loginBtn--google:before {
  border-right: #BB3F30 1px solid;
  background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
}
.loginBtn--google:hover,
.loginBtn--google:focus {
  background: #E74B37;
}
</style>

     <header class="header" style="line-height: 40px; background-color: black;color:white;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">Hotline:0363701249</div>
                    {{-- <div class="col-md-6 text-right"> Hỗ trợ 24/7</div> --}}
                    <div class="col-md-6 text-right">
                        <button class="loginBtn loginBtn--facebook">
                          Đăng nhập với Facebook
                        </button>

                        <a href="{{route('get.login.social',['social'=>'google'])}}"><button class="loginBtn loginBtn--google">
                          Đăng nhập với Google
                        </button>
                    </a>
                    </div>

                </div>
            </div>
        </header>
     <header class="short-stor"  style=" box-shadow: 0 4px 2px -2px #dfdfdf;" >


            <div class="container">
                <div class="row">
                    
                    <div class="col-md-3 col-sm-12 text-center nopadding-right">
                        <div class="top-logo">
                            <a href="{{route('home')}}"><img src="img/logoxq.png" style="height: 55px;" alt="" /></a>
                        </div>
                    </div>
                  
                    <div class="col-md-6 text-center">
                        <div class="mainmenu" >
                            <nav>
                                <ul>
                                    <li class="expand"><a href="{{route('home')}}">Trang chủ</a></li>
                                    <li class="expand">
                                        <a>Sản phẩm</a>
                                        <ul class="restrain sub-menu">
                                        @if(isset($categories))
                                            @foreach($categories as $cate)
                                            <li><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">{{$cate->c_name}}</a></li>                   
                                            @endforeach
                                        @endif
                                    </ul>
                                    </li>
                                    <li class="expand"><a href="{{route('get.list.article')}}">Tin tức</a></li>
                                    <li class="expand"><a href="{{route('get.about_us')}}">Giới thiệu</a>
                                        <ul class="restrain sub-menu">
                                            <li><a href="{{route('get.about_us')}}">Về chúng tôi</a></li>
                                            <li><a href="{{route('get.thongtin')}}">Thông tin giao hàng</a></li>
                                            <li><a href="{{route('get.baomat')}}">Chính sách bảo mật</a></li>
                                            <li><a href="{{route('get.dieukhoan')}}">Điều khoản sử dụng</a></li>
                                        </ul>
                                    </li>
                                    <li class="expand"><a href="{{route('get.contact')}}">Liên hệ</a></li>
                              
                                  </ul>
                            </nav>
                        </div>
                       
                        <div class="row">
                            <div class="col-sm-12 mobile-menu-area">
                                <div class="mobile-menu hidden-md hidden-lg" id="mob-menu">
                                    <span class="mobile-menu-title">Menu</span>
                                    <nav>
                                        <ul>
                                            <li><a href="index.html">Trang chủ</a>
                                               
                                            </li>
                                            <li><a href="shop-grid.html">Sản phẩm</a>
                                                <ul>
                                                    @if(isset($categories))
                                                        @foreach($categories as $cate)
                                                        <li><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">{{$cate->c_name}}</a></li>                                                          @endforeach
                                                    @endif
                                                    
                                                </ul>                                          
                                            </li>
                                           <li><a href="{{route('get.list.article')}}">Tin tức</a></li>
                                           <li><a href="index.html">Giới thiệu</a></li>
                                           <li><a href="index.html">Liên hệ</a></li>
                                        </ul>
                                    </nav>
                                </div>                      
                            </div>
                        </div>
                        
                    </div>
                   
                    <div class="col-md-3 col-sm-12 nopadding-left">
                        <div class="top-detail">
                         
                            
                            <div class="disflow">
                                <div class="circle-shopping expand">
                                    <div class="shopping-carts text-right">
                                        <div class="cart-toggler">
                                            <a href="{{route('get.list.shopping.cart')}}"><i class="fas fa-shopping-cart"></i></a>
                                            <a href="{{route('get.list.shopping.cart')}}"><span class="cart-quantity">{{Cart::count()}}</span></a>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            
                             <div class="disflow">
                                <div class="header-search expand">
                                    <div class="search-icon fa fa-search"></div>
                                    <div class="product-search restrain">
                                        <div class="container nopadding-right">
                                            <form action="{{route('get.product.list')}}" id="searchform" method="get">
                                                <div class="input-group">
                                                    <input type="text" name="k" class="form-control" maxlength="128" placeholder="Search product...">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- search divition end -->
                            {{-- <p>Đăng nhập</p> --}}
                            <div class="disflow">
                                <div class="expand dropps-menu">
                                    <a href="{{route('get.login')}}"><i class="fas fa-user"></i></a>
                                    <ul class="restrain language">

                                        @if(Auth::check())
                                        <li><a href="{{route('user.dashboard')}}">Quản lý</a></li>
                                        <li><a href="{{route('get.list.shopping.cart')}}">Giỏ hàng</a></li>
                                        <li><a href="{{route('get.logout.user')}}">Thoát</a></li>
                                        @else
                                            <li><a href="{{route('get.register')}}">Đăng ký</a></li>
                                            <li><a href="{{route('get.login')}}">Đăng nhập</a></li>
                                        @endif
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </header> 
        