<footer>
            <!-- top footer area start -->
            <div class="top-footer-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="single-snap-footer">
                                <div class="snap-footer-title">
                                    <h4>Thông tin công ty</h4>
                                </div>
                                <div class="cakewalk-footer-content">
                                    <p class="footer-des">Vị thế công ty ngày càng trở lên mạnh mẽ sau vài năm hoạt động sôi nổi trở lại đây</p>
                                    <a href="#" class="read-more">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="single-snap-footer">
                                <div class="snap-footer-title">
                                    <h4>Thông tin</h4>
                                </div>
                                <div class="cakewalk-footer-content">
                                    <ul>
                                        <li><a href="{{route('get.about_us')}}" target="_blank">Về chúng tôi</a></li>
                                        <li><a href="{{route('get.thongtin')}}">Thông tin giao hàng</a></li>
                                        <li><a href="{{route('get.baomat')}}">Chính sách bảo mật</a></li>
                                        <li><a href="{{route('get.dieukhoan')}}">Điều khoản sử dụng</a></li>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="single-snap-footer">
                                <div class="snap-footer-title">
                                    <h4>Thông tin khác</h4>
                                </div>
                                <div class="cakewalk-footer-content">
                                    <ul>
                                       
                                        <li><a href="#">Liên hệ</a></li>
                                        <li><a href="#">Checkout</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 hidden-sm">
                            <div class="single-snap-footer">
                                <div class="snap-footer-title">
                                    <h4>Tài khoản</h4>
                                </div>
                                <div class="cakewalk-footer-content">
                                    <ul>
                                        <li><a href="#">Tổng quan</a></li>
                                        <li><a href="{{route('get.login')}}">Đăng nhập</a></li>
                                        <li><a href="{{route('get.list.shopping.cart')}}">Giỏ hàng</a></li>
                                        <li><a href="#">Affiliates</a></li>
                                        <li><a href="#">Sản phẩm yêu thích</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 hidden-sm">
                            <div class="single-snap-footer">
                                <div class="snap-footer-title">
                                    <h4>Theo dõi</h4>
                                </div>
                                <div class="cakewalk-footer-content social-footer">
                                    <ul>
                                        <li><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a><span> Facebook</span></li>
                                        <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a><span> Google Plus</span></li>
                                        <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a><span> Twitter</span></li>
                                        <li><a href="https://youtube.com/" target="_blank"><i class="fa fa-youtube-play"></i></a><span> Youtube</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            <!-- top footer area end -->
            <!-- info footer start -->
            <div class="info-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="info-fcontainer">
                                <div class="infof-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="infof-content">
                                    <h3>Địa chỉ:</h3>
                                    <p>Thôn Nghi An - Trạm Lộ - Thuận Thành - Bắc Ninh</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="info-fcontainer">
                                <div class="infof-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="infof-content">
                                    <h3>Phone (Hỗ trợ):</h3>
                                    <p>0363701249</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="info-fcontainer">
                                <div class="infof-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="infof-content">
                                    <h3>Email hỗ trợ:</h3>
                                    <p>nvviet59@gmail.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 hidden-sm">
                            <div class="info-fcontainer">
                                <div class="infof-icon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <div class="infof-content">
                                    <h3>Mở cửa:</h3>
                                    <p>Thứ 2 - CN : 8:00 sáng - 20:00 tối</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- info footer end -->
            <!-- banner footer area start -->
          
            <!-- banner footer area end -->
            <!-- footer address area start -->
            <div class="address-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <address>Copyright © <a href="https://www.facebook.com/viet.nguyenviet.359">Việt Mobile</a> All Rights Reserved</address>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="footer-payment pull-right">
                                <a href="#"><img src="img/payment.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer address area end -->
        </footer>