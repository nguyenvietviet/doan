<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Xuân Quang mobile | Home</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="{{asset('')}}">
        <!-- Favicon
        ============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <!-- Fonts
        ============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>
        
        <!-- CSS  -->
        
        <!-- Bootstrap CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        
        <!-- owl.carousel CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
        
        <!-- owl.theme CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
            
        <!-- owl.transitions CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
        
        <!-- font-awesome.min CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        
        <!-- font-icon CSS
        ============================================ -->      
        <link rel="stylesheet" href="{{asset('fonts/font-icon.css')}}">
        
        <!-- jquery-ui CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
        
        <!-- mobile menu CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">
        
        <!-- nivo slider CSS
        ============================================ -->
        <link rel="stylesheet" href="{{asset('custom-slider/css/nivo-slider.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{asset('custom-slider/css/preview.css')}}" type="text/css" media="screen" />
        
        <!-- animate CSS
        ============================================ -->         
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        
        <!-- normalize CSS
        ============================================ -->        
        <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
   
        <!-- main CSS
        ============================================ -->          
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        
        <!-- style CSS
        ============================================ -->          
        <link rel="stylesheet" href="{{asset('style.css')}}">
        
        <!-- responsive CSS
        ============================================ -->          
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
        
        <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>


        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>



    </head>
    <body class="home-one">
        @if(get_data_user('web','active')==1)
            <p>Tài khoản của bạn chưa được kích hoạt, vui lòng check mail để kích hoạt tài khoản</p>
        @endif

       @if (session('warning'))
        <div class="alert alert-danger alert-dismissible" style="position: absolute;margin-left: 80%;margin-top: 3%; background-color: #f8d7da; border-color: #f5c6cb;" role="alert">
           <span class="badge badge-pill badge-danger" style="color: #fff;background-color: #dc3545;">{{ session('warning') }}<br>
           </span> 
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">×</span>
           </button>
        </div>
        @endif
        @if (session('thongbao'))
        <div class="alert alert-success alert-dismissible" style="position: absolute;margin-left: 80%;margin-top: 3%;" role="alert">
           <span class="badge badge-pill badge-success" style="color: #fff;
    background-color: #28a745;">{{ session('thongbao') }}</span> 
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">×</span>
           </button>
        </div>
        @endif
        
       @include('components.header')
    
       
        <!-- header area end -->
        @yield('content')


        @if (session('warning'))
    <div class="alert alert-warning">
        {{ session('warning') }}
    </div>
@endif
    
        <!-- FOOTER START -->
        @include('components.footer')
        <!-- FOOTER END -->
        
        <!-- JS -->
        <script src="{{asset('js/js1/jquery-3.3.1.slim.min.js')}}"></script>
        <script src="{{asset('js/js1/popper.min.js')}}"></script>
        <script src="{{asset('js/js1/bootstrap.min.js')}}"></script>
        <!-- jquery-1.11.3.min js
        ============================================ -->         
        <script src="{{asset('js/vendor/jquery-1.11.3.min.js')}}"></script>
        
        <!-- bootstrap js
        ============================================ -->         
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        
        <!-- Nivo slider js
        ============================================ -->        
        <script src="{{asset('custom-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
        <script src="{{asset('custom-slider/home.js')}}" type="text/javascript"></script>
        
        <!-- owl.carousel.min js
        ============================================ -->       
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        
        <!-- jquery scrollUp js 
        ============================================ -->
        <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
        
        <!-- price-slider js
        ============================================ --> 
        <script src="{{asset('js/price-slider.js')}}"></script>
        
        <!-- elevateZoom js 
        ============================================ -->
        <script src="{{asset('js/jquery.elevateZoom-3.0.8.min.js')}}"></script>
        
        <!-- jquery.bxslider.min.js
        ============================================ -->       
        <script src="{{asset('js/jquery.bxslider.min.js')}}"></script>
        
        <!-- mobile menu js
        ============================================ -->  
        <script src="{{asset('js/jquery.meanmenu.js')}}"></script>   
        
        <!-- wow js
        ============================================ --> 

        <script src="{{asset('js/gmap.js')}}"></script>      
        <script src="{{asset('js/wow.js')}}"></script>
        
        <!-- plugins js
        ============================================ -->         
        <script src="{{asset('js/plugins.js')}}"></script>
        
        <!-- main js
        ============================================ -->           
        <script src="{{asset('js/main.js')}}"></script>

        @yield('js')
    </body>
</html>