@extends('layouts.app')
@section('content')
<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="container-inner">
							<ul>
								<li class="home">
									<a href="{{route('home')}}">Trang chủ</a>
									<span><i class="fa fa-angle-right"></i></span>
								</li>
								<li class="category3"><span>Tin tức</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="main-contact-area">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						@include('components.article')
					</div>
					<div class="col-md-3">
						<h4>Bài viết nổi bật</h4>
						<div class="list_article_hot">
							@include('components.article_hot')
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection