@extends('layouts.app')
@section('content')
<div class="our-product-area new-product">
    <div class="container">
        <div class="area-title">
            <h2>Giỏ hàng của bạn</h2>
        </div>
        <table class="table">
			    <thead>
			      <tr>
			        <th>STT</th>
			        <th>Tên SP</th>
			        <th>Hình ảnh</th>
			        <th>Giá</th>
			        <th>Số lượng</th>
			        <th>Thành tiền</th>
			        <th>Thao tác</th>
			      </tr>
			    </thead>
			    <tbody>
			    	<?php $i=1 ?>
			    	@foreach($products as $key => $product)
			      <tr>
				      	<td>#{{$i}}</td>
				        <td><a href="">{{$product->name}}</a></td>
				        <td>
				        	<img style="width: 80px;height: 80px;" src="{{asset("")}}/{{pare_url_file($product->options->avatar)}}">
				        </td>
				        <td>{{number_format($product->price)}} đ</td>
				        <td>{{$product->qty}}</td>
				        <td>{{number_format($product->qty * $product->price,0,',','.')}} đ</td>
				        <td>
				        	<a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href=""><i class="fas fa-pen"></i> Sửa</a>
			                <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href="{{route('delete.shopping.cart',$key)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
				        </td>
			      </tr>
			      <?php $i++ ?>
			      	@endforeach
			    </tbody>
			  </table>
			  <h4 class="pull-right">Tổng tiền thanh toán: {{\Cart::subtotal()}} <a href="{{route('get.form.pay')}}" class="btn-success btn">Thanh toán</a></h4>

       </div>
  </div>
 
@endsection