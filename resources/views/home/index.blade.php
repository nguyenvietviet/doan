@extends('layouts.app')
@section('content')
<style type="text/css">
   .active {
   color:#ff9705;
   }
  /* .benphai{
      text-align: right;
   }*/
</style>
@include('components.slide')
<div class="container">
<div style="margin: 15px 0 0 0;"><a href="{{route('home')}}" target="_blank"><img alt="CÔ VY LÀM KHÓ CÓ CELLPHONES LO" src="covi.png" title="CÔ VY LÀM KHÓ CÓ CELLPHONES LO" width="100%"></a></div>
</div>
<!-- product section start -->
@if(isset($productHot))
<div class="our-product-area new-product">
   <div class="container" id="myDIV">
      <!-- our-product area start -->
      <div class="row">
         <div class="col-md-3">
            <div class="topbar-left" >
               <aside class="widge-topbar">
                  <div class="bar-title">
                     <h2>Danh mục</h2>
                  </div>
               </aside>
               <aside class="sidebar-content">
                  <ul>
                     @if(isset($categories))
                     @foreach($categories as $cate)
                     <li><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">{{$cate->c_name}}</a></li>
                     @endforeach
                     @endif
                  </ul>
               </aside>
               <aside class="widge-topbar">
                  <div class="bar-title">
                     {{-- <div class="bar-ping"><img src="img/bar-ping.png" alt=""></div> --}}
                     <h2>Tìm kiếm theo giá</h2>
                  </div>
               </aside>
               <aside class="sidebar-content">
                  <div class="sidebar-title">
                     <h6>Khoảng giá</h6>
                  </div>
                  <ul>

                     <li><a class="{{ Request::get('price') == 1 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 1])}}">dưới 1tr</a></li>
                     <li><a class="{{ Request::get('price') == 2 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 2])}}">1.000.000 - 3.000.000đ</a></li>
                     <li><a class="{{ Request::get('price') == 3 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 3])}}">3.000.000 - 5.000.000đ</a></li>
                     <li><a class="{{ Request::get('price') == 4 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 4])}}">5.000.000 - 7.000.000đ</a></li>
                     <li><a class="{{ Request::get('price') == 5 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 5])}}">7.000.000 - 10.000.000đ</a></li>
                     <li><a class="{{ Request::get('price') == 6 ? 'active' : '' }}" href="{{request()->fullUrlWithQuery(['price' => 6])}}">lớn hơn 10.000.000đ</a></li>
                     <li><a href="{{route('home')}}">Tất cả</a></li>
                  </ul>
               </aside>
               <aside class="widge-topbar" style="margin-bottom: 36%;">
                  <div class="bar-title">
                     <h2>Tags</h2>
                  </div>
                  <div class="exp-tags">
                     <div class="tags">
                        <a href="#">Nokia</a>
                        <a href="#">Iphone</a>
                        <a href="#">Samsung</a>
                        <a href="#">Xiaomi</a>
                        <a href="#">Oppo</a>
                     </div>
                  </div>
               </aside>
            </div>
         </div>
         <div class="col-md-9">
            <div class="area-title" style="margin-top: 2%;">
               <h2>Điện thoại nổi bật</h2>
            </div>
            <div class="row" style="border: 1px solid #eee;">
               @foreach($productHot as $proHot)
               <!--Tạo biến price tính giảm giá sản phẩm theo %-->
               <?php 
                  $price = $proHot->pro_price;
                  
                     if($proHot->pro_sale)
                     {
                         $price = $price *(100 - $proHot->pro_sale)/100;
                  }
                   ?>
               <!--Tạo biến price tính giảm giá sản phẩm theo %-->
               <div class="col-md-3" style="border: solid 1px #eee;">
                  <div class="single-product first-sale" >
                     <div class="product-img">
                        @if($proHot->pro_number== 0)
                        <span style="background-color: #e91e63;position: absolute;color: white;padding:2px 6px; border-radius: 5px;font-size: 12px;">Tạm hết hàng</span>
                        @endif
                        @if($proHot->pro_sale)
                        <span style="position: absolute;background-image: linear-gradient(-90deg,#ec1f1f 0%,#ff9c00 100%);border-radius: 10px;padding: 1px 7px;color: white;font-size: 10px;right: 0">Khuyễn mãi {{$proHot->pro_sale}}%</span>
                        @endif
                        <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">
                        <img class="primary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                        <img class="secondary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                        </a>
                        <div class="tra-gop"><span>Trả góp 0%</span></div>
                        {{--  
                        <div class="product_type product_type_hot product_type_order_0">Hot</div>
                        --}}
                        <div class="action-zoom">
                           <div class="add-to-cart">
                              <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}" title="Xem chi tiết"><i class="fa fa-search-plus"></i></a>
                           </div>
                        </div>
                        <div class="actions">
                           <div class="action-buttons">
                              <div class="add-to-links">
                                 <div class="compare-button">
                                    <a href="{{route('add.shopping.cart',$proHot->id)}}" title="Thêm vào giỏ hàng"><i class="fas fa-cart-plus"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        {{-- 
                        <div class="price-box">
                           <span class="new-price">{{number_format($proHot->pro_price,0,',','.')}}đ</span>
                        </div>
                        --}}
                     </div>
                     <div class="product-content">
                        <h2 class="product-name"><a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">{{$proHot->pro_name}}</a></h2>
                        <div class="row">
                           @if($proHot->pro_sale >=1)
                           <h5 class="price" style="color:#f57224;">{{number_format($price,0,',','.')}} đ <span class="old-price" style="font-size: 13px;">{{number_format($proHot->pro_price,0,',','.')}} đ</span></h5>
                           @else
                           <h5 class="price" style="color:#f57224;">{{number_format($proHot->pro_price,0,',','.')}} đ</h5>
                           @endif
                        </div>
                        <p class="shottext">{{$proHot->pro_description}}</p>
                        <a href="{{route('add.shopping.cart',$proHot->id)}}" class="btn btn-primary btn-sm">Đặt mua</a>
                     </div>
                  </div>
               </div>
               @endforeach
               <!-- single-product end -->
            </div>
            <div class="text-center">{{$productHot->links()}}</div>
         </div>
      </div>
       <hr>
   </div>
</div>

@endif
<!-- our-product area end --> 

{{-- @if(isset($category)) --}}
 @foreach($category as $cate)
<div class="our-product-area new-product" style="margin-bottom: 2%;">
   <div class="container" id="myDIV">
      <!-- our-product area start -->
      <div class="row">
         <div class="col-md-12">
            <div class="area-title" style="margin-top: 0;">
               <h2><a href="{{route('get.list.product',[$cate->c_slug,$cate->id])}}">Điện thoại {{$cate->c_name}}</a></h2>
               <a class="benphai" href="">Xem thêm >></a>
            </div>
            <div class="row" style="border: 1px solid #eee;">

            {{-- @foreach($category as $cate) --}}
            {{-- @if(isset($cate->producs)) --}}
                 
                   @foreach($cate->products as $pro)

                         <?php 
                           $price = $pro->pro_price;
                           
                              if($pro->pro_sale)
                              {
                                  $price = $price *(100 - $pro->pro_sale)/100;
                              }
                           ?>
                     <div class="col-md-2" style="border: solid 1px #eee;">
                  <div class="single-product first-sale" >
                     <div class="product-img">
                        @if($pro->pro_number== 0)
                        <span style="background-color: #e91e63;position: absolute;color: white;padding:2px 6px; border-radius: 5px;font-size: 12px;">Tạm hết hàng</span>
                        @endif
                        @if($pro->pro_sale)
                        <span style="position: absolute;background-image: linear-gradient(-90deg,#ec1f1f 0%,#ff9c00 100%);border-radius: 10px;padding: 1px 7px;color: white;font-size: 10px;right: 0">Khuyễn mãi {{$pro->pro_sale}}%</span>
                        @endif
                        <a href="{{route('get.detail.product',[$pro->pro_name,$pro->id])}}">
                        <img class="primary-image" src="{{asset("")}}/{{ pare_url_file($pro->pro_avatar)}}" alt="" />
                        <img class="secondary-image" src="{{asset("")}}/{{ pare_url_file($pro->pro_avatar)}}" alt="" />
                        </a>
                        <div class="tra-gop"><span>Trả góp 0%</span></div>
                        <div class="action-zoom">
                           <div class="add-to-cart">
                              <a href="{{route('get.detail.product',[$pro->pro_name,$pro->id])}}" title="Xem chi tiết"><i class="fa fa-search-plus"></i></a>
                           </div>
                        </div>
                        <div class="actions">
                           <div class="action-buttons">
                              <div class="add-to-links">
                                 <div class="compare-button">
                                    <a href="{{route('add.shopping.cart',$pro->id)}}" title="Thêm vào giỏ hàng"><i class="fas fa-cart-plus"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                     </div>
                     <div class="product-content">
                        <h2 class="product-name"><a href="{{route('get.detail.product',[$pro->pro_name,$pro->id])}}">{{$pro->pro_name}}</a></h2>
                        <div class="row">
                           @if($pro->pro_sale >=1)
                           <h5 class="price" style="color:#f57224;">{{number_format($price,0,',','.')}}đ<span class="old-price" style="font-size: 13px;">{{number_format($pro->pro_price,0,',','.')}}đ</span></h5>
                           @else
                           <h5 class="price" style="color:#f57224;">{{number_format($pro->pro_price,0,',','.')}} đ</h5>
                           @endif
                        </div>
                        <p class="shottext">{{$pro->pro_description}}</p>
                        <a href="{{route('add.shopping.cart',$pro->id)}}" class="btn btn-primary btn-sm">Đặt mua</a>
                     </div>
                  </div>
               </div>
                      
                   @endforeach
                   {{-- @endif --}}
            {{-- <div class="text-center">{{$product->links()}}</div> --}}
         </div>
      </div>
       <hr>
   </div>

</div>
</div>
 @endforeach
{{-- @endif --}}

<!--TTTTTTTTTTTTTTTTTTTTTTTTTTT-->




<!-- latestpost area start -->
@if(isset($articleNews))
<div class="latest-post-area">
   <div class="container">
      <div class="area-title">
         <h2>Bài viết mới</h2>
      </div>
      <div class="row" style="border: 1px solid #ccc!important;border-radius: 7px;">
         <div class="all-singlepost">
            <!-- single latestpost start -->
            @foreach($articleNews as $arNew)
            <div class="col-md-3 col-sm-4">
               <div class="single-post" style="margin-bottom: 40px;">
                  <div class="post-thumb">
                     <a href="{{route('get.detail.article',[$arNew->a_slug,$arNew->id])}}">
                     <img src="{{asset("")}}/{{ pare_url_file($arNew->a_avatar)}}" alt="" style="width: 100%;height: 180px; object-fit: cover;" />
                     </a>
                  </div>
                  <div class="post-thumb-info">
                     <div class="postexcerpt">
                        <p style="height:50px;">{{$arNew->a_name}}</p>
                        <a href="{{route('get.detail.article',[$arNew->a_slug,$arNew->id])}}" class="read-more">Xem thêm</a>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach
            <div class="text-center">{{$articleNews->links()}}</div>
            <!-- single latestpost end -->
         </div>
      </div>
   </div>
</div>
@endif



@include('components.product_view')
<!-- latestpost area end -->
<!-- block category area start -->
<div class="block-category">
   <div class="container">
      <div class="row">
         @if(isset($categoriesHome))
         <!-- featured block start -->
         @foreach($categoriesHome as $categoriHome)
         <div class="col-md-4">
            <!-- block title start -->
            <div class="block-title">
               <h2 style="border-radius: 7px;">{{$categoriHome->c_name}}</h2>
            </div>
            @if(isset($categoriHome->products))
            <div class="block-carousel">
               @foreach($categoriHome->products as $product)
               <?php 
                  $ageDetail =0;
                  if($product->pro_total_rating)
                  {
                     $ageDetail = round($product->pro_total_number/$product->pro_total_rating,2);
                  }
                  ?>
               <!--Tạo biến Price để tính giảm giá-->
               <?php 
                  $price = $product->pro_price;
                  
                     if($product->pro_sale)
                     {
                         $price = $price *(100 - $product->pro_sale)/100;
                  }
                   ?>
               <!--Tạo biến Price để tính giảm giá-->
               <div class="block-content">
                  <!-- single block start -->
                  <div class="single-block">
                     <div class="block-image pull-left">
                        <a href="{{route('get.detail.product',[$product->pro_name,$product->id])}}"><img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" style="width: 170px;height: 208px;" alt="" /></a>
                     </div>
                     <div class="category-info">
                        <h3><a href="{{route('get.detail.product',[$product->pro_name,$product->id])}}">{{$product->pro_name}}</a></h3>
                        <p>{{$product->depscription}}</p>

                     @if($product->pro_sale >=1)
                        <div class="cat-price">{{$price}} <span class="old-price">{{number_format($product->pro_price,0,',','.')}}</span></div>
                     @else
                        <div class="cat-price">{{number_format($product->pro_price,0,',','.')}}</div>
                     @endif

                        <div class="cat-rating">
                           @for($i=1; $i<=5; $i++)
                           <a><i class="fa fa-star {{$i <= $ageDetail ? 'active' : ''}}"></i></a>
                           @endfor
                        </div>
                     </div>
                  </div>
                  <!-- single block end -->
               </div>
               @endforeach
            </div>
            @endif
         </div>
         @endforeach
         @endif
      </div>
   </div>
</div>
<!-- block category area end -->
<div class="testimonial-area lap-ruffel">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="crusial-carousel">
               <div class="crusial-content">
                  <p>“Mạnh mẽ, bền bỉ, tin cậy"</p>
                  <span>Xuân Quang Mobile</span>
               </div>
               <div class="crusial-content">
                  <p>“Chất lượng - Uy tín - Chính hãng"</p>
                  <span>Xuân Quang Mobile</span>
               </div>
               <div class="crusial-content">
                  <p>“Nếu những gì mà chúng tôi không có ,nghĩa là bạn không cần"</p>
                  <span>Xuân Quang Mobile</span>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop