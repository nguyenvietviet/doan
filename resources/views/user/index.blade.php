@extends('user.layout')
@section('content')


<style>
    .highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#container {
    height: 400px;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
</style>

<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                       <h1><a href="{{route('home')}}">Trang chủ/</a><a>Tổng Quan</a></h1>
                    </div>
                </div>
            </div>
</div>

<div class="content mt-3">

<div class="col-sm-6 col-lg-4">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{$totalTransaction}}</span>
            </h4>
  
            <p class="text-light">Tổng số đơn hàng</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>

<div class="col-sm-6 col-lg-4">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{$totalTransactionDone}}</span>
            </h4>
            <p class="text-light">Đã xử lý</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>

<div class="col-sm-6 col-lg-4">
    <div class="card text-white bg-flat-color-4">
        <div class="card-body pb-0">

            <h4 class="mb-0">
                <span class="count">{{$totalTransaction - $totalTransactionDone}}</span>
            </h4>
            <p class="text-light">Chưa xử lý</p>

            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                <canvas id="widgetChart4"></canvas>
            </div>

        </div>
    </div>
</div>

</div>

<div class="row">
  
  <div class="col-md-12" style="margin-top: 1.3%;">
    <div class="card-header">
                            <strong class="card-title">Danh sách đơn hàng của bạn </strong>
                        </div>
    <table  class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Số ĐT</th>
                        <th>Địa chỉ</th>
                        <th>Tổng tiền</th>
                        <th>Trạng thái đơn hàng</th>
                        <th>Thời gian đặt hàng</th>
                        <th>Cập nhật</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; ?>
                    @if(isset($transactions))
                        @foreach($transactions as $transaction)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$transaction->tr_address}}</td>
                            <td>{{$transaction->tr_phone}}</td>
                            <td>{{number_format($transaction->tr_total,0,',','.')}}VNĐ</td>
                            <td>
                              @if($transaction->tr_status == 1)
                                   <a style="color: white;" class="badge badge-success">Đã xử lý</a>
                                   @elseif($transaction->tr_status == 0)
                                   <a href="{{route('admin.get.active.transaction',$transaction->id)}}" style="color: white;" class="badge badge-secondary">Chờ xử lý</a>
                                   @elseif($transaction->tr_status == 2)
                                   <a style="color: white;" class="badge badge-warning">Đang gửi</a>
                                   @else
                                   <a style="color: white;" class="badge badge-danger">Cancel</a>
                                   @endif
                            </td>
                            <td>{{$transaction->created_at->format('H:i:s | d-m-Y ')}}</td>
                            <td>{{$transaction->updated_at->format('H:i:s | d-m-Y ')}}</td>
                        </tr>
                        @endforeach
                    @endif
                     <?php $i++; ?>
                  </tbody>
               </table>
               <div class="offset-5">
                   {{$transactions ->links()}}
               </div>
  </div>
</div>


@stop