@extends('layouts.app')
@section('content')
<style type="text/css">
   .list_start i:hover{
   cursor: pointer;
   }
   .list_text{
   display: inline-block;
   margin-left: 0;
   position: relative;
   background-color: #52b858;
   color: #fff;
   padding: 2px 8px;
   box-sizing: border-box;
   font-size: 12px;
   border-radius:2px;
   display: none;
   }
   .list_text:after{
   right: 100%;
   top: 50%;
   border: solid transparent;
   content: " ";
   height: 0;
   width: 0;
   position: absolute;
   pointer-events: none;
   border-color: rgba(82,184,88,0);
   border-right-color: #52b858;
   border-width: 6px;
   margin-top: -6px;
   }
	.list_start .rating_active,.pro-rating .active {
		color:#ff9705;
	}

</style>
<div class="breadcrumbs">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="container-inner">
               <ul>
                  <li class="home">
                     <a href="{{route('home')}}">Trang chủ</a>
                     <span><i class="fa fa-angle-right"></i></span>
                  </li>
                  <li class="category3"><span>{{$cateProduct->c_name}}</span></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="product-details-area">
   <div class="container">
      <div class="row">
         {{-- @foreach($productDetail as $proDetail) --}}
         <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="zoomWrapper">
               <div id="img-1" class="zoomWrapper single-zoom">
                  <a href="#">
                     <div style="height:450px;width:450px;" class="zoomWrapper"><img id="zoom1" src="{{asset("")}}/{{ pare_url_file($productDetail->pro_avatar)}}" data-zoom-image="" alt="big-1" style="position: absolute;"></div>
                  </a>
               </div>
            </div>
         </div>
         {{-- @endforeach --}}
         <div class="col-md-7 col-sm-7 col-xs-12">
            <div class="product-list-wrapper">
               <div class="single-product">
                  <div class="product-content">
                     <h2 class="product-name"><a>{{$productDetail->pro_name}}</a></h2>
                     <div class="rating-price">
                        <?php 
                        $ageDetail =0;
                        if($productDetail->pro_total_rating)
                        {
                           $ageDetail = round($productDetail->pro_total_number/$productDetail->pro_total_rating,2);
                        }
                     ?>
                        <div class="pro-rating">
                           @for($i=1; $i<=5; $i++)
                           <a><i class="fa fa-star {{$i <= $ageDetail ? 'active' : ''}}"></i></a>
                           @endfor
                        </div>
                        <div class="price-boxes">
                           <span class="new-price">{{number_format($productDetail->pro_price,0,',','.')}}đ</span>
                        </div>
                     </div>
                     <div class="product-desc">
                        <h5><i>Mô tả:</i></h5>
                        <p>{{$productDetail->pro_description}}</p>
                     </div>
                     {{-- 
                     <p class="availability in-stock">Availability: <span>In stock</span></p>
                     --}}
                     <div class="actions-e">
                        <div class="action-buttons-single">
                           <div class="inputx-content">
                              <label for="qty">Số lượng còn:</label>
                              <input type="text" disabled="" name="qty" id="qty" maxlength="12" value="{{$productDetail->pro_number}}" title="Qty" class="input-text qty">
                           </div>
                           <div class="inputx-content">
                              <label for="qty">Đã bán:</label>
                              <input type="text" disabled="" maxlength="12" value="{{$productDetail->pro_pay}}" title="Qty" class="input-text qty">
                           </div>
                           <div class="add-to-cart">
                              <a href="{{route('add.shopping.cart',$productDetail->id)}}">Đặt mua</a>
                           </div>
                           {{-- <div class="add-to-links">
                              <div class="add-to-wishlist">
                                 <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                              </div>
                              <div class="compare-button">
                                 <a href="#" data-toggle="tooltip" title="" data-original-title="Compare"><i class="fa fa-refresh"></i></a>
                              </div>
                           </div> --}}

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
      <div class="col-md-8">
         <div class="single-product-tab">
            <!-- Nav tabs -->
            <ul class="details-tab">
               <li class="active"><a href="#home" data-toggle="tab">Nội dung chi tiết sản phẩm</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="home">
                  <div class="product-tab-content">
                     <p>{!!$productDetail->pro_content!!}</p>
                  </div>
               </div>
               <div class="component_rating" style="margin-bottom: 20px;">
                  <h3>Đánh giá sản phẩm </h3>
                  <div class="component_rating_content" style="display: flex;align-items: center;border-radius: 5px;border:1px solid #dedede;">
                     <div class="rating_item" style="width: 20%;position: relative;">
                        <span class="fa fa-star" style="font-size: 80px;color: #ff9705;display: block;margin:0 auto;text-align: center;"></span><b style="position: absolute;top: 50%;left: 50%;color: white;font-size: 20px;transform: translateX(-50%) translateY(-50%);">{{$ageDetail}}</b>
                     </div>
                     <div class="list_rating" style="width: 60%;padding:20px;">
                       {{--  @for($i=1; $i<=5;$i++) --}}
                         @foreach($arrayRatings as $key => $arrayRating)

               <?php
                     
                     $itemAge =round( ($arrayRating['total']/$productDetail->pro_total_rating)*100,0);        
                ?>


                        <div class="item_rating" style="display: flex; align-items: center;">
                           <div style="width: 10%;">
                              {{$key}}<span class="fa fa-star"></span>
                           </div>
                           <div style="width: 70%;margin:0 20px;">
                              <span style="width: 100%;height: 6px; display: block;border: 1px solid #dedede;height: 9px;border-radius: 5px;background-color: #EEEEEE;"><b style="width: {{$itemAge}}%;background-color: #f25800;display: block;height: 100%;border-radius: 5px;"></b></span>
                           </div>
                           <div style="width: 20%;">
                              <a href="">{{$arrayRating['total']}} đánh giá {{-- ({{$itemAge}}%) --}}</a>
                           </div>
                        </div>

                              @endforeach
                       {{--  @endfor --}}
                     </div>
                     <div style="width: 20%;">
                        <a class="js_rating_action" style="width: 250px;background-color: #288ad6;border-radius:5px;padding: 10px;color: white; cursor: pointer;">Đánh giá</a>
                     </div>
                  </div>
                  <?php 
                     $listRatingText =[
                     	1 => 'không thích',
                     	2 => 'tạm được',
                     	3 => 'bình thường',
                     	4 => 'tốt',
                     	5 => 'rất tốt',
                     ]
                     ?>
                  <div class="form_rating hide">
                  		<div style="display: flex;margin-top: 15px;" >
		                     <p style="margin-bottom: 0;">Chọn đánh giá của bạn</p>
		                     <span style="margin:0 15px; " class="list_start">
			                     @for($i=1;$i<=5;$i++)
			                     	<i class="fa fa-star" data-key="{{$i}}"></i>
			                     @endfor


		                     </span>
		                     <span class="list_text"></span>
		                     <input type="hidden" value=""  class="number_rating">
		                  </div>
                  	<div style="margin-top: 5px;">
                  		<textarea name="" cols="10" id="ra_content" rows="5" class="form-control"></textarea>
                  	</div>
                  	<div style="margin-top: 15px;">
						<a href="{{route('post.rating.product',$productDetail)}}" class="js_rating_product" style="width: 250px;background-color: #288ad6;border-radius:5px;padding: 10px;color: white; cursor: pointer;">Gửi đánh giá</a>
                  	</div>
                  </div>
               </div>


               <div class="component_list_rating">
                 
                  @if(isset($ratings))
                    @foreach($ratings as $rating)
                        <div class="rating_item" style="margin:10px 0;">
                        <div>
                           <span style="text-transform: uppercase;font-weight: bold;color:#333;">{{isset($rating->user->name) ? $rating->user->name : '[N\A]' }}</span>
                           <a href="" style="color:#2ba832;"><i class="fa fa-check-circle-o"></i> Đã mua hàng tại website</a>
                        </div>
                        <p style="margin-bottom: 0;">
                           <span class="pro-rating">

                              @for($i=1;$i<=5;$i++)
                                 <i class="fa fa-star {{$i<= $rating->ra_number ? 'active': ''}}"></i>
                              @endfor
                           </span>
                           <span>{{$rating->ra_content}}</span>
                        </p>
                        <div>
                           <span>
                              <i class="fa fa-clock-o" aria-hidden="true">{{$rating->created_at}}</i>
                           </span>
                        </div>
                     </div>

                    @endforeach
                  @endif
   
               </div>
               
            </div>
         </div>
      </div>
      <div class="col-md-4">

         <div class="single-product-tab">
            <!-- Nav tabs -->
            <ul class="details-tab">
               <li class="active"><a href="#home" data-toggle="tab">Thông số sản phẩm</a></li>
            </ul>
            <table class="table">
               {!!$productDetail->pro_parameter!!}
          </table>
         </div>

         <div class="single-product-tab">
            <!-- Nav tabs -->
            <ul class="details-tab">
               <li class="active"><a href="#home" data-toggle="tab">Sản phẩm mới nhất</a></li>
            </ul>
            @foreach($productH as $p)
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-6">
                    <a href="{{route('get.detail.product',[$p->pro_name,$p->id])}}"><img src="{{asset("")}}/{{ pare_url_file($p->pro_avatar)}}" style="height: 150px;"></a> 
                  </div>
                  <div class="col-md-6" style="margin-top: 5%;">
                     <a href="{{route('get.detail.product',[$p->pro_name,$p->id])}}"><p style="text-transform: uppercase;"><strong>{{$p->pro_name}}</strong></p></a>
                     <p>Giá: {{number_format($p->pro_price,0,',','.')}}đ</p>
                     <a href="{{route('get.detail.product',[$p->pro_name,$p->id])}}"><p>>> Xem chi tiết </p></a>
                  </div>
               </div>
               <hr>
            </div>
            <hr>
            @endforeach
         </div>
      </div>
      
      </div>
   </div>
   </div>
</div>
<div class="our-product-area new-product">
   <div class="container">
      <div class="area-title">
         <h2>Sản phẩm nổi bật</h2>
      </div>
      <!-- our-product area start -->
      <div class="row">
         <div class="col-md-12">
            <div class="row">
               <div class="features-curosel">
                  <!-- single-product start -->
                  @foreach($productHot as $proHot)
                  <div class="col-lg-12 col-md-12">
                     <div class="single-product first-sale">
                        <div class="product-img">
                           <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">
                           <img class="primary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                           <img class="secondary-image" src="{{asset("")}}/{{ pare_url_file($proHot->pro_avatar)}}" alt="" />
                           </a>
                           <div class="action-zoom">
                              <div class="add-to-cart">
                                 <a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}" title="Quick View"><i class="fa fa-search-plus"></i></a>
                              </div>
                           </div>
                           <div class="actions">
                              <div class="action-buttons">
                                 <div class="add-to-links">
                                    <div class="add-to-wishlist">
                                       <a href="#" title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                    </div>
                                    <div class="compare-button">
                                       <a href="#" title="Add to Cart"><i class="icon-bag"></i></a>
                                    </div>
                                 </div>
                                 <div class="quickviewbtn">
                                    <a href="#" title="Add to Compare"><i class="fa fa-retweet"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="price-box">
                              <span class="new-price">{{number_format($proHot->pro_price,0,',','.')}}đ</span>
                           </div>
                        </div>
                        <div class="product-content">
                           <h2 class="product-name"><a href="{{route('get.detail.product',[$proHot->pro_name,$proHot->id])}}">{{$proHot->pro_name}}</a></h2>
                           <p class="shottext">{{$proHot->pro_description}}</p>
                        </div>
                     </div>
                  </div>
                  @endforeach
                  <!-- single-product end -->
               </div>
            </div>
         </div>
      </div>
      <!-- our-product area end -->   
   </div>
</div>
@stop

@section('js')
	<script>
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$(function(){
			let listStart= $(".list_start .fa");

			listRatingText ={
                     	1 : 'không thích',
                     	2 : 'tạm được',
                     	3 : 'bình thường',
                     	4 : 'tốt',
                     	5 : 'rất tốt',
             }

			listStart.mouseover(function(){
				let $this = $(this);
				let number =$this.attr('data-key');
				listStart.removeClass('rating_active');

				$(".number_rating").val(number);

				$.each(listStart, function(key,value){
					if(key+1 <= number)
					{
						$(this).addClass('rating_active')
					}
				});

				$(".list_text").text('').text(listRatingText[number]).show();

			});

			$(".js_rating_action").click(function(event){
				event.preventDefault();
				if($(".form_rating").hasClass('hide'))
				{
					$(".form_rating").addClass('active').removeClass('hide')
				}else
				{
					$(".form_rating").addClass('hide').removeClass('active')
				}
			});

			$(".js_rating_product").click(function(e){
				event.preventDefault();

				let content = $("#ra_content").val();
				let number = $(".number_rating").val();
				let url = $(this).attr('href');

				if(content && number)
				{
					$.ajax({
					  url: url,
					  type:'POST',
					  data:{
					  	number :number,
					  	r_content : content
					  }
					}).done(function(result) {
					 	if(result.code ==1)
					 	{
					 		alert("Gửi đánh giá thành công");
					 		location.reload();
					 	}
					});
				}
			});
		});
	</script>
@endsection