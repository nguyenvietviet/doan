-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2020 at 02:42 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_cellphone`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `avatar`, `active`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Việt', 'nvviet59@gmail.com', NULL, NULL, 1, '$2y$10$.57.MpWrKJnmbYmkiy9Z6.79HucG/htCkqKTgXXJsdjGkpl0Alsdi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `a_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_content` longtext COLLATE utf8mb4_unicode_ci,
  `a_active` tinyint(4) NOT NULL DEFAULT '1',
  `a_author_id` int(11) NOT NULL DEFAULT '0',
  `a_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_view` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `a_hot` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `a_name`, `a_slug`, `a_description`, `a_content`, `a_active`, `a_author_id`, `a_description_seo`, `a_title_seo`, `a_avatar`, `a_view`, `created_at`, `updated_at`, `a_hot`) VALUES
(1, 'Cách sử dụng đt 1', 'cach-su-dung-dt-1', 'Cách sử dụng đt 2019', '<p>C&aacute;ch sử dụng đt</p>', 1, 0, 'Cách sử dụng đt', 'Cách sử dụng đt', '2019-11-20__oppo-a5-2020-white-400x460.png', 0, '2019-11-16 09:56:38', '2019-12-31 04:28:16', 1),
(2, 'iPhone 7: Thiết kế nhàm chán liệu?', 'iphone-7-thiet-ke-nham-chan-lieu', 'iPhone 7: Thiết kế nhàm chán?', '<p>Bẵng đi 2 năm kể từ khi iPhone 6 rồi cả 6S ra mắt, mọi người tr&ocirc;ng chờ một m&agrave;n kế nghiệp thần sầu tới từ gương mặt mới toanh mang t&ecirc;n iPhone 7. Tuy nhi&ecirc;n, tại thời điểm lộ diện, thế hệ n&agrave;y vẫn chưa thực sự khiến fan h&acirc;m mộ ấn tượng mạnh mẽ với những lời rỉ tai nhau rằng &quot;thiết kế y hệt đời trước&quot;, &quot;camera k&eacute;p lồi v&agrave; th&ocirc;&quot;. Lời chế giễu đ&oacute; kh&ocirc;ng phải l&agrave; kh&ocirc;ng c&oacute; cơ sở, khi hầu như mọi thay đổi tr&ecirc;n thiết kế iPhone 7 chỉ l&agrave; c&aacute;ch sắp xếp dải ăng-ten kh&aacute;c đi, bỏ cổng tai nghe truyền thống v&agrave; th&ecirc;m camera k&eacute;p tr&ecirc;n phi&ecirc;n bản Plus. Sự lồi của camera vẫn được l&ocirc;i ra b&agrave;n t&aacute;n nhiều khi c&aacute;c đối thủ kh&aacute;c hầu hết đ&atilde; loại bỏ diện mạo đ&oacute;, trong khi Apple vẫn &quot;ngoan cố&quot; cứng đầu.</p>', 1, 0, 'iPhone 7: Thiết kế nhàm chán', 'iPhone 7: Thiết kế nhàm chán', '2019-11-20__iphone.jpg', 0, '2019-11-16 10:10:05', '2019-12-28 07:17:10', 0),
(3, 'laptop giảm khủng 20%, cơ hội hiếm có đừng bỏ lỡ', 'laptop-giam-khung-20-co-hoi-hiem-co-dung-bo-lo', 'Khuyến mãi duy nhất ngày 20/11/2019.', '<p>Chương tr&igrave;nh Hot sale 20/11 năm nay, kh&aacute;ch h&agrave;ng c&oacute; thể mua laptop Online tại Thế Giới Di Động sẽ được giảm gi&aacute; 20%, duy nhất trong ng&agrave;y h&ocirc;m nay (20/11). Mua laptop cho Ng&agrave;y nh&agrave; gi&aacute;o từ c&aacute;c thương hiệu nổi tiếng với ch&iacute;nh s&aacute;ch bảo h&agrave;nh uy t&iacute;n c&ograve;n được tặng k&egrave;m th&ecirc;m loạt ưu đ&atilde;i gi&aacute; trị.</p>', 1, 0, 'Hôm nay (20/11) laptop giảm khủng 20%, cơ hội hiếm có, săn ngay', 'Hôm nay (20/11) laptop giảm khủng 20%, cơ hội hiếm có, săn ngay', '2019-11-20__tin.jpg', 0, '2019-11-19 22:12:47', '2019-12-28 07:17:26', 0),
(4, 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi ?', 'iphone-x-tai-tho-bi-anti-day-tranh-cai', 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', '<p>Mặc d&ugrave; gl&agrave; chiếc smartphone đột ph&aacute; nhất sau 10 năm ph&aacute;t triển của Apple đồng thời gần như hiện thực ho&aacute; được thiết kế m&agrave;n h&igrave;nh tr&agrave;n cạnh, iPhone X vẫn mang tr&ecirc;n m&igrave;nh một &quot;c&aacute;i gai&quot; kh&oacute; bỏ: Phần &quot;tai thỏ&quot; ở đỉnh cạnh tr&ecirc;n. Đ&acirc;y l&agrave; biểu hiện r&otilde; nhất cho thấy Apple vẫn chưa sẵn s&agrave;ng nghĩ ra một phương &aacute;n kh&aacute;c để giấu đi camera trước c&ugrave;ng c&aacute;c cảm biến, bắt buộc phải giữ lại ở một khoảng ch&egrave;n v&agrave;o m&agrave;n h&igrave;nh n&agrave;y. Kẻ ti&ecirc;n phong lu&ocirc;n gặp nhiều trắc trở, d&ugrave; c&oacute; những ấn tượng tốt nhưng nhiều fan kh&oacute; t&iacute;nh vẫn chưa thực sự bị thuyết phục 100% bởi phần &quot;tai thỏ&quot; n&agrave;y. Việc cắt khuyết lạ lẫm cũng phần n&agrave;o để lại nhiều hệ quả phức tạp như h&igrave;nh ảnh bị che lấp khi xem video, chơi game...</p>', 1, 0, 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', 'iPhone X: \"Tai thỏ\" bị anti đầy tranh cãi', '2019-11-20__iphonex.jpg', 0, '2019-11-19 22:17:28', '2019-12-28 07:17:38', 0),
(5, 'Các điểm tổ chức sự kiện chào năm mới 2020', 'cac-diem-to-chuc-su-kien-chao-nam-moi-2020', 'Chương trình chào năm mới 2020 gồm hoạt động chính là nhạc hội, một số nơi tổ chức bắn pháo hoa.', '<h1>C&aacute;c điểm tổ chức sự kiện ch&agrave;o năm mới 2020</h1>\r\n\r\n<p>Chương tr&igrave;nh ch&agrave;o năm mới 2020 gồm hoạt động ch&iacute;nh l&agrave; nhạc hội, một số nơi tổ chức bắn ph&aacute;o hoa.</p>\r\n\r\n<p>Theo kế hoạch, nhiều địa phương tổ chức hoạt động đếm ngược thời khắc chuyển giao năm cũ v&agrave; năm mới (countdown).&nbsp;</p>\r\n\r\n<p><strong>H&agrave; Nội&nbsp;</strong>tổ chức lễ countdown tại Quảng trường C&aacute;ch mạng Th&aacute;ng T&aacute;m v&agrave; vườn hoa tượng đ&agrave;i L&yacute; Th&aacute;i Tổ. Ngo&agrave;i ra, Quảng trường Đ&ocirc;ng Kinh Nghĩa Thục sẽ c&oacute; chương tr&igrave;nh &acirc;m nhạc điện tử s&ocirc;i động, c&ugrave;ng nhiều hoạt động văn nghệ, tr&ograve; chơi quanh phố đi bộ Hồ Gươm.</p>\r\n\r\n<p>Chương tr&igrave;nh &acirc;m nhạc đếm ngược v&agrave; bắn ph&aacute;o hoa&nbsp;<strong>Đ&agrave; Nẵng&nbsp;</strong>sẽ diễn ra từ 21h ng&agrave;y 31/12 đến 0h ng&agrave;y 1/1/2020 tại Quảng trường 29 th&aacute;ng 3.&nbsp;</p>\r\n\r\n<p>Tại&nbsp;<strong>TP HCM</strong>, chương tr&igrave;nh &acirc;m nhạc countdown 31/12 được tổ chức tại phố đi bộ Nguyễn Huệ (đoạn từ Mạc Thị Bưởi đến Ng&ocirc; Đức Kế) từ&nbsp;20h.&nbsp;Ph&aacute;o hoa sẽ được bắn ở ba điểm l&agrave; đường hầm s&ocirc;ng S&agrave;i G&ograve;n (hầm Thủ Thi&ecirc;m) quận 2, t&ograve;a nh&agrave; Landmark 81 v&agrave; C&ocirc;ng vi&ecirc;n Central Park, quận B&igrave;nh Thạnh; C&ocirc;ng vi&ecirc;n Văn h&oacute;a Đầm Sen, quận 11 từ&nbsp;0h đến 0h15 ng&agrave;y 1/1/2020. Th&agrave;nh phố c&ograve;n tổ chức chương tr&igrave;nh chiếu s&aacute;ng nghệ thuật v&agrave; đồ họa 3D tr&ecirc;n mặt tiền t&ograve;a nh&agrave; UBND, đường L&ecirc; Th&aacute;nh T&ocirc;n v&agrave;o tối 31/12 v&agrave; 1/1/2020.&nbsp;</p>', 1, 0, 'Chương trình chào năm mới 2020 gồm hoạt động chính là nhạc hội, một số nơi tổ chức bắn pháo hoa.', 'Các điểm tổ chức sự kiện chào năm mới 2020', '2019-12-28__daiphunnuochunguyn-1577333869-6006-1577335673.jpg', 0, '2019-12-28 07:52:09', '2019-12-31 04:38:58', 1),
(6, 'Những điểm khách đổ đến, người dân tránh xa', 'nhung-diem-khach-do-den-nguoi-dan-tranh-xa', 'Những điểm khách đổ đến, người dân tránh xa', '<p>Nhiều người khuyến c&aacute;o du kh&aacute;ch kh&ocirc;ng n&ecirc;n mất tiền v&agrave; thời gian tới Champs-Elys&eacute;es, Time Square, Pattaya d&ugrave; ch&uacute;ng được xem l&agrave; biểu tượng du lịch.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/champs-elysees-m_680x0.jpg\" style=\"height:383px; width:680px\" /></p>\r\n\r\n<p><strong>Champs-Elys&eacute;es, Paris, Ph&aacute;p</strong></p>\r\n\r\n<p>Celine, nh&acirc;n vi&ecirc;n thiết kế thời trang, chia sẻ: &ldquo; Kh&ocirc;ng ai sống ở Paris t&igrave;nh nguyện tới đ&acirc;y, trừ khi họ tham gia c&aacute;c cuộc biểu t&igrave;nh. C&oacute; qu&aacute; nhiều du kh&aacute;ch, những nh&agrave; h&agrave;ng, cửa h&agrave;ng đắt đỏ. Đ&acirc;y chắc chắn kh&ocirc;ng phải con đường đẹp nhất ở th&agrave;nh phố&rdquo;. Ban ng&agrave;y, những đo&agrave;n du kh&aacute;ch v&agrave; người th&iacute;ch &quot;sống ảo&quot; khiến con phố trở n&ecirc;n qu&aacute; tải. &ldquo;Du kh&aacute;ch n&ecirc;n đi dạo con k&ecirc;nh, ở bờ s&ocirc;ng Seine, khu Marais, hoặc xung quanh Montorgueil&rdquo;, Celine n&oacute;i. Ảnh:<em>&nbsp;France 24</em>.</p>', 1, 0, 'Những điểm khách đổ đến, người dân tránh xa', 'Những điểm khách đổ đến, người dân tránh xa', '2019-12-28__8-diem-khach-do-den-nguoi-dan-tranh-xa-1577360771.jpg', 0, '2019-12-28 07:53:52', '2019-12-28 07:53:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_icon` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_active` tinyint(4) NOT NULL DEFAULT '1',
  `c_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_total_product` int(11) NOT NULL DEFAULT '0',
  `c_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_keyword_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `c_home` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `c_name`, `c_slug`, `c_icon`, `c_avatar`, `c_active`, `c_title_seo`, `c_total_product`, `c_description_seo`, `c_keyword_seo`, `created_at`, `updated_at`, `c_home`) VALUES
(3, 'Sam Sung', 'sam-sung', 'fa-apple', NULL, 1, 'Điện thoại samsung', 0, 'Điện thoại samsung', NULL, '2019-11-14 06:04:10', '2019-12-30 10:16:41', 1),
(4, 'Xiaomi', 'xiaomi', 'fa-mobile', NULL, 1, 'Điện thoại Xiaomi', 0, 'Điện thoại Xiaomi', NULL, '2019-11-14 06:04:31', '2019-12-30 10:16:49', 1),
(5, 'Apple', 'apple', 'fa-mobile', NULL, 1, 'Điện thoại apple', 0, 'Điện thoại apple', NULL, '2019-11-20 06:03:38', '2019-12-30 16:14:16', 1),
(6, 'Huawei', 'huawei', 'fa-mobile', NULL, 1, 'Điện thoại Huawei', 0, 'Điện thoại Huawe', NULL, '2019-11-20 06:04:14', '2019-11-20 06:04:14', 0),
(7, 'Nokia', 'nokia', 'fa-mobile', NULL, 1, 'Điện thoại Nokia', 0, 'Điện thoại Nokia', NULL, '2019-11-20 06:04:59', '2019-11-20 06:04:59', 0),
(8, 'OPPO', 'oppo', 'fa-mobile', NULL, 1, 'Điện thoại OPPO', 0, 'Điện thoại OPPO', NULL, '2019-11-20 06:05:46', '2019-11-20 06:05:46', 0),
(9, 'Realme', 'realme', 'fa-fa-mobile', NULL, 1, 'Realme', 0, 'điện thoại Realme', NULL, '2020-01-10 07:49:08', '2020-01-10 07:49:08', 0),
(10, 'VinSmart', 'vinsmart', 'fa-fa-mobile', NULL, 1, 'điện thoại VinSmart', 0, 'điện thoại VinSmart', NULL, '2020-01-10 07:49:47', '2020-01-10 07:49:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_content` text COLLATE utf8mb4_unicode_ci,
  `c_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `c_name`, `c_title`, `c_email`, `c_content`, `c_status`, `created_at`, `updated_at`) VALUES
(1, 'nguyễn việt', 'mua', 'nvviet59@gmail.com', 'mua hàng', 0, NULL, NULL),
(2, 'nguyễn việt địa', 'đơn mua', 'abc@gmail.com', 'mua hàng', 0, '2019-12-01 09:12:39', '2019-12-01 09:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(16, '2014_10_12_000000_create_users_table', 1),
(17, '2014_10_12_100000_create_password_resets_table', 1),
(18, '2019_11_12_043257_create_categories_table', 1),
(19, '2019_11_13_185702_create_products_table', 1),
(20, '2019_11_13_193132_alter_column_pro_content_and_pro_title_seo_in_table_products', 1),
(22, '2019_11_16_160931_create_article_table', 2),
(23, '2019_12_01_155321_create_contact_table', 3),
(24, '2019_12_09_114002_create_transactions_table', 4),
(25, '2019_12_09_114026_create_orders_table', 4),
(26, '2019_12_09_115021_alter_column_pro_pay_in_table_products', 4),
(27, '2019_12_27_113410_create_ratings_table', 5),
(28, '2019_12_27_114442_alter_column_rating_in_table_products', 6),
(29, '2019_12_28_132521_alter_column_total_pay_in_table_users', 7),
(30, '2019_12_30_100453_create_page_statics_table', 8),
(31, '2019_12_30_165758_alter_column_c_home_in_table_categories', 9),
(32, '2019_12_31_093435_create_admins_table', 10),
(33, '2019_12_31_112052_alter_column_a_hot_in_table_articles', 11),
(34, '2020_01_02_113001_create_parameters_table', 12),
(35, '2020_01_02_113648_alter_column_pro_parameter_id_in_products_table', 12),
(36, '2020_01_02_164107_create_slides_table', 13),
(37, '2020_01_08_105936_alter_column_about_end_address_in_users_table', 14),
(38, '2020_01_08_150640_alter_column_code_and_time_code_in_users_table', 15),
(39, '2020_01_10_131424_alter_column_code_active_in_users_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `or_transaction_id` int(11) NOT NULL DEFAULT '0',
  `or_product_id` int(11) NOT NULL,
  `or_qty` tinyint(4) NOT NULL DEFAULT '0',
  `or_price` int(11) NOT NULL DEFAULT '0',
  `or_sale` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `or_transaction_id`, `or_product_id`, `or_qty`, `or_price`, `or_sale`, `created_at`, `updated_at`) VALUES
(1, 4, 6, 1, 4, 4, NULL, NULL),
(2, 4, 10, 1, 0, 0, NULL, NULL),
(3, 5, 7, 1, 2, 2, NULL, NULL),
(4, 6, 7, 1, 32323, 2, NULL, NULL),
(5, 6, 10, 1, 21990000, 0, NULL, NULL),
(6, 10, 7, 1, 32323, 2, NULL, NULL),
(7, 11, 7, 1, 32323, 2, NULL, NULL),
(8, 12, 11, 1, 7400000, 0, NULL, NULL),
(9, 13, 9, 1, 5600000, 2, NULL, NULL),
(10, 14, 11, 1, 7400000, 0, NULL, NULL),
(11, 15, 10, 1, 21990000, 50, NULL, NULL),
(12, 16, 10, 1, 21990000, 50, NULL, NULL),
(13, 17, 10, 1, 21990000, 50, NULL, NULL),
(14, 18, 10, 1, 21990000, 50, NULL, NULL),
(15, 19, 10, 1, 21990000, 50, NULL, NULL),
(16, 20, 14, 1, 8000000, 0, NULL, NULL),
(17, 21, 10, 1, 21990000, 50, NULL, NULL),
(18, 23, 10, 1, 21990000, 50, NULL, NULL),
(19, 24, 11, 1, 7400000, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_statics`
--

CREATE TABLE `page_statics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ps_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ps_type` tinyint(4) NOT NULL DEFAULT '1',
  `ps_content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_statics`
--

INSERT INTO `page_statics` (`id`, `ps_name`, `ps_type`, `ps_content`, `created_at`, `updated_at`) VALUES
(1, 'về chúng tôi', 1, '<p>nội dung về ch&uacute;ng t&ocirc;i</p>', '2019-12-30 04:01:10', '2019-12-30 04:01:10'),
(3, 'Thông tin giao hàng', 2, '<p><strong>I. Vận chuyển trong nội ngoại th&agrave;nh S&agrave;i G&ograve;n:</strong></p>\r\n\r\n<p><em>a. Kh&aacute;ch h&agrave;ng&nbsp;muốn đặt giao h&agrave;ng xin để lại:</em></p>\r\n\r\n<p>- M&atilde; số sản phẩm c&aacute;c bạn muốn đặt (nếu c&oacute;)</p>\r\n\r\n<p>- Số điện thoại của bạn.</p>\r\n\r\n<p>- Thời gian + địa điểm lấy h&agrave;ng.</p>\r\n\r\n<p>- để G&agrave; Tươi Long B&igrave;nh giao h&agrave;ng nhanh hơn cho qu&yacute; kh&aacute;ch.</p>\r\n\r\n<p><em>b. Thời gian giao h&agrave;ng v&agrave; cước ph&iacute;:</em></p>\r\n\r\n<p>- Nhận ship tất cả c&aacute;c quận trong TP.HCM kể cả c&aacute;c quận xa như B&igrave;nh T&acirc;n, B&igrave;nh Ch&aacute;nh, G&ograve; Vấp, Thủ Đức, H&oacute;c M&ocirc;n&hellip;</p>\r\n\r\n<p>- Giao trong khoản thời gian từ 8h tới 17h (đơn h&agrave;ng sau 18h th&igrave; qua h&ocirc;m sau mới giao)</p>\r\n\r\n<p><strong>- Gi&aacute; giao h&agrave;ng:</strong></p>\r\n\r\n<p>Mức ph&iacute; giao h&agrave;ng: 30k (gồm c&aacute;c&nbsp;quận 12, H&oacute;c M&ocirc;n, B&igrave;nh T&acirc;n,&nbsp;quận 2, 9, Thủ Đức ,quận B&igrave;nh Ch&aacute;nh, Nh&agrave; B&egrave;, Củ Chi)</p>\r\n\r\n<p>- Ph&iacute; n&agrave;y trả cho b&ecirc;n cty chuyển ph&aacute;t h&agrave;ng hoặc nh&acirc;n vi&ecirc;n giao h&agrave;ng</p>\r\n\r\n<p><strong>II. Vận chuyển c&aacute;c tỉnh th&agrave;nh kh&aacute;c:</strong></p>\r\n\r\n<p>- Li&ecirc;n hệ G&agrave; Tươi Long B&igrave;nh&nbsp;để vận chuyển h&agrave;ng c&aacute;ch tốt nhất.</p>\r\n\r\n<p>- hotline : 091.6610.228</p>', '2020-01-02 08:31:07', '2020-01-02 08:31:07'),
(4, 'Thông tin bảo mật', 3, '<p>Ch&uacute;ng t&ocirc;i ph&aacute;t triển một loạt c&aacute;c dịch vụ gi&uacute;p h&agrave;ng triệu người h&agrave;ng ng&agrave;y kh&aacute;m ph&aacute; v&agrave; tương t&aacute;c với thế giới theo những c&aacute;ch mới. C&aacute;c dịch vụ của ch&uacute;ng t&ocirc;i gồm c&oacute;:</p>\r\n\r\n<ul>\r\n	<li>C&aacute;c ứng dụng, trang web v&agrave; thiết bị của Google, chẳng hạn như T&igrave;m kiếm, YouTube v&agrave; Google Home</li>\r\n	<li>C&aacute;c nền tảng như tr&igrave;nh duyệt Chrome v&agrave; hệ điều h&agrave;nh Android</li>\r\n	<li>C&aacute;c sản phẩm t&iacute;ch hợp v&agrave;o c&aacute;c ứng dụng v&agrave; trang web của b&ecirc;n thứ ba, chẳng hạn như c&aacute;c quảng c&aacute;o v&agrave; Google Maps được nh&uacute;ng</li>\r\n</ul>\r\n\r\n<p>Bạn c&oacute; thể sử dụng c&aacute;c dịch vụ của ch&uacute;ng t&ocirc;i theo nhiều c&aacute;ch kh&aacute;c nhau để quản l&yacute; quyền ri&ecirc;ng tư của m&igrave;nh. V&iacute; dụ: bạn c&oacute; thể đăng k&yacute; một T&agrave;i khoản Google nếu muốn tạo v&agrave; quản l&yacute; những nội dung như email v&agrave; ảnh, hoặc xem th&ecirc;m c&aacute;c kết quả t&igrave;m kiếm c&oacute; li&ecirc;n quan. Ngo&agrave;i ra, bạn c&oacute; thể sử dụng nhiều dịch vụ của Google khi đ&atilde; đăng xuất hoặc kh&ocirc;ng cần tạo t&agrave;i khoản, chẳng hạn như t&igrave;m kiếm tr&ecirc;n Google hoặc xem c&aacute;c video tr&ecirc;n YouTube. Bạn cũng c&oacute; thể chọn duyệt web ở chế độ ri&ecirc;ng tư bằng c&aacute;ch sử dụng Chrome ở Chế độ ẩn danh. Hơn nữa, tr&ecirc;n những dịch vụ của ch&uacute;ng t&ocirc;i, bạn c&oacute; thể điều chỉnh c&aacute;c mục c&agrave;i đặt bảo mật để kiểm so&aacute;t th&ocirc;ng tin ch&uacute;ng t&ocirc;i thu thập v&agrave; c&aacute;ch th&ocirc;ng tin của bạn được sử dụng.</p>\r\n\r\n<p>Để gi&uacute;p giải th&iacute;ch mọi điều r&otilde; r&agrave;ng nhất c&oacute; thể, ch&uacute;ng t&ocirc;i đ&atilde; th&ecirc;m c&aacute;c v&iacute; dụ, video giải th&iacute;ch v&agrave; c&aacute;c định nghĩa cho những&nbsp;<a href=\"https://policies.google.com/privacy/key-terms?hl=vi#key-terms\">thuật ngữ ch&iacute;nh</a>. Nếu c&oacute; bất kỳ c&acirc;u hỏi n&agrave;o về Ch&iacute;nh s&aacute;ch bảo mật n&agrave;y, bạn c&oacute; thể&nbsp;<a href=\"https://support.google.com/policies?p=privpol_privts&amp;hl=vi\" target=\"_blank\">li&ecirc;n hệ với ch&uacute;ng</a>&nbsp;t&ocirc;i</p>', '2020-01-02 08:31:45', '2020-01-02 08:31:45'),
(5, 'Điều khoản sửa dụng', 4, '<h2>Sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i</h2>\r\n\r\n<p>Bạn phải tu&acirc;n thủ mọi ch&iacute;nh s&aacute;ch đ&atilde; cung cấp cho bạn trong phạm vi Dịch vụ.</p>\r\n\r\n<p>Kh&ocirc;ng được sử dụng tr&aacute;i ph&eacute;p Dịch vụ của ch&uacute;ng t&ocirc;i. V&iacute; dụ: kh&ocirc;ng được g&acirc;y trở ngại cho Dịch vụ của ch&uacute;ng t&ocirc;i hoặc t&igrave;m c&aacute;ch truy cập ch&uacute;ng bằng phương ph&aacute;p n&agrave;o đ&oacute; kh&ocirc;ng th&ocirc;ng qua giao diện v&agrave; hướng dẫn m&agrave; ch&uacute;ng t&ocirc;i cung cấp. Bạn chỉ c&oacute; thể sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i theo như được luật ph&aacute;p cho ph&eacute;p, bao gồm cả c&aacute;c luật v&agrave; quy định hiện h&agrave;nh về quản l&yacute; xuất khẩu v&agrave; t&aacute;i xuất khẩu. Ch&uacute;ng t&ocirc;i c&oacute; thể tạm ngừng hoặc ngừng cung cấp Dịch vụ của ch&uacute;ng t&ocirc;i cho bạn nếu bạn kh&ocirc;ng tu&acirc;n thủ c&aacute;c điều khoản hoặc ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hoặc nếu ch&uacute;ng t&ocirc;i đang điều tra h&agrave;nh vi bị nghi ngờ l&agrave; sai phạm.</p>\r\n\r\n<p>Việc bạn sử dụng Dịch vụ của ch&uacute;ng t&ocirc;i kh&ocirc;ng c&oacute; nghĩa l&agrave; bạn được sở hữu bất cứ c&aacute;c quyền sở hữu tr&iacute; tuệ n&agrave;o đối với Dịch vụ của ch&uacute;ng t&ocirc;i hoặc n&ocirc;̣i dung m&agrave; bạn truy c&acirc;̣p. Bạn kh&ocirc;ng được sử dụng n&ocirc;̣i dung từ Dịch vụ của ch&uacute;ng t&ocirc;i trừ khi bạn được chủ sở hữu nội dung đ&oacute; cho ph&eacute;p hoặc được luật ph&aacute;p cho ph&eacute;p. C&aacute;c điều khoản n&agrave;y kh&ocirc;ng cấp cho bạn quyền sử dụng bất kỳ thương hiệu hoặc l&ocirc;g&ocirc; n&agrave;o được sử dụng trong Dịch vụ của ch&uacute;ng t&ocirc;i. Kh&ocirc;ng được x&oacute;a, che khuất hoặc thay đổi bất kỳ th&ocirc;ng b&aacute;o ph&aacute;p l&yacute; n&agrave;o được hiển thị trong hoặc k&egrave;m theo Dịch vụ của ch&uacute;ng t&ocirc;i.</p>\r\n\r\n<p>Dịch vụ của ch&uacute;ng t&ocirc;i hiển thị một số n&ocirc;̣i dung kh&ocirc;ng phải của Google. Chỉ một m&igrave;nh đối tượng cung cấp sẽ chịu tr&aacute;ch nhiệm về nội dung n&agrave;y. Ch&uacute;ng t&ocirc;i c&oacute; thể xem x&eacute;t n&ocirc;̣i dung để x&aacute;c định xem nội dung đ&oacute; c&oacute; bất hợp ph&aacute;p hoặc vi phạm ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hay kh&ocirc;ng, v&agrave; ch&uacute;ng t&ocirc;i c&oacute; thể x&oacute;a hoặc từ chối hiển thị n&ocirc;̣i dung m&agrave; ch&uacute;ng t&ocirc;i c&oacute; l&yacute; do ch&iacute;nh đ&aacute;ng để tin rằng nội dung đ&oacute; vi phạm ch&iacute;nh s&aacute;ch của ch&uacute;ng t&ocirc;i hoặc luật ph&aacute;p. Tuy nhi&ecirc;n, điều đ&oacute; kh&ocirc;ng nhất thiết c&oacute; nghĩa l&agrave; ch&uacute;ng t&ocirc;i sẽ xem x&eacute;t nội dung, do vậy bạn đừng cho rằng ch&uacute;ng t&ocirc;i sẽ l&agrave;m như vậy.</p>\r\n\r\n<p>Li&ecirc;n quan đến việc bạn sử dụng Dịch vụ, ch&uacute;ng t&ocirc;i c&oacute; thể gửi cho bạn c&aacute;c th&ocirc;ng b&aacute;o dịch vụ, th&ocirc;ng b&aacute;o quản trị v&agrave; th&ocirc;ng tin kh&aacute;c. Bạn c&oacute; thể chọn kh&ocirc;ng nhận một số th&ocirc;ng b&aacute;o n&agrave;y.</p>\r\n\r\n<p>Một số c&aacute;c Dịch vụ của ch&uacute;ng t&ocirc;i c&oacute; sẵn tr&ecirc;n c&aacute;c thiết bị di động. Kh&ocirc;ng sử dụng c&aacute;c Dịch vụ đ&oacute; theo c&aacute;ch khiến bạn bị mất tập trung v&agrave; ngăn cản bạn tu&acirc;n thủ c&aacute;c luật về an to&agrave;n hoặc giao th&ocirc;ng.</p>', '2020-01-02 08:32:11', '2020-01-02 08:32:11');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `para_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `para_name`, `created_at`, `updated_at`) VALUES
(1, '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Độ ph&acirc;n giải</p>\r\n			</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Camera sau</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Camera trước</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Dung lượng Pin</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Dung lượng</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Ram</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Th&ocirc;ng số CPU</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Ch&iacute;p&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Quay phim</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>', '2020-01-02 05:03:47', '2020-01-02 09:10:58');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_category_id` int(11) NOT NULL DEFAULT '0',
  `pro_price` int(11) NOT NULL DEFAULT '0',
  `pro_author_id` int(11) NOT NULL DEFAULT '0',
  `pro_sale` tinyint(4) NOT NULL DEFAULT '0',
  `pro_active` tinyint(4) NOT NULL DEFAULT '1',
  `pro_hot` tinyint(4) NOT NULL DEFAULT '0',
  `pro_view` int(11) NOT NULL DEFAULT '0',
  `pro_description` text COLLATE utf8mb4_unicode_ci,
  `pro_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_description_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_keyword_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pro_title_seo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_content` longtext COLLATE utf8mb4_unicode_ci,
  `pro_pay` tinyint(4) NOT NULL DEFAULT '1',
  `pro_number` tinyint(4) NOT NULL DEFAULT '1',
  `pro_total_rating` int(11) NOT NULL DEFAULT '0' COMMENT 'Tổng số đánh giá',
  `pro_total_number` int(11) NOT NULL DEFAULT '0' COMMENT 'Tổng số điểm đánh giá',
  `pro_parameter` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pro_name`, `pro_slug`, `pro_category_id`, `pro_price`, `pro_author_id`, `pro_sale`, `pro_active`, `pro_hot`, `pro_view`, `pro_description`, `pro_avatar`, `pro_description_seo`, `pro_keyword_seo`, `created_at`, `updated_at`, `pro_title_seo`, `pro_content`, `pro_pay`, `pro_number`, `pro_total_rating`, `pro_total_number`, `pro_parameter`) VALUES
(9, 'Điện thoại Samsung Galaxy Note 5', 'dien-thoai-samsung-galaxy-note-5', 3, 5600000, 0, 2, 1, 1, 0, 'Điện thoại Samsung Galaxy Note 5', '2019-11-20__nokia-61-plus-3-400x460.png', 'Điện thoại Samsung Galaxy Note 5', NULL, '2019-11-20 09:11:15', '2019-12-31 07:10:09', 'Điện thoại Samsung Galaxy Note 5', '<p>Nội dung h&igrave;nh ảnh test&nbsp;Điện thoại Samsung Galaxy Note 5</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/nokia-61-plus-3-400x460.png\" style=\"height:460px; width:400px\" /></p>', 2, 1, 1, 4, NULL),
(10, 'Iphone 11', 'iphone-11', 5, 21990000, 0, 50, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng iPhone 11 64GB', '2019-11-20__iphone-11-red-2-400x460.png', 'Iphone 11', NULL, '2019-11-20 09:39:51', '2020-01-08 10:17:27', 'Iphone 11', '<h3 dir=\"ltr\" style=\"text-align:center\">N&acirc;ng cấp mạnh mẽ về camera</h3>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">N&oacute;i về n&acirc;ng cấp th&igrave; camera ch&iacute;nh l&agrave; điểm c&oacute; nhiều cải tiến nhất tr&ecirc;n thế hệ&nbsp;<a href=\"https://www.thegioididong.com/dtdd-apple-iphone\" target=\"_blank\" title=\"Tham khảo giá điện thoại smartphone iPhone chính hãng\">iPhone</a>&nbsp;mới.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/iphone-11-tgdd42.jpg\" style=\"height:469px; width:800px\" /></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">Nếu như trước đ&acirc;y iPhone Xr chỉ c&oacute; một camera th&igrave; nay với iPhone 11 ch&uacute;ng ta sẽ c&oacute; tới hai camera ở mặt sau.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">Ngo&agrave;i camera ch&iacute;nh vẫn c&oacute; độ ph&acirc;n giải 12 MP th&igrave; ch&uacute;ng ta sẽ c&oacute; th&ecirc;m một camera g&oacute;c si&ecirc;u rộng v&agrave; cũng với độ ph&acirc;n giải tương tự.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/iphone-11-tgdd36.jpg\" style=\"height:600px; width:800px\" /></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">Theo Apple th&igrave; việc chuyển đổi qua lại giữa hai ống k&iacute;nh th&igrave; sẽ kh&ocirc;ng l&agrave;m thay đổi m&agrave;u sắc của bức ảnh</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">Năm nay Apple cũng đ&atilde; n&acirc;ng cấp độ ph&acirc;n giải camera trước n&ecirc;n 12 MP thay v&igrave; 7 MP như thế hệ trước đ&oacute;.<img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/iphone-11-tgdd6.jpg\" style=\"height:600px; width:800px\" /></p>', 7, 12, 0, 0, '<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Th&ocirc;ng số</td>\r\n			<td>Chi tiết th&ocirc;ng số</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ ph&acirc;n giải</td>\r\n			<td>1080x1920</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Camera sau</td>\r\n			<td>16Mpx</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Camera trước</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dung lượng Pin</td>\r\n			<td>4000mAh</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dung lượng</td>\r\n			<td>250Gb</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ram</td>\r\n			<td>2Gb</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&ocirc;ng số CPU</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ch&iacute;p&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(11, 'Điện thoại Xiaomi Mi 9 SE', 'dien-thoai-xiaomi-mi-9-se', 4, 7400000, 0, 0, 1, 1, 0, 'KHUYẾN MÃI\r\nGiảm thêm 5% (430.000₫) cho khách mua online có sinh nhật trong tháng 11 Xem chi tiết *\r\nGiảm ngay 1 triệu (đã trừ vào giá)\r\n* Không áp dụng khi mua trả góp 0%', '2019-11-21__xiaomi-mi-9-se-blue-18thangbh-400x460.png', 'Điện thoại Xiaomi Mi 9 SE', NULL, '2019-11-21 10:13:27', '2020-01-08 10:21:32', 'Điện thoại Xiaomi Mi 9 SE', '<h2>Vẫn như thường lệ th&igrave; b&ecirc;n cạnh chiếc flagship&nbsp;<a href=\"https://www.thegioididong.com/dtdd/xiaomi-mi-9\" target=\"_blank\" title=\"Tham khảo điện thoại Xiaomi Mi 9 chính hãng\">Xiaomi Mi 9</a>&nbsp;cao cấp của m&igrave;nh th&igrave; Xiaomi cũng giới thiệu th&ecirc;m phi&ecirc;n bản r&uacute;t gọn l&agrave; chiếc&nbsp;<a href=\"https://www.thegioididong.com/dtdd/xiaomi-mi-9-se\" target=\"_blank\" title=\"Tham khảo điện thoại Xiaomi Mi 9 SE chính hãng\">Xiaomi Mi 9 SE</a>. M&aacute;y vẫn sở hữu cho m&igrave;nh nhiều trang bị cao cấp tương tự đ&agrave;n anh.</h2>\r\n\r\n<h3>Thiết kế sang trọng tương tự Xiami Mi 9</h3>\r\n\r\n<p>Về ngoại h&igrave;nh th&igrave; chiếc&nbsp;<a href=\"https://www.thegioididong.com/dtdd\" target=\"_blank\" title=\"Tham khảo các dòng điện thoại chính hãng\">smartphone</a>&nbsp;Xiaomi Mi 9 SE kh&aacute; tương đồng với người anh em cao cấp v&agrave; bạn sẽ vẫn c&oacute; một&nbsp;thiết kế mặt lưng với hiệu ứng đổi m&agrave;u gradient đẹp mắt.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/xiaomi-mi-9-se-tgdd-9.jpg\" style=\"height:533px; width:800px\" /></p>\r\n\r\n<p>K&iacute;ch thước m&agrave;n h&igrave;nh tr&ecirc;n m&aacute;y được r&uacute;t gọn xuống chỉ c&ograve;n&nbsp;5.97 inch với viền m&agrave;n h&igrave;nh được l&agrave;m cong 2.5D rất hiện đại.</p>\r\n\r\n<p>M&aacute;y vẫn giữ kiểu thiết kế m&agrave;n h&igrave;nh giọt nước qua đ&oacute; tăng tỉ lệ m&agrave;n h&igrave;nh ở mặt trước l&ecirc;n tới&nbsp;90.47%.</p>\r\n\r\n<h3>Được trang bị con chip ho&agrave;n to&agrave;n mới</h3>\r\n\r\n<p>Năm ngo&aacute;i chiếc&nbsp;Xiaomi Mi 8 SE l&agrave; chiếc m&aacute;y đầu ti&ecirc;n được trang bị con chip&nbsp;Snapdragon 710 th&igrave; chiếc Mi 9 SE năm nay cũng l&agrave; chiếc m&aacute;y đầu ti&ecirc;n được trang bị con chip&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/snapdragon-712-ra-mat-hieu-nang-tang-10-so-snapdragon-710-1148674\" target=\"_blank\" title=\"Tìm hiểu chip Snapdragon 712 8 nhân 64-bit\">Snapdragon 712 8 nh&acirc;n 64-bit</a>.</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/xiaomi-mi-9-se-tgdd-9(1).jpg\" style=\"height:533px; width:800px\" /></p>\r\n\r\n<p>Con chip mới n&agrave;y cho hiệu năng tốt hơn 10% so với thế hệ cũ v&agrave; bạn c&oacute; thể tự tin &quot;chiến&quot; mọi tựa game nặng nhất hiện nay tr&ecirc;n Mi 9 SE.</p>\r\n\r\n<h3>Camera số một trong tầm gi&aacute;</h3>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/xiaomi-mi-9-se-camera-tgdd-3.jpg\" style=\"height:600px; width:800px\" /><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/xiaomi-mi-9-se-camera-tgdd-4.jpg\" style=\"height:600px; width:800px\" /><img alt=\"\" src=\"http://localhost/Web_Cellphone/public/uploads/files/xiaomi-mi-9-se-tgdd-7.jpg\" style=\"height:533px; width:800px\" /></p>\r\n\r\n<p>Mặt trước vẫn l&agrave; camera 20 MP với chế độ l&agrave;m đẹp được t&iacute;ch hợp sẵn thoải m&aacute;i cho bạn selfie với bạn b&egrave;.</p>\r\n\r\n<p>Ngo&agrave;i ra khả năng xo&aacute; ph&ocirc;ng của m&aacute;y cũng rất tốt, kh&ocirc;ng bị lem nhiều v&agrave;o vật thể v&agrave; l&agrave;m mờ được hậu cảnh ph&iacute;a sau.</p>', 4, 9, 1, 3, NULL),
(12, 'Sam sung galaxy s10+(250Gb)', 'sam-sung-galaxy-s10250gb', 3, 29000000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy S10+ (512GB)', '2019-12-28__sieu-pham-galaxy-s-moi-2-512gb-black-600x600.jpg', NULL, NULL, '2019-12-28 08:29:21', '2019-12-28 08:40:42', 'Sam sung galaxy s10+(250Gb)', '<p>M&agrave;n h&igrave;nh của m&aacute;y c&oacute; k&iacute;ch thước 6.4 inch c&ugrave;ng độ ph&acirc;n giải khủng 2K+ cho bạn thưởng thức những bộ phim hay xem những h&igrave;nh ảnh sắc n&eacute;t.</p>\r\n\r\n<p><a href=\"https://www.thegioididong.com/tin-tuc/hdr10-la-gi-vi-sao-no-lam-cho-man-hinh-galaxy-s10-dep-hon--1151189\" target=\"_blank\" title=\"Tìm hiểu công nghệ HDR10+\" type=\"Tìm hiểu công nghệ HDR10+\">C&ocirc;ng nghệ HDR10+</a>&nbsp;ti&ecirc;n tiến nhất hiện nay cho bạn một trải nghiệm h&igrave;nh ảnh thực sự kh&aacute;c biệt so với phần c&ograve;n lại của thế giới smartphone.</p>\r\n\r\n<p>Xem th&ecirc;m:&nbsp;<a href=\"https://www.thegioididong.com/tin-tuc/tren-tay-galaxy-s10-va-s10-plus-dung-nhu-loi-don-1148424\" target=\"_blank\" title=\"Trên tay Galaxy S10 Plus\" type=\"Trên tay Galaxy S10 Plus\">Tr&ecirc;n tay Galaxy S10 v&agrave; S10 Plus từ reviewer nổi tiếng: Đ&uacute;ng như lời đồn!</a></p>\r\n\r\n<h3>Camera được n&acirc;ng l&ecirc;n tầm cao mới</h3>\r\n\r\n<p>Những chiếc Galaxy S tới từ&nbsp;<a href=\"https://www.thegioididong.com/dtdd-samsung\" target=\"_blank\" title=\"Tham khảo các dòng điện thoại Samsung chính hãng\">Samsung</a>&nbsp;lu&ocirc;n được người d&ugrave;ng đ&aacute;nh gi&aacute; cao về camera v&agrave; với&nbsp;Samsung Galaxy S10+ (512GB) cũng kh&ocirc;ng phải l&agrave; một ngoại lệ.</p>\r\n\r\n<p>Trang c&ocirc;ng nghệ&nbsp;DxOMark - trang chuy&ecirc;n đ&aacute;nh gi&aacute; về camera cũng đ&atilde; đưa ra nhận x&eacute;t về Galaxy S10+ l&agrave; một trong những chiếc m&aacute;y c&oacute; camera tốt nhất tr&ecirc;n thị trường nhờ khả năng chụp si&ecirc;u rộng cũng như h&igrave;nh ảnh sắc n&eacute;t, m&agrave;u sắc rực rỡ.</p>\r\n\r\n<h3>Sở hữu con chip mạnh mẽ nhất năm 2019</h3>\r\n\r\n<p>Con chip&nbsp;Exynos 9820 kết hợp với 8 GB RAM đủ sức cho bạn c&oacute; thể sử dụng tất cả game v&agrave; ứng dụng nặng nhất hiện nay một c&aacute;ch mượt m&agrave;, bất kể l&agrave; Li&ecirc;n Qu&acirc;n Mobile, Free Fire hay PUBG.</p>', 1, 10, 0, 0, NULL),
(13, 'Sam sung galaxy note 9', 'sam-sung-galaxy-note-9', 3, 180000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy Note 9', '2019-12-28__samsung-galaxy-a50-black-600x600.jpg', NULL, NULL, '2019-12-28 08:32:30', '2019-12-28 16:44:22', 'Sam sung galaxy note 9', '<p>Ngo&agrave;i những thao t&aacute;c điều khiển m&aacute;y từ xa bằng c&aacute;ch chạm, g&otilde; như Samsung đ&atilde; giới thiệu tr&ecirc;n&nbsp;<a href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-note-9\" target=\"_blank\" title=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\" type=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\">Note 9</a>, b&uacute;t S Pen của Note 10 nay c&ograve;n c&oacute; thể thực hiện nhiều hơn thế với động t&aacute;c vẫy, uốn tay nhờ những cảm biến n&agrave;y.</p>\r\n\r\n<p>V&iacute; dụ ứng dụng camera tr&ecirc;n m&aacute;y hỗ trợ kết nối với S Pen, cho ph&eacute;p bạn biến b&uacute;t S Pen th&agrave;nh c&ocirc;ng cụ gi&uacute;p bạn điều chỉnh ống k&iacute;nh camera, thay đổi m&agrave;u sắc, zoom,... giống như đang thao t&aacute;c bằng tay.</p>\r\n\r\n<p>Ngo&agrave;i những thao t&aacute;c điều khiển m&aacute;y từ xa bằng c&aacute;ch chạm, g&otilde; như Samsung đ&atilde; giới thiệu tr&ecirc;n&nbsp;<a href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-note-9\" target=\"_blank\" title=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\" type=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\">Note 9</a>, b&uacute;t S Pen của Note 10 nay c&ograve;n c&oacute; thể thực hiện nhiều hơn thế với động t&aacute;c vẫy, uốn tay nhờ những cảm biến n&agrave;y.</p>\r\n\r\n<p>V&iacute; dụ ứng dụng camera tr&ecirc;n m&aacute;y hỗ trợ kết nối với S Pen, cho ph&eacute;p bạn biến b&uacute;t S Pen th&agrave;nh c&ocirc;ng cụ gi&uacute;p bạn điều chỉnh ống k&iacute;nh camera, thay đổi m&agrave;u sắc, zoom,... giống như đang thao t&aacute;c bằng tay.</p>\r\n\r\n<p>Ngo&agrave;i những thao t&aacute;c điều khiển m&aacute;y từ xa bằng c&aacute;ch chạm, g&otilde; như Samsung đ&atilde; giới thiệu tr&ecirc;n&nbsp;<a href=\"https://www.thegioididong.com/dtdd/samsung-galaxy-note-9\" target=\"_blank\" title=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\" type=\"Tham khảo điện thoại Samsung Galaxy Note 9 chính hãng, giá rẻ\">Note 9</a>, b&uacute;t S Pen của Note 10 nay c&ograve;n c&oacute; thể thực hiện nhiều hơn thế với động t&aacute;c vẫy, uốn tay nhờ những cảm biến n&agrave;y.</p>\r\n\r\n<p>V&iacute; dụ ứng dụng camera tr&ecirc;n m&aacute;y hỗ trợ kết nối với S Pen, cho ph&eacute;p bạn biến b&uacute;t S Pen th&agrave;nh c&ocirc;ng cụ gi&uacute;p bạn điều chỉnh ống k&iacute;nh camera, thay đổi m&agrave;u sắc, zoom,... giống như đang thao t&aacute;c bằng tay.</p>', 1, 20, 0, 0, NULL),
(14, 'Sam sung galaxy note 11', 'sam-sung-galaxy-note-11', 3, 8000000, 0, 0, 1, 1, 0, 'Sam sung galaxy note 11', '2019-12-28__note11.jpg', NULL, NULL, '2019-12-28 08:34:29', '2020-01-02 09:23:32', 'Sam sung galaxy note 11', '<p>M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.</p>\r\n\r\n<p>M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.</p>\r\n\r\n<p>M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.</p>\r\n\r\n<p>M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.M&aacute;y c&oacute; k&iacute;ch thước 6.3 inch c&ugrave;ng độ ph&acirc;n giải Full HD+,&nbsp;được trang bị&nbsp;<a href=\"https://www.thegioididong.com/hoi-dap/cong-nghe-ma-hinh-dynamic-amoled-co-gi-noi-bat-1151123\" target=\"_blank\" title=\"Tìm hiểu về tấm nền Dynamic AMOLED\" type=\"Tìm hiểu về tấm nền Dynamic AMOLED\">tấm nền Dynamic AMOLED</a>&nbsp;mang lại m&agrave;u sắc đậm đ&agrave; v&agrave; sống động hơn.</p>\r\n\r\n<p>&nbsp;</p>', 2, 1, 0, 0, NULL),
(15, 'Sam sung galaxy tab6', 'sam-sung-galaxy-tab6', 3, 14000000, 0, 0, 1, 1, 0, 'Ưu đãi khi mua Phụ Kiện cùng Samsung Galaxy Tab S6', '2019-12-28__sieu-pham-galaxy-s-moi-2-512gb-black-600x600.jpg', NULL, NULL, '2019-12-28 08:36:39', '2020-01-02 09:11:09', 'Sam sung galaxy tab6', '<h3>Ngoại h&igrave;nh mỏng nhẹ, đẹp mắt</h3>\r\n\r\n<p>Ấn tượng đầu ti&ecirc;n về chiếc&nbsp;<a href=\"https://www.thegioididong.com/may-tinh-bang-samsung\" target=\"_blank\" title=\"Tham khảo máy tính bảng Samsung chính hãng\">m&aacute;y t&iacute;nh bảng Samsung</a>&nbsp;n&agrave;y ch&iacute;nh l&agrave; n&oacute; rất mỏng v&agrave; sang trọng, nếu&nbsp;bạn đang c&oacute; suy nghĩ l&agrave; những chiếc m&aacute;y t&iacute;nh bảng thường c&oacute; k&iacute;ch thước lớn, trọng lượng nặng v&agrave; kh&oacute; mang theo th&igrave; đ&acirc;y sẽ l&agrave; một trải nghiệm ho&agrave;n to&agrave;n kh&aacute;c.</p>\r\n\r\n<p>Samsung Galaxy Tab S6&nbsp;ho&agrave;n to&agrave;n hỗ trợ tốt mọi người d&ugrave;ng cho mọi nhu cầu sử dụng: từ giải tr&iacute; xem phim, chơi game, cho đến học tập, l&agrave;m việc khi phải sử dụng m&aacute;y l&acirc;u d&agrave;i.</p>\r\n\r\n<p>Samsung n&oacute;i rằng 86% những người đ&atilde; mua Galaxy Tab S4 đ&atilde; sử dụng ứng dụng m&aacute;y ảnh &quot;thường xuy&ecirc;n&quot; v&igrave; thế h&atilde;ng muốn n&acirc;ng cao trải nghiệm về nhiếp ảnh tr&ecirc;n chiếc tablet mới của m&igrave;nh.</p>\r\n\r\n<p>Ngo&agrave;i cảm biến ch&iacute;nh 13 MP th&igrave; m&aacute;y sở hữu th&ecirc;m 1 cảm biến g&oacute;c si&ecirc;u rộng với độ ph&acirc;n giải 5 MP nữa.</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Th&ocirc;ng tin</td>\r\n			<td>Chi tiết</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Camera</td>\r\n			<td>13px</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ram</td>\r\n			<td>4gb</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>', 1, 20, 0, 0, '<table border=\"1\" bordercolor=\"#ccc\" cellpadding=\"5\" cellspacing=\"0\" style=\"border-collapse:collapse\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">\r\n			<p><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp;<small> </small>&nbsp; &nbsp; &nbsp;&nbsp;</span></strong></p>\r\n			</td>\r\n			<td style=\"text-align:center\"><strong><span style=\"color:#000000\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Chi tiết th&ocirc;ng số&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Độ ph&acirc;n giải</p>\r\n			</td>\r\n			<td>123</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Camera sau</p>\r\n			</td>\r\n			<td>\r\n			<p>123</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Camera trước</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Dung lượng Pin</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Dung lượng</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Ram</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Th&ocirc;ng số CPU</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Ch&iacute;p&nbsp;</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>&nbsp; &nbsp;Quay phim</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>'),
(16, 'Máy tính bảng 21in23', 'may-tinh-bang-21in23', 3, 3000000, 0, 0, 1, 0, 0, 'Máy tính bảng 21in23 Máy tính bảng 21in23', '2019-12-28__tab6.jpg', NULL, NULL, '2019-12-28 16:40:45', '2020-01-02 06:08:32', 'Máy tính bảng 21in23', '<p>Bộ đ&ocirc;i camera k&eacute;p ở mặt sau của m&aacute;y c&oacute; độ ph&acirc;n giải ch&iacute;nh 16 MP v&agrave; phụ 5 MP, trong đ&oacute; ống k&iacute;nh phụ g&oacute;c rộng l&agrave; 5 MP cho ph&eacute;p chụp ảnh được bao qu&aacute;t v&agrave; lạ mắt hơn.</p>\r\n\r\n<p>Bộ đ&ocirc;i camera k&eacute;p ở mặt sau của m&aacute;y c&oacute; độ ph&acirc;n giải ch&iacute;nh 16 MP v&agrave; phụ 5 MP, trong đ&oacute; ống k&iacute;nh phụ g&oacute;c rộng l&agrave; 5 MP cho ph&eacute;p chụp ảnh được bao qu&aacute;t v&agrave; lạ mắt hơn.</p>\r\n\r\n<p>Bộ đ&ocirc;i camera k&eacute;p ở mặt sau của m&aacute;y c&oacute; độ ph&acirc;n giải ch&iacute;nh 16 MP v&agrave; phụ 5 MP, trong đ&oacute; ống k&iacute;nh phụ g&oacute;c rộng l&agrave; 5 MP cho ph&eacute;p chụp ảnh được bao qu&aacute;t v&agrave; lạ mắt hơn.</p>\r\n\r\n<p>Bộ đ&ocirc;i camera k&eacute;p ở mặt sau của m&aacute;y c&oacute; độ ph&acirc;n giải ch&iacute;nh 16 MP v&agrave; phụ 5 MP, trong đ&oacute; ống k&iacute;nh phụ g&oacute;c rộng l&agrave; 5 MP cho ph&eacute;p chụp ảnh được bao qu&aacute;t v&agrave; lạ mắt hơn.</p>', 1, 4, 0, 0, '<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Th&ocirc;ng số</td>\r\n			<td>Chi tiết th&ocirc;ng số</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Độ ph&acirc;n giải</td>\r\n			<td>1280x190</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Camera sau</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Camera trước</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dung lượng Pin</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dung lượng</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ram</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Th&ocirc;ng số CPU</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ch&iacute;p&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ra_product_id` int(11) NOT NULL DEFAULT '0',
  `ra_number` tinyint(4) NOT NULL DEFAULT '0',
  `ra_content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ra_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `ra_product_id`, `ra_number`, `ra_content`, `ra_user_id`, `created_at`, `updated_at`) VALUES
(3, 9, 4, 'sản phẩm tốt', 4, '2019-12-27 08:43:23', '2019-12-27 08:43:23'),
(4, 11, 3, 'tốt', 4, '2019-12-30 09:21:15', '2019-12-30 09:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noidung` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `ten`, `noidung`, `hinh`, `created_at`, `updated_at`) VALUES
(2, 'Thuận Thành Tech', '<p>eee 123123</p>', 'FP_slide1.jpg', '2020-01-02 09:54:24', '2020-01-02 10:22:34'),
(3, 'Slide2', '<p>slide2</p>', '7s_slide2.jpg', '2020-01-02 10:23:00', '2020-01-02 10:23:11');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tr_user_id` int(11) NOT NULL DEFAULT '0',
  `tr_total` int(11) NOT NULL DEFAULT '0',
  `tr_note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tr_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `tr_user_id`, `tr_total`, `tr_note`, `tr_address`, `tr_phone`, `tr_status`, `created_at`, `updated_at`) VALUES
(7, 4, 31677, 'ship ngay và luôn', 'Bắc Ninh', '09231232', 1, '2019-12-18 10:28:43', '2019-12-30 08:52:15'),
(8, 4, 31677, 'ship đi', 'test lại', '09231232', 1, '2019-12-18 10:34:31', '2019-12-30 08:52:27'),
(12, 4, 7400000, 'tôi muốn mua trong 2 ngày', 'Hà nội - Thanh xuân', '09231232', 1, '2019-12-30 08:55:20', '2019-12-30 09:06:16'),
(13, 4, 5488000, 'rrrrrrrrrrrrrrrrrrrrrrrrrrr', 'hn', '09231232', 1, '2019-12-30 08:57:53', '2019-12-31 07:10:09'),
(14, 4, 7400000, 'thử transaction', 'hn', '09231232', 1, '2019-12-31 07:18:22', '2019-12-31 07:18:37'),
(15, 4, 10995000, 'Test trường pay', 'Thanh hóa', '09231232', 1, '2019-12-31 07:23:09', '2019-12-31 07:23:55'),
(16, 4, 10995000, 'số lượng', 'qqqqqqq', '09231232', 1, '2019-12-31 07:51:40', '2019-12-31 07:51:51'),
(17, 4, 10995000, 'qưe', '22', '09231232', 1, '2019-12-31 07:52:41', '2019-12-31 07:52:59'),
(18, 4, 10995000, 'ê', 'ê', '09231232', 1, '2019-12-31 07:54:58', '2019-12-31 07:56:07'),
(19, 4, 10995000, 'qưe', 'qưe', '09231232', 1, '2019-12-31 07:56:40', '2019-12-31 07:56:50'),
(20, 4, 8000000, 'tôi muốn mua', 'hà nội', '09231232', 1, '2020-01-02 09:23:20', '2020-01-02 09:23:32'),
(21, 4, 10995000, 'Cần mua đt', 'TP.HCM', '09231232', 0, '2020-01-08 03:25:37', '2020-01-08 03:25:37'),
(23, 5, 10995000, 'ko', 'bn', '0923123232', 1, '2020-01-08 09:10:31', '2020-01-08 10:17:27'),
(24, 5, 7400000, 'ko có', 'BN', '0923123232', 1, '2020-01-08 09:31:38', '2020-01-08 10:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_pay` int(11) NOT NULL DEFAULT '0',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_code` timestamp NULL DEFAULT NULL,
  `code_active` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_active` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `avatar`, `active`, `password`, `remember_token`, `created_at`, `updated_at`, `total_pay`, `address`, `about`, `code`, `time_code`, `code_active`, `time_active`) VALUES
(4, 'huy', 'emlacaloc@gmail.com', '09231232', NULL, 1, '$2y$10$9rp3So6wgoi04rSKzgm4Mel8vcvWiThVCiatSSapkkc21N5gvr/4K', NULL, '2019-11-21 23:39:00', '2020-01-08 06:58:26', 12, 'Nghi an trmaj lộ', 'giới thiệu bnar thân', NULL, NULL, NULL, NULL),
(5, 'Nguyễn Viết Việt', 'nvviet59@gmail.com', '0923123232', NULL, 2, '$2y$10$bJMqNkkP3rtvYa1tPzpNTuzeAO3uHddnC75eDRI7KOZHoKxmLnC/2', NULL, '2019-11-22 08:54:02', '2020-01-10 01:50:57', 2, NULL, NULL, '$2y$10$TVJeJQx/MiVu8mSiU8q1heiHbzLYvoESg/L9KnkSTtOCtPkl8X42W', '2020-01-10 01:50:57', NULL, NULL),
(6, 'ngViet', 'huy@gmail.com', '021212', NULL, 1, '$2y$10$291IL8H0MbcSQ5spWDJCEOuOTlD3FOkDTYjd8.woNrrI.AG140sQO', NULL, '2019-12-09 03:27:00', '2019-12-09 03:27:00', 3, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Nguyễn Hiệu', 'nnn@gmail.com', '091293', NULL, 2, '$2y$10$zQwP3qXwq5RXa.kK8v1H9upQ0o8KV8YuM/kqViMsSaPr106JyrZX.', NULL, '2020-01-10 06:41:58', '2020-01-10 06:48:59', 0, NULL, NULL, NULL, NULL, '$2y$10$E3ikTaoJwGIC0o/Fo/XyQe0GVLeqrMn8Ymu0kEK2OjRmKky6d4toW', '2020-01-10 06:41:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD KEY `admins_active_index` (`active`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_a_name_unique` (`a_name`),
  ADD KEY `articles_a_slug_index` (`a_slug`),
  ADD KEY `articles_a_active_index` (`a_active`),
  ADD KEY `articles_a_author_id_index` (`a_author_id`),
  ADD KEY `articles_a_hot_index` (`a_hot`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_c_name_unique` (`c_name`),
  ADD KEY `categories_c_slug_index` (`c_slug`),
  ADD KEY `categories_c_active_index` (`c_active`),
  ADD KEY `categories_c_home_index` (`c_home`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_or_transactions_id_index` (`or_transaction_id`),
  ADD KEY `orders_or_product_id_index` (`or_product_id`);

--
-- Indexes for table `page_statics`
--
ALTER TABLE `page_statics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_statics_ps_type_index` (`ps_type`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_pro_slug_index` (`pro_slug`),
  ADD KEY `products_pro_category_id_index` (`pro_category_id`),
  ADD KEY `products_pro_author_id_index` (`pro_author_id`),
  ADD KEY `products_pro_active_index` (`pro_active`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_ra_product_id_index` (`ra_product_id`),
  ADD KEY `ratings_ra_user_id_index` (`ra_user_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_tr_user_id_index` (`tr_user_id`),
  ADD KEY `transactions_tr_status_index` (`tr_status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_active_index` (`active`),
  ADD KEY `users_code_index` (`code`),
  ADD KEY `users_code_active_index` (`code_active`),
  ADD KEY `users_time_active_index` (`time_active`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `page_statics`
--
ALTER TABLE `page_statics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
