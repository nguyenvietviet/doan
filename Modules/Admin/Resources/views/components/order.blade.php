@if($orders)
<table class="table">
			    <thead>
			      <tr>
			        <th>STT</th>
			        <th>Tên SP</th>
			        <th>Hình ảnh</th>
			        <th>Giá</th>
			        <th>Số lượng</th>
			        <th>Thành tiền</th>
			        <th>Thao tác</th>
			      </tr>
			    </thead>
			    <tbody>
			    	<?php $i=1 ?>
			    	@foreach($orders as $key => $order)
			      <tr>
				      	<td>#{{$i}}</td>
				        <td><a href="{{route('get.detail.product',[str_slug($order->product->pro_name),$order->or_product_id])}}" target="_blank">{{isset($order->product->pro_name) ? $order->product->pro_name : ''}}</a></td>
				        <td>
				        	<img style="width: 80px;height: 80px;" src="{{asset("")}}/{{isset($order->product->pro_avatar) ? pare_url_file($order->product->pro_avatar): ''}}">
				        </td>
				        <td>{{number_format($order->or_price)}} đ</td>
				        <td>{{$order->or_qty}}</td>
				        <td>{{number_format($order->or_qty * $order->or_price,0,',','.')}} đ</td>
				        <td>
				        		
			                <a class="badge badge-danger" style="padding:5px 10px;border:1px solid #eee;" href=""><i class="fas fa-trash-alt"></i> Xóa</a>
				        </td>
			      </tr>
			      <?php $i++ ?>
			      	@endforeach
			    </tbody>
			  </table>
@endif