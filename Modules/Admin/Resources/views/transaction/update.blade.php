@extends('admin::layouts.master')
@section('content')

<style>
   input[type=radio] {
      margin:5px;
   }
</style>
{{-- 
<div class="card-header">
   --}}
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Đơn hàng</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Cập nhật đơn hàng</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   {{-- 
</div>
--}}
<div class="card">
<div class="card-header">
   <strong>Cập nhật</strong> 
</div>
<div class="card-body card-block">
   <form action="" method="post" enctype="multipart/form-data">
      @csrf
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <label>Tên khách hàng</label>
                  <input type="text" value="{{$transaction->user->name}}" disabled="" class="form-control">
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <label>Địa chỉ</label>
                  <input type="text" value="{{$transaction->tr_address}}" disabled="" class="form-control">
               </div>
            </div>
             <div class="col-md-4">
               <div class="form-group">
                  <label>Điện thoại</label>
                  <input type="number" value="{{$transaction->tr_phone}}" disabled="" class="form-control">
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <label>Thời gian đặt hàng</label>
                  <input type="text" value="{{$transaction->created_at->format('d-m-Y')}}" disabled="" class="form-control">
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <label>Tổng tiền</label>
                  <input type="text" value="{{number_format($transaction->tr_total,0,',','.')}} VNĐ" disabled="" class="form-control">
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <label>Phương thức thanh toán</label>
                  <br>

               <div style="margin-top: 2%;">
                  <label class="radio-inline">
                     <input value="1"
                     @if($transaction->tr_type == 1)
                     {{"checked"}}
                     @endif
                     type="radio" disabled="">Thường
                  </label>
                  <label class="radio-inline">
                     <input value="2"
                     @if($transaction->tr_type == 2)
                     {{"checked"}}
                     @endif
                     type="radio" disabled="">Online
                  </label>
               </div>
               </div>
            </div>
         </div>
      <div class="col-md-6">
            <div class="form-group">
               <label for="name">Trạng thái giao hàng</label>

               <br>
               <label class="radio-inline">
               <input name="quyen" value="0"
               @if($transaction->tr_status == 0)
               {{"checked"}}
               @endif
               type="radio">Chờ xử lý
               </label>
               <br>       
               <label class="radio-inline">
               <input name="quyen" value="1"
               @if($transaction->tr_status == 1)
               {{"checked"}}
               @endif
               type="radio">Đã xử lý
               </label>
               <br>
               <label class="radio-inline">
               <input name="quyen" value="2"
               @if($transaction->tr_status == 2)
               {{"checked"}}
               @endif
               type="radio">Đang gửi
               </label>
               <br>
               <label class="radio-inline">
               <input name="quyen" value="3"
               @if($transaction->tr_status == 3)
               {{"checked"}}
               @endif
               type="radio">Cancel
               </label>

            </div>
         </div>
            <button type="submit" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Lưu thông tin
            </button>
      </div>
   </form>
</div>
</div>   
@endsection