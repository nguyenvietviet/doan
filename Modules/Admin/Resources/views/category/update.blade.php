@extends('admin::layouts.master')
@section('content')
{{-- <div class="card-header"> --}}
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.get.list.category')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Cập nhật</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
{{-- </div> --}}
<section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-warning">
            <div class="card-header">
              <h3 class="card-title">Cập nhật</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            @include("admin::category.form")
      
   </div>
  </div>
</div>
</div>
</section>
@endsection