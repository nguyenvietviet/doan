 <style>
    .error-text{
      color:red;
    }
  </style>

      @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
      @endif

<form action="" method="post">
        @csrf
         <div class="form-group">
            <label for="name">Tên danh mục</label>
            <input type="text" class="form-control" placeholder="Tên danh mục" value="{{ old('name',isset($category->c_name) ? $category->c_name : '')}}" name="name">
                  @if($errors->has('name'))
              <span class="error-text">
                  {{$errors->first('name')}}
              </span>
                @endif
         </div>
         <div class="form-group">
            <label for="name">Icon:</label>
            <input type="text" class="form-control" placeholder="fa fa-icon" value="{{ old('icon',isset($category->c_icon) ? $category->c_icon : '')}}" name="icon">
             @if($errors->has('icon'))
              <span class="error-text">
                  {{$errors->first('icon')}}
              </span>
                @endif
         </div>
         <div class="form-group">
            <label for="name">Thứ tự:</label>
            <input type="number" class="form-control" value="{{ old('thutu',isset($category->thutu) ? $category->thutu : '')}}" name="thutu">
             @if($errors->has('thutu'))
              <span class="error-text">
                  {{$errors->first('thutu')}}
              </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Meta title:</label>
            <input type="text" class="form-control" placeholder="Meta title" value="{{ old('c_title_seo',isset($category->c_title_seo) ? $category->c_title_seo : '')}}" name="c_title_seo">
            
         </div>
         <div class="form-group">
            <label for="name">Meta desciption:</label>
            <input type="text" class="form-control" placeholder="Meta desciption" value="{{ old('c_description_seo',isset($category->c_description_seo) ? $category->c_description_seo : '')}}" name="c_description_seo">
         </div>
         <div class="checkbox">
            <label><input type="checkbox" name="hot"> Nổi bật</label>
         </div>
         <button type="submit" class="btn btn-primary btn-sm">
         <i class="fa fa-dot-circle-o"></i> Lưu thông tin
         </button>
</form>