@extends('admin::layouts.master')

@section('content')
 <div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Đánh giá</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
          <li class="breadcrumb-item active">Đánh giá</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
	 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý đánh giá</strong>

                        </div>
                        <div class="card-body">
                 <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Tên thành viên</th>
                        <th>Sản phẩm</th>
                        <th>Nội dung</th>
                        <th>Đánh giá</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                     @if(isset($ratings))
                        @foreach($ratings as $key => $rating)
                            <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$rating->user->name}}</td>
                        <td>{{$rating->product->pro_name}}</td>
                        <td>{{$rating -> ra_content}}</td>
                        <td>{{$rating -> ra_number}} sao</td>
                        <td>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('deleteR',$rating->id)}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                        @endforeach
                     @endif
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
@endsection