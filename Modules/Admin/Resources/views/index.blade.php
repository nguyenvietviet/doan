@extends('admin::layouts.master')
@section('content')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
   .highcharts-figure, .highcharts-data-table table {
   min-width: 310px; 
   max-width: 800px;
   margin: 1em auto;
   }
   #container {
   height: 400px;
   }
   .highcharts-data-table table {
   font-family: Verdana, sans-serif;
   border-collapse: collapse;
   border: 1px solid #EBEBEB;
   margin: 10px auto;
   text-align: center;
   width: 100%;
   max-width: 500px;
   }
   .highcharts-data-table caption {
   padding: 1em 0;
   font-size: 1.2em;
   color: #555;
   }
   .highcharts-data-table th {
   font-weight: 600;
   padding: 0.5em;
   }
   .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
   padding: 0.5em;
   }
</style>
<div class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tổng quan</h1>
         </div>
         <!-- /.col -->
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
               <li class="breadcrumb-item active">Trang tổng quan</li>
            </ol>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</div>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
               <span class="info-box-icon bg-info elevation-1"><i class="fas fas fa-copy"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Bài viết</span>
                  <span class="info-box-number">
                  {{count($article)}}
                  </span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Đánh giá</span>
                  <span class="info-box-number">{{count($ratings)}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <!-- fix for small devices only -->
         <div class="clearfix hidden-md-up"></div>
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Sản phẩm</span>
                  <span class="info-box-number">{{count($product)}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
         <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
               <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>
               <div class="info-box-content">
                  <span class="info-box-text">Thành viên</span>
                  <span class="info-box-number">{{count($users)}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->
      </div>



      <div class="animated fadeIn">
         <div class="row">

        <!--Col-md6-->
            <div class="col-md-6">
              <div class="card">
              <div class="card-header border-transparent">
                <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                        </button>
                     </div>
               <figure class="highcharts-figure">
                  <div id="container"></div>
               </figure>
             </div>
             </div>
            </div>
          <!--Col-md6-->
          <div class="col-md-6">
            <div class="card">
               <div class="card-header border-transparent">
                  <h3 class="card-title">Đánh giá</h3>
                  <div class="card-tools">
                     <button type="button" class="btn btn-tool" data-card-widget="collapse">
                     <i class="fas fa-minus"></i>
                     </button>
                     <button type="button" class="btn btn-tool" data-card-widget="remove">
                     <i class="fas fa-times"></i>
                     </button>
                  </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body p-0">
                  <div class="table-responsive">
                     <table class="table m-0">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Tên thành viên</th>
                              <th>Sản phẩm</th>
                              <th>Đánh giá</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $i=1; ?>
                           @if(isset($ratings))
                           @foreach($ratings as $rating)
                           <tr>
                              <td>{{$i++}}</td>
                              <td>{{$rating->user->name}}</td>
                              <td>{{$rating->product->pro_name}}</td>
                              <td>{{$rating->ra_number}} sao</td>
                           </tr>
                           @endforeach
                           @endif
                           <?php $i++ ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>

            
           <!--Col-md6-->
         </div>
      </div>


      <div class="animated fadeIn">
         <div class="row">

<div class="col-md-12">
               <div class="card">
                  <div class="card-header border-transparent">
                     <h3 class="card-title">Đơn hàng</h3>
                     <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                        </button>
                     </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                     <div class="table-responsive">
                        {{-- <table class="table m-0"> --}}
                          <table id="example2" class="table table-bordered table-hover">
                          {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Tên khách hàng</th>
                                 <th>Số ĐT</th>
                                 <th>Tổng tiền</th>
                                 <th>Trạng thái</th>
                                 <th>Time</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $i=1; ?>
                              @foreach($transactionNews as $transaction)
                              <tr>
                                 <td>{{$i++}}</td>
                                 <td>{{isset($transaction->user->name) ? $transaction->user->name : '[N\A]'}}</td>
                                 <td>{{$transaction->tr_phone}}</td>
                                 <td>{{number_format($transaction->tr_total,0,',','.')}}VNĐ</td>
                                 <td>
                                   @if($transaction->tr_status == 1)
                                   <a style="color: white;" class="badge badge-success">Đã xử lý</a>
                                   @elseif($transaction->tr_status == 0)
                                   <a href="{{route('admin.get.active.transaction',$transaction->id)}}" style="color: white;" class="badge badge-secondary">Chờ xử lý</a>
                                   @elseif($transaction->tr_status == 2)
                                   <a style="color: white;" class="badge badge-warning">Đang gửi</a>
                                   @else
                                   <a style="color: white;" class="badge badge-danger">Cancel</a>
                                   @endif
                                 </td>
                                 <td>{{$transaction->created_at->format('H:i:s | d-m-Y ')}}</td>
                              </tr>
                              @endforeach
                              <?php $i++; ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>

               {{-- <div class="offset-5">{{$transactionNews->links()}}</div>  --}}
</div>
          

          <!--Col-md6-->
      </div>
   </div>
   <!-- .animated -->
   </div>
</section>
@endsection
@section('js')
<script>
   let data = "{{$dataMoney}}";
   
   
   dataChart = JSON.parse(data.replace(/&quot;/g,'"'));
   // var dataChart = jQuery.parseJSON(data);
   console.log(dataChart)
   
   Highcharts.chart('container', {
   chart: {
       type: 'column'
   },
   title: {
       text: 'Biểu đồ doanh thu'
   },
   accessibility: {
       announceNewData: {
           enabled: true
       }
   },
   xAxis: {
       type: 'category'
   },
   yAxis: {
       title: {
           text: 'Biểu đồ'
       }
   
   },
   legend: {
       enabled: false
   },
   plotOptions: {
       series: {
           borderWidth: 0,
           dataLabels: {
               enabled: true,
               format: '{point.y:.1f}VNĐ'
           }
       }
   },
   
   tooltip: {
       headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
       pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
   },
   
   series: [
       {
           name: "Browsers",
           colorByPoint: true,
           data: dataChart
         }
   ]
   });
</script>
@stop