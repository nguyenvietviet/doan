@extends('admin::layouts.master')
@section('content')

<style>
   .rating .active{color:#ff9705 !important;}
   .card-title .active{color: red;}
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sản phẩm</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item">Kho</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

{{-- <div class="row"> --}}
   <div class="col-sm-12">
      <form class="form-inline" action="" style="margin-bottom: 20px; margin-top: 10px;">
          <div class="form-group">       
            <input type="text" class="form-control" placeholder="Tên sản phẩm..." name="name" value="{{ \Request::get('name')}}">
          </div>
          <div class="form-group">       
            <select name="cate" id="" class="form-control">
               @if(isset($categories))
                  @foreach($categories as $category)
                     <option value="{{$category->id}}" {{\Request::get('cate')==$category->id ? "selected='selected'" : ""}}>{{$category->c_name}}</option>
                  @endforeach
               @endif
            </select>
          </div>
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
   </div>
{{-- </div> --}}

<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý sản phẩm / <a href="admin/warehouse?type=inventory" class="{{Request::get('type') == "inventory" || !Request::get('type') ? "active" : ""}}" >Hàng tồn /</a> <a href="admin/warehouse?type=pay" class="{{Request::get('type') == "pay" ? "active" : ""}}"> Bán Chạy</a></strong>
            </div>

            <div>
               <form method="post" enctype="multipart/form-data" action="{{route('admin.get.list.warehouse')}}">
                @csrf
                <div class="card-body">
                    <div class="col-md-12">
                        <table class="table">
                            <tr>
                                <td width="50%" align="right">
                                    <input type="file" name="select_file"/>
                                </td>
                                <td width="50%" align="left">
                                    <input type="submit" name="upload" class="btn btn-primary" value="Upload">
                                </td>
                            </tr>
                          
                        </table>
                      
                    </div>
                </div>
            </form>
            </div>


            <div class="card-body">
               <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Tên sản phẩm</th>
                        <th>Loại sản phẩm</th>
                        <th>Hình ảnh</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php $i=1; ?>
                     @if(isset($products))
                     @foreach($products as $product)
                     
                     <?php 
                        $age =0;
                        if($product->pro_total_rating)
                        {
                           $age = round($product->pro_total_number/$product->pro_total_rating,2);
                        }
                     ?>

                     <tr>
                        <td>{{$product->id}}</td>
                        <td>
                           {{$product -> pro_name}}
                           <ul style="padding-left: 17px;">
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_price}}(đ)</span></li>
                              <li><span><i class="fas fa-dollar-sign" style="padding-right: 3px;"></i></span><span>{{$product -> pro_sale}}(%)</span></li>

                              <li><span>Đánh giá:</span>
                                 <span class="rating">
                                    @for($i=1;$i<=5;$i++)
                                          <i class="fa fa-star {{$i <= $age ?'active':''}}" style="color: #999;"></i>
                                    @endfor
                                 </span>
                              </li>
                              <li><span>Số lượng:</span><span>{{$product->pro_number}}</span></li>
                              <li><span>Số lượng bán:</span><span>{{$product->pro_pay}}</span></li>
                           </ul>
                        </td>
                        <td>{{ isset($product->category->c_name) ? $product->category->c_name : '[N\A]'}}</td>
                        <td>
                           <img src="{{asset("")}}/{{ pare_url_file($product->pro_avatar)}}" alt="ảnh" class="img img-reponsive" style="height: 100px;width: 100px;">
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['active',$product->id])}}" class="badge {{$product->getStatus($product->pro_active)['class']}}">{{$product ->getStatus($product->pro_active)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('admin.get.action.product',['hot',$product->id])}}" class="badge {{$product->getHot($product->pro_hot)['class']}}">{{$product ->getHot($product->pro_hot)['name']}}</a>
                        </td>
                       
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.product',$product->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                           <a class="badge badge-danger" onclick="return confirm('Bạn có muốn xóa tin này?')" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.action.product',['delete',$product->id])}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                     <?php $i++ ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection

