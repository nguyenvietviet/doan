@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Sản phẩm</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.get.list.slide')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Thêm mới</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-info">
            <div class="card-header">
               <strong class="card-title">Thêm Slide</strong>
            </div>
            <div class="card-body">


            @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
           @endif
                <form action="admin/slide/them" method="POST" enctype="multipart/form-data">     
                      @csrf
      
                      <div class="form-group">
                          <label>Tên</label>
                          <input class="form-control" name="Ten" placeholder="Nhập vào tiêu đề" />
                      </div>
          
                      <div class="form-group">
                          <label>Nội dung</label>
                          <textarea name="NoiDung" id="ckeditor" class="form-control ckeditor" rows="3"></textarea>
                      </div>
                     
                      <div class="form-group">
                          <label>Hình ảnh</label>
                          <input type="file" name="Hinh" class="form-control"/>
                      </div>
                  
                      <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                      <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
                  <form>
          </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection