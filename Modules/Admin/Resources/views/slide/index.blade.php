@extends('admin::layouts.master')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Slide</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


	<div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Danh sách</strong>
                        </div>
                        <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>STT</th>
                        {{-- <th>Tên</th> --}}
                        {{-- <th>Nội dung</th> --}}
                        <th>Slide</th>
                        <th>Trạng thái</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <?php $i=1 ?>
                    <tbody>
                      @foreach($slide as $sl)
                      <tr>
                        <td>{{$i++}}</td>
                        {{-- <td>{{$sl->ten}}</td> --}}
                        {{-- <td>{!!$sl->noidung!!}</td> --}}
                        
                        <td>
                            <img width="300px" src="image_slide/{{$sl->hinh}}"/>
                        </td>
                        <td>
                          <a href="{{route('admin.get.action.slide',['active',$sl->id])}}" class="badge {{$sl->getStatus($sl->s_publish)['class']}}">{{$sl ->getStatus($sl->s_publish)['name']}}</a>
                        </td>
                        <td>
                          <a href="{{route('admin.get.edit.slide',$sl->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('admin.get.action.slide',['delete',$sl->id])}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                   <?php $i++ ?>
                        </div>
                    </div>
                </div>


                </div>
            </div>
@endsection