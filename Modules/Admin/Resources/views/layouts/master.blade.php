<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | DataTables</title>
  <!-- Tell the browser to be responsive to screen width -->
  <base href="{{asset("")}}">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="theme_admin1/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="theme_admin1/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="theme_admin1/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="theme_admin1/index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <img src="theme_admin1/dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Xuân Quang MB</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="theme_admin1/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{get_data_user('admins','name')}}</a>
          {{-- <hr> --}}
          <a href="{{route('admin.logout')}}" class="d-block">Thoát</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{route('admin.home')}}" class="nav-link {{\Request::route()->getName()=='admin.home' ? 'active' : ''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Trang tổng quan
              </p>
            </a>
           
          </li>
          <li class="nav-item">
            <a href="{{route('admin.get.list.category')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.category' ? 'active' : ''}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Danh mục
              </p>
              <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.category')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.category' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.category')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.category' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Sản phẩm
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.product')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.product' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.product')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.product' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.warehouse')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.warehouse' ? 'active' : ''}}">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Kho
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.rating')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.rating' ? 'active' : ''}}">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Đánh giá
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Tin tức
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.article')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.article' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.article')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.article' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.transaction')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.transaction' ? 'active' : ''}}">
              <i class="nav-icon fas fa-truck"></i>
              <p>
                Đơn hàng
                {{-- <i class="fas fa-angle-left right"></i> --}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('admin.get.list.user')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.user' ? 'active' : ''}}">
              <i class="nav-icon fa fa-group"></i>
              <p>
                Thành viên
              </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Slide
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.slide')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.slide' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.slide')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.slide' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.page_static')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.page_static' ? 'active' : ''}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pages
                {{-- <i class="fas fa-angle-left right"></i> --}}
              </p>
            </a>
            {{-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.page_static')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.page_static' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.page_static')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.page_static' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul> --}}
          </li>
         
            </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

<section class="content">
    @yield('content')
</section>

    
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.2
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="theme_admin1/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="theme_admin1/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="theme_admin1/plugins/datatables/jquery.dataTables.js"></script>
<script src="theme_admin1/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="theme_admin1/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="theme_admin1/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

 <script src="ckeditor/ckeditor.js"></script>
<script src="ckfinder/ckfinder.js"></script>
   
       

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
    <script>
        function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#out_img').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
              }
            }
            $("#input_img").change(function() {
              readURL(this);
            });
    </script>

@yield('js')
</body>
</html>
