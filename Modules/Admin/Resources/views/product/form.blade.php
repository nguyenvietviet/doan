<style>
    .error-text{
      color:red;
    }
  </style>

      @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
      @endif

<form action="" method="post" enctype="multipart/form-data">
   @csrf
   <div class="row">
      <div class="col-sm-8">
         <div class="form-group">
            <label for="pro_name">Tên sản phẩm</label>
            <input type="text" class="form-control" placeholder="Tên sản phẩm" value="{{ old('pro_name',isset($product->pro_name) ? $product->pro_name : '')}}" name="pro_name">
            @if($errors->has('pro_name'))
            <span class="error-text">
            {{$errors->first('pro_name')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Mô tả:</label>
            <textarea name="pro_description" class="form-control" id="" cols="30" rows="3" placeholder="Mô tả ngắn sản phẩm" value="">{{ old('pro_description',isset($product->pro_description) ? $product->pro_description : '')}}</textarea>
            @if($errors->has('pro_description'))
            <span class="error-text">
            {{$errors->first('pro_description')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="name">Nội dung:</label>
            <textarea name="pro_content" class="form-control" id="pro_content" cols="30" rows="3" placeholder="Nội dung" value="">{{ old('pro_content',isset($product->pro_content) ? $product->pro_content : '')}}</textarea>
            @if($errors->has('pro_content'))
            <span class="error-text">
            {{$errors->first('pro_content')}}
            </span>
            @endif
         </div>
         {{-- {{old('para_name',isset($product->pro_parameter) ? $product->pro_parameter : '') == $pa->id ? "selected=='selected'" : "" }} --}}
          <div class="form-group">
            <label for="name">Thông số:</label>
            @foreach($para as $pa)
            <textarea name="para_name" class="form-control"  id="a_content" cols="30" rows="3" placeholder="Nội dung" value="{{$pa->id}}">{{old('para_name',isset($pa->para_name) ? $pa->para_name:'')}}</textarea>
            @endforeach
            @if($errors->has('para_name'))
            <span class="error-text">
            {{$errors->first('para_name')}}
            </span>
            @endif
         </div>

         <div class="form-group">
            <label for="name">Meta title:</label>
            <input type="text" class="form-control" placeholder="Meta title" value="{{ old('pro_title_seo',isset($product->pro_title_seo) ? $product->pro_title_seo : '')}}" name="pro_title_seo">
         </div>
         <div class="form-group">
            <label for="name">Meta desciption:</label>
            <input type="text" class="form-control" placeholder="Meta desciption" value="{{ old('pro_description_seo',isset($product->pro_description_seo) ? $product->pro_description_seo : '')}}" name="pro_description_seo">
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group">
            <label for="name">Loại sản phẩm:</label>
            <select name="pro_category_id" id="" class="form-control">
               <option value="">--Chọn loại sản phẩm--</option>
               @if(isset($categories))
               @foreach($categories as $category)
               <option value="{{$category->id}}"{{old('pro_category_id',isset($product->pro_category_id) ? $product->pro_category_id : '') == $category->id ? "selected=='selected'" : "" }}>{{$category->c_name}}</option>
               @endforeach
               @endif
            </select>
            @if($errors->has('pro_category_id'))
            <span class="error-text">
            {{$errors->first('pro_category_id')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="pro_price">Giá sản phẩm:</label>
            <input type="number" placeholder="Giá sản phẩm" class="form-control" name="pro_price" value="{{old('pro_price',isset($product->pro_price) ? $product->pro_price : '')}}">
            @if($errors->has('pro_price'))
            <span class="error-text">
            {{$errors->first('pro_price')}}
            </span>
            @endif
         </div>
         <div class="form-group">
            <label for="pro_sale">% Khuyến mãi:</label>
            <input type="number" placeholder="% giảm giá" class="form-control" name="pro_sale" value="{{old('pro_sale',isset($product->pro_sale) ? $product->pro_sale : '0')}}">
         </div>
         <div class="form-group">
            <label for="pro_sale">Số lượng sản phẩm:</label>
            <input type="number" placeholder="10" class="form-control" name="pro_number" value="{{old('pro_number',isset($product->pro_number) ? $product->pro_number : '0')}}">
         </div>
        
         <div class="form-group">
            <img height="100px;" src="{{asset('image/default.jpg')}}" id="out_img">
         </div>
         <div class="form-group">
            <label for="avatar">Avatar:</label>
            <input type="file" name="avatar" id="input_img" class="form-control">
         </div>
         <div class="checkbox">
            <label><input type="checkbox" name="hot"> Nổi bật</label>
         </div>
      </div>
   </div>
   <button type="submit" class="btn btn-primary btn-sm">
   <i class="fa fa-dot-circle-o"></i> Lưu thông tin
   </button>
</form>
@section('js')
<script>
        CKEDITOR.replace( 'pro_content', {
            language:'vi',
            filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
            filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        } );
    </script>
    <script>
        CKEDITOR.replace( 'a_content', {
            language:'vi',
            filebrowserBrowseUrl: 'http://localhost/Web_Cellphone/public/ckfinder/ckfinder.html',
            filebrowserUploadUrl: 'http://localhost/Web_Cellphone/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        } );
    </script>
@endsection