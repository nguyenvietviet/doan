@extends('admin::layouts.master')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1><a href="{{route('admin.home')}}">Trang chủ/</a><a href="{{route('admin.get.create.parameter')}}">Bài viết/</a><a href="{{route('admin.get.list.parameter')}}">Danh mục</a></h1>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <form class="form-inline" action="" style="margin-bottom: 20px; margin-top: 10px;">
          <div class="form-group">       
            <input type="text" class="form-control" placeholder="Tên bài viết..." name="name" value="{{ \Request::get('name')}}">
          </div>
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </form>
   </div>
</div>

<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Quản lý thông số <a href="{{route('admin.get.create.parameter')}}" class="pull-right badge badge-primary"><i class="fas fa-plus-circle"></i> Thêm mới</a></strong>
            </div>
            <div class="card-body">
               <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th style="width: 20%;">Tên bài viết</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($para))
                     @foreach($para as $pa)
                     <tr>
                        <td>{{$pa->id}}</td>
                        <td>
                           {!!$pa->para_name!!}
                        </td>
                        <td>
                           <a class="badge badge-info" style="padding:5px 10px;border:1px solid #eee;" href="{{route('admin.get.edit.parameter',$pa->id)}}"><i class="fas fa-pen"></i> Cập nhật</a>
                        </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- .animated -->
@endsection