<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('authenticate')->group(function(){
     Route::get('/login', 'AdminAuthController@getLogin')->name('admin.login');
     Route::post('/login', 'AdminAuthController@postLogin');
     Route::get('/logout', 'AdminAuthController@getLogout')->name('admin.logout');
});

Route::prefix('admin')->middleware('CheckLoginAdmin')->group(function() {
    Route::get('/', 'AdminController@index')->name('admin.home');

    Route::group(['prefix' => 'category'],function(){
    	//http://localhost/Web_Cellphone/public/admin/category
    	Route::get('/','AdminCategoryController@index')->name('admin.get.list.category');
    	Route::get('/create','AdminCategoryController@create')->name('admin.get.create.category');
    	Route::post('/create','AdminCategoryController@store');
    	Route::get('/update/{id}','AdminCategoryController@edit')->name('admin.get.edit.category');
    	Route::post('/update/{id}','AdminCategoryController@update');
    	Route::get('/{action}/{id}','AdminCategoryController@action')->name('admin.get.action.category');
    });
    Route::group(['prefix' => 'product'],function(){
    	//http://localhost/Web_Cellphone/public/admin/category
    	Route::get('/','AdminProductController@index')->name('admin.get.list.product');
    	Route::get('/create','AdminProductController@create')->name('admin.get.create.product');
    	Route::post('/create','AdminProductController@store');
    	Route::get('/update/{id}','AdminProductController@edit')->name('admin.get.edit.product');
    	Route::post('/update/{id}','AdminProductController@update');
    	Route::get('/{action}/{id}','AdminProductController@action')->name('admin.get.action.product');
    });

    //Tin tức
     Route::group(['prefix' => 'article'],function(){
        //http://localhost/Web_Cellphone/public/admin/category
        Route::get('/','AdminArticleController@index')->name('admin.get.list.article');
        Route::get('/create','AdminArticleController@create')->name('admin.get.create.article');
        Route::post('/create','AdminArticleController@store');
        Route::get('/update/{id}','AdminArticleController@edit')->name('admin.get.edit.article');
        Route::post('/update/{id}','AdminArticleController@update');
        Route::get('/{action}/{id}','AdminArticleController@action')->name('admin.get.action.article');
    });

     //Kho ( WareHouse )
     Route::group(['prefix' => 'warehouse'],function(){

        Route::get('/','AdminWarehouseController@getWareHouseProduct')->name('admin.get.list.warehouse');
        Route::post('/','AdminWarehouseController@import')->name('product.import');
    });

     //Quản lý đơn hàng
     Route::group(['prefix' => 'transaction'],function(){
        //http://localhost/Web_Cellphone/public/admin/category
        Route::get('/','AdminTransactionController@index')->name('admin.get.list.transaction');
        Route::get('/view/{id}','AdminTransactionController@viewOrder')->name('admin.get.view.order');
        Route::get('/active/{id}','AdminTransactionController@actionTransaction')->name('admin.get.active.transaction');

    //Cập nhật lại trạng thái đơn hàng
        Route::get('/update/{id}','AdminTransactionController@edit')->name('admin.get.edit.transaction');
        Route::post('/update/{id}','AdminTransactionController@update');

        Route::get('/xoa/{id}','AdminTransactionController@getXoa')->name('deleteTran');
        Route::get('/{action}/{id}','AdminTransactionController@action')->name('admin.get.action.transaction');
        
    });

     // Slide
     Route::group(['prefix'=>'slide'],function(){

        Route::get('/','SlideController@index')->name('admin.get.list.slide');
        Route::get('/them','SlideController@create')->name('admin.get.create.slide');
        Route::post('/them','SlideController@store');

        Route::get('/sua/{id}','SlideController@edit')->name('admin.get.edit.slide');
        Route::post('/sua/{id}','SlideController@update');

        Route::get('/{action}/{id}','SlideController@getXoa')->name('admin.get.action.slide');
    });


     //Parameter
     Route::group(['prefix' => 'parameter'],function(){

        Route::get('/','ParameterController@index')->name('admin.get.list.parameter');
        Route::get('/create','ParameterController@create')->name('admin.get.create.parameter');
        Route::post('/create','ParameterController@store');
        Route::get('/update/{id}','ParameterController@edit')->name('admin.get.edit.parameter');
        Route::post('/update/{id}','ParameterController@update');
        Route::get('/{action}/{id}','ParameterController@action')->name('admin.get.action.parameter');
    });
        

     //Quản lý thành viên
     Route::group(['prefix' => 'user'],function(){
        Route::get('/','AdminUserController@index')->name('admin.get.list.user');
    });

     Route::group(['prefix' => 'rating'],function(){
        Route::get('/','AdminRatingController@index')->name('admin.get.list.rating');
        Route::get('/xoa/{id}','AdminRatingController@delete')->name('deleteR');
    });

     //Qly ve chung toi
     Route::group(['prefix' => 'page-static'],function(){
        Route::get('/','AdminPageStaticCotrllerController@index')->name('admin.get.list.page_static');
        Route::get('/create','AdminPageStaticCotrllerController@create')->name('admin.get.create.page_static');
        Route::post('/create','AdminPageStaticCotrllerController@store');
        Route::get('/update/{id}','AdminPageStaticCotrllerController@edit')->name('admin.get.edit.page_static');
        Route::post('/update/{id}','AdminPageStaticCotrllerController@update');
        // Route::get('/{action}/{id}','AdminPageStaticCotrllerController@action')->name('admin.get.action.page_static');
        Route::get('/xoa/{id}','AdminPageStaticCotrllerController@delete')->name('deletePage');
    });
});
	