<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\Order;
use DB;

class AdminTransactionController extends Controller
{

    public function index()
    {
    	// $transactions = Transaction::with('user:id,name')
     //        ->orderByDesc('id')
     //        ->get();
        // $transactions = Transaction::with('user:id,name')->orderBy('created_at','ASC')->get();
        $Total = Transaction::where('tr_status',Transaction::STATUS_DONE)->sum('tr_total');
        $transactions = Transaction::getDataTransaction(request()->all());
    	$viewData = [
    		'transactions' => $transactions,
            'Total' => $Total,
            'moneyTotal' => $transactions->where('tr_status', STATUS_DONE)->sum('tr_total'),
    	];
        return view('admin::transaction.index',$viewData);
    }

    public function viewOrder(Request $request,$id)
    {
    	if($request->ajax())
    	{
        	$orders = Order::where('or_transaction_id',$id)->get();
        	$html = view('admin::components.order',compact('orders'))->render();

        	return \response()->json($html);
    	}

    }
    /* Xử lý trạng thái đơn hàng*/

    public function actionTransaction($id)
    {
        $transaction = Transaction::find($id);
        $orders = Order::where('or_transaction_id',$id)->get();
        if($orders)
        {
             //Cập nhật lại số lượng của sản phẩm

             //Tăng cái biến pay sản phẩm
            foreach($orders as $order)
            {
                $product = Product::find($order->or_product_id);
                $product->pro_number =$product->pro_number - $order->or_qty;
                $product->pro_pay ++;
                $product->save();
            }
        }

        /*Cập nhật lại trạng thái đơn hàng*/
        \DB::table('users')->where('id',$transaction->tr_user_id)->increment('total_pay');

        $transaction->tr_status= Transaction::STATUS_DONE;
        $transaction->save();
        return redirect()->back()->with('thongbao','Xử lý đơn hàng thành công');
    }


//Cập nhật trạng thái đơn hàng
    public function edit($id)
    {
      $transaction = Transaction::find($id);
      return view('admin::transaction.update',compact('transaction'));
    }

    public function update(Request $request,$id)
    {
        $transaction = Transaction::find($id);
        $transaction->tr_status = $request->quyen;
       
        $transaction->save();
       return redirect()->route('admin.get.list.transaction')->with('thongbao','Cập nhật đơn hàng thành công');
    }

    public function action($action,$id)
    {
        if($action)
        {
            $transaction = Transaction::find($id);
            switch($action)
            {
                case 'delete':
                    $transaction->delete();
                    break;
               
            }
           
        }
        return redirect()->back();
    }

    // public function getXoa($id)
    // {
    //     Transaction::where('id',$id)->delete();

    //     return redirect()->back(); 
    // }


}
