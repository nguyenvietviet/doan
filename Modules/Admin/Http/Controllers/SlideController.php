<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Slide;

class SlideController extends Controller
{
   public function index()
    {
        $slide = Slide::orderBy('id','DESC')->get();
        return view('admin::slide.index',['slide'=>$slide]);
    }

    public function create()
    {
        return view('admin::slide.them');
    }

    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'Ten'=>'required',
        //     'NoiDung'=>'required',
        //     'Hinh'=>'max:3000'
        // ],
        // [
        //     'Ten.required'=>'Bạn chưa nhập tên',
        //     'NoiDung'=>'Bạn chưa nhập nội dung',
        //     'Hinh.max' =>'Dung lượng ảnh vượt quá 3M'
        // ]);

        $slide = new Slide;
        $slide->ten = $request->Ten;
        $slide->noidung = $request->NoiDung;
        
        $file = $request->file('Hinh')->getClientOriginalName();
        $slide->hinh = $file;
        $request->file('Hinh')->move(base_path('public/image_slide/'),$file);

        $slide->save();
        return view('admin::slide.them')->with('thongbao','Thêm thành công');

    }

    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin::slide.sua',['slide'=>$slide]);
    }

    public function update(Request $request,$id)
    {
        // $this->validate($request,[
        //     'Ten'=>'required',
        //     'NoiDung'=>'required',
        // ],
        // [
        //     'Ten.required'=>'Bạn chưa nhập tên',
        //     'NoiDung'=>'Bạn chưa nhập nội dung',
        // ]);

        $slide = Slide::find($id);
        $slide->ten = $request->Ten;
        $slide->noidung = $request->NoiDung;
        
        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');

            $name = $file->getClientOriginalName();
            $Hinh = str_random(2)."_".$name;

            while(file_exists("image_slide/".$Hinh))
            {
                $Hinh = str_random(2)."_".$name;
            }

            $file->move("image_slide",$Hinh);
            if($slide->hinh)
            {
                unlink("image_slide/".$slide->hinh);
            }
            $slide->hinh = $Hinh;
        }

        $slide->save();
        return redirect()->back()->with('thongbao','Sửa thành công');
    }

    public function getXoa($action,$id)
    {
       if($action)
        {
            $slide = Slide::find($id);
          
            switch($action)
            {
                case 'delete':
                        $slide->delete();
                    break;
                case 'active':
                    $slide->s_publish = $slide->s_publish ? 0:1;
                     $slide->save();
                    break;
            }
        }
        return redirect()->back();
    }
}
