<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Parameter;

class ParameterController extends Controller
{
    public function index()
    {
        $para = Parameter::all();
        return view('admin::parameter.index',['para'=>$para]);
    }

    public function create()
    {
        return view('admin::parameter.create');
    }

    public function store(Request $request)
    {
        // dd($requestProduct->all());
         $this->insertOrUpdate($request);
         return redirect()->back()->with('thongbao','Thêm thành công');
    }

    public function edit($id)
    {
        $para = Parameter::find($id);
        return view('admin::parameter.create',compact('para'));
    }

    public function update(Request $request,$id)
    {
        $this->insertOrUpdate($request,$id);
        return redirect()->back()->with('thongbao','Sửa thành công');
    }

    public function insertOrUpdate($request,$id='')
    {
        $para = new Parameter();

        if($id) $para = Parameter::find($id);

        $para->para_name = $request->para_name;
        $para->save();

    }

    public function action($id)
    {
        Parameter::where('id',$id)->delete();
        return redirect()->back();
    }

}
