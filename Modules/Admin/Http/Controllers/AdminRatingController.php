<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Rating;
use App\Models\User;
use App\Models\Product;
use App\Models\Contact;

class AdminRatingController extends Controller
{
    public function index()
    {
        $ratings = Rating::with('user:id,name','product:id,pro_name')->paginate(10);

        $viewData =[
            'ratings' => $ratings,
            
        ];
        return view('admin::rating.index',$viewData);
    }

    public function delete($id)
    {
        Rating::where('id',$id)->delete();
        return redirect()->back();
    }

    
 
}
