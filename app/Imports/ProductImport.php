<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class ProductImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            //
            'pro_name' => $row['tensanpham'],
            'pro_category_id' => $row['madanhmuc'],
            'pro_price' => $row['gia'],
            'pro_sale' => $row['giamgia'],
            
            'pro_description' => $row['mota'],
            'pro_number' => $row['soluongsanpham'],
            'pro_content' => $row['noidung'],

            // 'pro_name' => $row['pro_name'],
            // 'pro_category_id' => $row['pro_category_id'],
            // 'pro_price' => $row['pro_price'],
            // 'pro_sale' => $row['pro_sale'],
            
            // 'pro_description' => $row['pro_description'],
            // 'pro_number' => $row['pro_number'],
            // 'pro_content' => $row['pro_content'],
           
        ]);
    }
}
