<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    //
    protected $table = 'transactions';
    protected $fillable = ['*'];
    protected $guarded = ['*'];

    // const PENDING = 0;
    // const DELIVERY = 1;
    // const FINISH = 2;
    // const CANCEL = 3;

    const STATUS_DONE    = 1;
    const STATUS_DEFAULT = 0;
    const STATUS_SENDING = 2;
    const STATUS_CANCEL = 3;

    const TYPE_CART = 1;
    const TYPE_PAY  = 2;


    protected $status=[
        1=>[
            'name' => 'đã xử lý',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'chờ xử lý',
            'class' => 'badge-secondary'
        ],
         2=>[
            'name' => 'Đang gửi',
            'class' => 'badge-Warning'
        ],
         3=>[
            'name' => 'Cancel',
            'class' => 'badge-danger'
        ]
    ];
    

   

    public function getStatus()
    {
        return array_get($this->status,$this->tr_status,'[N\A]');
    }



    public function user()
    {
        return $this->belongSto(User::class, 'tr_user_id');
    }

    public static function filter($param = null) {
        $transactions = new Transaction();

        if (!empty($param['from_date']) && empty($param['to_date'])) {
            $transactions = $transactions->where('transactions.created_at', '>=', $param['from_date']);
        }

        if (empty($param['from_date']) && !empty($param['to_date'])) {
            $transactions = $transactions->where('transactions.created_at', '<=', $param['to_date']);
        }

        if (!empty($param['from_date']) && !empty($param['to_date'])) {
            $transactions = $transactions->whereBetween('transactions.created_at', [$param['from_date'], date('Y-m-d', strtotime("+1 day", strtotime($param['to_date'])))]);
        }

        return $transactions;
    }


// cái hàm nay để thay cho 2 hàm bên controller của ô.
    public static function getDataTransaction($param = null) {
        $transactions = self::filter($param);
        $transactions->select(
            'transactions.*'
        )
        ->with([
            'user' => function($user) {

            }
        ])
        ->orderBy('transactions.created_at', 'DESC');

        return $transactions->paginate(LIMIT);
    }
}
