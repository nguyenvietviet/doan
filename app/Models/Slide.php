<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //
    protected $table = 'slides';

    const STATUS_PUBLIC =1;
    const STATUS_PRIVATE =0;


     protected $status=[
       1=>[
            'name' => 'Public',
            'class' => 'badge-danger'
        ],
        0=>[
            'name' => 'Private',
            'class' => 'badge-secondary'
        ]
    ];

    public function getStatus()
    {
        return array_get($this->status,$this->s_publish,'[N\A]');
    }

}
