<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'a_name' => 'required|unique:articles,a_name,'.$this->id,
            'a_description' => 'required|min:3|max:100',
            'a_content'=> 'required'
        ];
    }
    public function messages(){
        return[
            'a_name.required'=>'trường này không được bỏ trống',
            'a_name.unique' =>'Tên bài viết đã tồn tại',
            'a_description.required'=>'trường này không được bỏ trống',
            'a_description.min'=>'Mô tả phải có độ dài từ 3 đến 100 ký tự',
            'a_description.max'=>'Mô tả phải có độ dài từ 3 đến 100 ký tự',
            'a_content.required'=>"trường này không được bỏ trống"
        ];
    }
}
