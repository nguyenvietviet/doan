<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\User;
use App\Http\Requests\RequestPassword;
use Illuminate\Support\Facades\Hash;    

class UserController extends Controller
{
    //SHow tổng quan user
    public function index()
    {
    	$transactions = Transaction::where('tr_user_id',get_data_user('web'));

    	$listTransaction = $transactions;
    	$transactions = $listTransaction->addSelect('id','tr_total','tr_address','tr_phone','created_at','tr_status','updated_at')->paginate(30);

    	$totalTransaction = $listTransaction->select('id')->count();
    	$totalTransactionDone = $listTransaction->where('tr_status',Transaction::STATUS_DONE)->select('id')->count();
    	

    	$viewData=[
    		'totalTransaction' => $totalTransaction,
    		'totalTransactionDone' => $totalTransactionDone,
    		'transactions'  => $transactions
    	];
    	return view('user.index',$viewData);
    }

    public function updateInfo()
    {
    	$user = User::find(get_data_user('web'));

    	return view('user.info',compact('user'));
    }

    public function saveUpdateInfo(Request $request)
    {
    	// dd($request);
    	$user = User::where('id',get_data_user('web'))
    	->update($request->except('_token'));

    	return redirect()->back()->with('thongbao','Cập nhật thông tin thành công');
    }

    public function updatePassword()
    {
        return view('user.password');
    }

    public function saveUpdatePassword(RequestPassword $requestPassword)
    {
        // dd($requestPassword->all());
        if($hashed = Hash::check($requestPassword->password_old, get_data_user('web','password')))
        {
            $user = User::find(get_data_user('web'));
            $user->password = bcrypt($requestPassword->password);
            $user->save();
            return redirect()->back()->with('thongbao','Cập nhật thành công');
        }
        return redirect()->back()->withd('warning','Mật khẩu không đúng');
    }
}
