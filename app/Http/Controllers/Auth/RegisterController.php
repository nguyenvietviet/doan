<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
use Mail;

class RegisterController extends Controller
{
    public function getRegister()
    {
        return view('auth.register');
    }

    public function postRegister(Request $request)
    {
         $this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:33',
                'phone' => 'required'
            ],
            [
                'name.required'=>'Bạn chưa nhập tên',
                'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
                'email.required'=>'Bạn chưa nhập Email',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập password',
                'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'Mật khẩu có tối đa là 33 ký tự',
                'phone.required' => "Bạn chưa nhập số điện thoại"
            ]);

        $user           = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone    = $request->phone;

        $user->save();

        if ($user->id) {
            $email = $user->email;

            $code = bcrypt(md5(time() . $email));
            $url  = route('user.verify.account', ['id' => $user->id, 'code' => $code]);

            $user->code_active = $code;
            $user->time_active = Carbon::now();
            $user->save();

            $data = [
                'route' => $url
            ];

            try{
                Mail::send('email.verify_account', $data, function ($message) use ($email) {
                    $message->to($email, 'Xác nhận tài khoản')->subject('Xác nhận tài khoản');
                });
            }catch (\Exception $exception){
                Log::error(" Cannot send email very account ". $exception->getMessage());
            }

            return redirect()->route('get.login')->with('thongbao','Tạo tài khoản thành công');
        }
        return redirect()->back()->with('warning','Tạo tài khoản thất bại');
    }

    //Xác nhận tài khoản
    public function verifyAccount(Request $request)
    {
        $code = $request->code;
        $id   = $request->id;

        $checkUser = User::where([
            'code_active' => $code,
            'id'          => $id
        ])->first();

        if (!$checkUser) {
            return redirect('/')->with('warning', 'Link bị lỗi, vui lòng thử lại sau');
        }
        $checkUser->active = 2;
        $checkUser->save();
        // return view('auth.passwords.reset');
        return redirect('/home')->with('thongbao', 'Xác nhận tài khoản thành công');
    }
}
