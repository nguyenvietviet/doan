<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Article; 
use App\Models\Category;
use App\Models\Transaction;
use App\Models\Order;
use App\Models\Slide;

class HomeController extends FrontendController
{
    
    public function __construct()
    {
        parent::__construct();
    }

  
    public function index(Request $request)
    {
        $slide = Slide::where('s_publish',1)->get();

        $category = Category::with('products')
        ->where('c_home',Category::HOME)
        ->orderBy('thutu','ASC')
        ->limit(2)
        ->get();

    	$productHot = Product::orderBy('id','DESC')->where([
    		'pro_hot' => Product::HOT_ON,
    		'pro_active' => Product::STATUS_PUBLIC
    	]);


        if($request->price);
         {
            // dd($request->price);
            $price = $request->price;
            switch ($price) {
               case '1':
                  $productHot->where('pro_price','<',1000000);
                  break;
               case '2':
                  $productHot->whereBetween('pro_price',[1000000,3000000]);
                  break;
                case '3':
                  $productHot->whereBetween('pro_price',[3000000,5000000]);
                  break;
                case '4':
                  $productHot->whereBetween('pro_price',[5000000,7000000]);
                  break;
                case '5':
                  $productHot->whereBetween('pro_price',[7000000,10000000]);
                  break;
                case '6':
                  $productHot->where('pro_price','>',10000000);
                  break;
                  default:
               $productHot->orderBy('id','DESC');
            }
         }
         $productHot = $productHot->paginate(8)->appends(request()->query());

        $articleNews = Article::orderBy('id','DESC')->paginate(4);

        //Hiển thị danh mục sản phẩm nổi bật ra trang chủ
        $categoriesHome = Category::with('products')
        ->where('c_home',Category::HOME)
        ->limit(3)
        ->get();

        
         $productSuggest =[];
        //kiểm tra người dùng đăng nhập
        if(get_data_user('web'))
        {
            $transactions = Transaction::where([
                'tr_user_id'=> get_data_user('web'),
                'tr_status' => Transaction::STATUS_DONE
            ])->pluck('id');


            if(!empty($transactions))
            {
                $listId = Order::whereIn('or_transaction_id',$transactions)->distinct()->pluck('or_product_id');
                // dd($listId);

                if(!empty($listId))
                {
                    $listIdCategory = Product::whereIn('id',$listId)->distinct()->pluck('pro_category_id'); 
                    
                    if($listIdCategory)
                    {
                        $productSuggest = Product::whereIn('pro_category_id',$listIdCategory)->limit(3)->get();
                    }
                } 
            }
        }

        $viewData = [
            // '$productPublic'=>$productPublic,
        	  'productHot' =>$productHot,
            'slide' =>$slide,
            'articleNews' =>$articleNews,
            'categoriesHome' =>$categoriesHome,
            'productSuggest' =>$productSuggest,
            'category' =>$category,
             'query'  => $request->query()
        ];
        return view('home.index',$viewData);
    }
}
